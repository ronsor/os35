all: out.img
LIBS = libc/libc.a libx86/libx86.a libm/libm.a lua-5.3.4/src/liblua.a klua.c fatfs_ex/fatfs.a libz/uz.a
PWD := $(shell pwd)
INC = -I $(PWD)/libx86 -I $(PWD)/libm -I $(PWD)/libc/include -I ./
QEMU = q386 -curses
boot12.bin: boot12.asm
	nasm -f bin boot12.asm -o boot12.bin
init.exe: kmain.c kconsole.c kdiskio.c earlyfs.c kfileio.c $(LIBS) kexfile.c
	smlrcc -unreal earlyfs.c kinit.asm kconsole.c kdiskio.c kfileio.c kmain.c kexfile.c $(LIBS) -o init.exe -v $(INC) -I $(PWD)/lua-5.3.4/src -DLUA_32BITS
out.img: boot12.bin init.exe $(wildcard lua/*.lua) mods/tcpip/tcpip.bin
	mformat -Ci out.img -f 1440 -B boot12.bin ::
	mcopy -i out.img init.exe ::
	mmd -i out.img ::bin
	mmd -i out.img ::etc
	mcopy -D o -i out.img etcdata/* ::etc
	mcopy -i out.img mods/tcpip/tcpip.bin ::bin
	for k in $$(ls lua/*.lua); do util/lz -c < $$k > /tmp/001.luz; mcopy -i out.img /tmp/001.luz ::/bin/$$(basename $$k | sed 's/\.lua//g'); mattrib -i out.img +s ::/bin/$$(basename $$k | sed 's/\.lua//g'); done
	mcopy -D o -i out.img lua/init.lua ::init.lua
mods/tcpip/tcpip.bin:
	make -C mods/tcpip
lua-5.3.4/src/liblua.a:
	make -C lua-5.3.4 generic CC=smlrcc CFLAGS="-unreal -DLUA_32BITS $(INC)"
libc/libc.a:
	make -C libc CC=smlrcc CFLAGS='-Iinclude -Isrc/templates -unreal'
libm/libm.a:
	smlrcc -unreal -c -o libm/libm.a $(wildcard libm/*.c) $(INC)
libx86/libx86.a: $(wildcard libx86/*.c)
	smlrcc -unreal -c -o libx86/libx86.a $(wildcard libx86/*.c) $(INC)
fatfs/fatfs.a:
	smlrcc -unreal -c -o fatfs/fatfs.a $(wildcard fatfs/*.c) $(INC)
fatfs_ex/fatfs.a:
	smlrcc -unreal -c -o fatfs_ex/fatfs.a $(wildcard fatfs_ex/*.c) $(INC)
libz/uz.a:
	smlrcc -unreal -c -o libz/uz.a $(wildcard libz/*.c) $(INC)
z80/z80.a:
	smlrcc -unreal -c -o z80/z80.a $(wildcard z80/*.c) $(INC)
test: all
	cd qemu-data && $(QEMU) -hda ../test.hdd -fda ../out.img -boot a -vga std
