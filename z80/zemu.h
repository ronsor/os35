/* ZEMU.h
 * Header for ZEMU example.
 *
 * Copyright (c) 2012, 2016 Lin Ke-Fong
 *
 * This code is free, do whatever you want with it.
 */

#ifndef __ZEMU_INCLUDED__
#define __ZEMU_INCLUDED__

#include "z80emu.h"

typedef struct ZEMU {

	Z80_STATE	state;
	unsigned char	memory[65536];
	int 		is_done;

} ZEMU;

extern void     SystemCall (ZEMU *ZEMU);

#endif
