#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#ifdef EARLYFS
#include "fatfs/pff.h"
struct _private_earlyfile {
	char fname[64];
	long pos;
};
#endif
int system(char *cmd) {
	if (!strcmp(cmd, "reboot")) {
		asm("db 0x0ea\ndw 0x0000\ndw 0xffff");
	}
	return 127;
}
char *getenv(char *name) {
	return NULL;
}
#ifdef EARLYFS
#define EFPRIV struct _private_earlyfile
size_t _earlyfile_read(FILE *stream, char *buf, size_t len) {
	EFPRIV* priv = stream->private;
	pf_open(priv->fname);
	pf_lseek(priv->pos);
	if (buf == NULL) return priv->pos == earlyfs_fsize();
	if (priv->pos == earlyfs_fsize()) return 0;
	UINT brd;
	pf_read(buf, len + priv->pos > earlyfs_fsize() ? earlyfs_fsize() - priv->pos : len, &brd);
	priv->pos += brd;
	return brd;
}
int _earlyfile_close(FILE *stream) {
	free(stream->private);
	free(stream);
	return 0;
}
int _earlyfile_seek(FILE *stream, long offset, int whence) {
	EFPRIV* priv = stream->private;
	pf_open(priv->fname);
	pf_lseek(priv->pos);
	if (whence == SEEK_END) { priv->pos = earlyfs_fsize(); }
	if (whence == SEEK_START) { priv->pos = offset > earlyfs_fsize() ? earlyfs_fsize() : offset; }
	if (whence == SEEK_CUR) { priv->pos += (priv->pos + offset) > earlyfs_fsize() ? earlyfs_fsize() - priv->pos : offset; }
	return 0;
}
struct File_methods _private_earlyfile_methods = {
	NULL,
	_earlyfile_read,
	_earlyfile_close,
	_earlyfile_seek,
	NULL
};
#endif
FILE *fopen(const char *path, const char *mode) {
	if (path[0] == '%') {
	#ifdef EARLYFS
		if (pf_open(path + 1)) return NULL;
		FILE *handle = malloc(sizeof(FILE));
		handle->vmt = &_private_earlyfile_methods;
		handle->flags = F_RAW;
		struct _private_earlyfile *private = malloc(sizeof(struct _private_earlyfile));
		strcpy(private->fname, path + 1);
		private->pos = 0;
		handle->private = private;
		return handle;
	#else
		char newpath[96];
		sprintf(newpath, "%d:/%s", getbootdrv() > 0 ? getbootdrv() - 0x80 : 0, path+1);
		return fopenex(newpath, mode);
	#endif
	} else {
		return fopenex(path, mode);
	}
	return NULL;
}
FILE *freopen(const char *path, const char *mode, FILE *stream) {
	return NULL;
}
