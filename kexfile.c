#include <stdio.h>
#include "fatfs_ex/ff.h"
#include <string.h>
#include "stat.h"
PARTITION VolToPart[] = {{0,0},{1,0},{2,0},{3,0},{4,0}};

static FATFS fs;
int disk_mount(char *path) {
	return f_mount(&fs, path, 1);
}
static size_t _fwrite(FILE *fp, char *buf, size_t len) {
	UINT ret;
	f_write((FIL*)fp->private, buf, len, &ret);
	return ret;
}
static size_t _fread(FILE *fp, char *buf, size_t len) {
	UINT ret;
	if (!buf) return f_eof((FIL*)fp->private);
	f_read((FIL*)fp->private, buf, len, &ret);
	return ret;
}
static int _fclose(FILE *fp) {
	f_close((FIL*)fp->private);
	free((FIL*)fp->private);
	free(fp);
}
static int _fseek(FILE *fp, long offset, int whence) {
	return f_lseek((FIL*)fp->private, whence == SEEK_SET ? offset : whence == SEEK_END ? f_size((FIL*)fp->private) : f_tell((FIL*)fp->private) + offset);
}
static int _fsetopt(FILE *fp, int cmd, void *buf) {
	if (cmd = CTL_FLUSH) { f_sync((FIL*)fp->private); return 0; }
}
struct File_methods _fmethods = {
	_fwrite,
	_fread,
	_fclose,
	_fseek,
	_fsetopt
};
FILE *fopenex(const char *path, const char *mode) {
	int fflags = 0;
	if (mode[0] == 'w')
		fflags = FA_WRITE | FA_CREATE_ALWAYS;
	else
		fflags = FA_READ;
	if (mode[1] == '+')
		fflags |= FA_READ | FA_WRITE;
	FIL *f = calloc(1,sizeof(FIL));
	if (!f) return NULL;
	if (f_open(f, path, fflags)) {
		free(f);
		return NULL;
	}
	FILE *ret = malloc(sizeof(FILE));
	ret->flags = 0;
	ret->vmt = &_fmethods;
	ret->private = f;
	return ret;
}

int rename(const char *a, const char *b) {
	return f_rename(a, b);
}

int remove(const char *a) {
	return f_unlink(a);
}
int mkdir(const char *a) {
	return f_mkdir(a);
}
int mkfs(const char *a) {
	return f_mkfs(a, FM_ANY, 0, 0, 0);

}
int listdir(const char *a, const char **out) {
	*out = malloc(32768);
	**out = 0;
	DIR dir; FILINFO file;
	int res = f_opendir(&dir, a);
	if (res) return res;
	file.fname[0] = 1;
	while ((res = f_readdir(&dir, &file)) == 0 && file.fname[0]) {
		sprintf(*out, "%s%s\n", *out, file.fname);
	}
	f_closedir(&dir);
	return res;
}
int stat(struct stat* st, char *path) {
	FILINFO f;
	if (f_stat(path, &f)) return 1;
	st->size = f.fsize;
	st->dir = f.fattrib&AM_DIR;
	st->attr = f.fattrib&AM_SYS ? "sys" : f.fattrib&AM_RDO ? "rdonly" : "none";
	return 0;
}
int chmod(char *path, char *attr) {
	return f_chmod(path, !strcmp(attr, "sys") ? AM_SYS : !strcmp(attr, "rdonly") ? AM_RDO : 0, AM_SYS | AM_RDO);
}
