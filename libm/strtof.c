/*
  Copyright (c) 2017, Alexey Frunze
  2-clause BSD license.
*/
#ifdef __SMALLER_C_32__

#include <math.h>
int isdigit(int c);
typedef int bool;
static int false = 0;
static int true = 1;
float strtof(const char* str, char **endptr) {
    float res = 0.0F;
    char *ptr = (char*)str;
    bool afterDecimalPoint = false;
    float div = 1; // Divider to place digits after the deciaml point

    while (*ptr != '\0') {
        if (isdigit(*ptr)) {
            // Integer part
            if (!afterDecimalPoint) {
                res = res * 10; // Shift the previous digits to the left
                res = res + *ptr - '0'; // Add the new one
            }
            // Decimal part
            else {
                div = div * 10;
                res = res + (float)(*ptr - '0') / div;
            }
        }
        else if (*ptr == '.') {
            afterDecimalPoint = true;
        }
        else {
            break;
        }
        
        ptr++;
    }
    if (endptr) *endptr = ptr;
    return res;
}
#endif
