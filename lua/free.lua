if arg[1] == "-h" then
	print("Usage: free [-m] [-k]")
	return
end
function printf(...)
	print(string.format(...))
end
divisor = arg[1] == "-m" and 1024*1024 or arg[1] == "-k" and 1024 or 1
printf("%16s%8s%8s", "total", "used", "free")
used, total = os.free()
used = math.floor(math.floor(used * 1024) / divisor)
total = math.floor(math.floor(total * 1024) / divisor)
printf("Mem:    %8d%8d%8d", total+1, used+1, total - used+1)
