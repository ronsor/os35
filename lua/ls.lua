if arg[1] == "-h" then
	print("Usage: ls [-l] [path]")
	return
end

if #arg > 0 and arg[1] ~= "-l" or #arg > 1 then path = arg[#arg] else path = "." end
if os.stat(path) == nil then
	print("No such file or directory.")
	os.exit(1)
end
for ct, v in ipairs(os.listdir(path)) do
	if arg[1] == "-l" then
		vs = os.stat(path .. "/" .. v)
		ua = vs.attr == "sys" and "r-xr-xr-x" or vs.attr == "rdonly" and "rwxrwxr-x" or "rwxrwxrwx"
		io.stdout:write(string.format("%s %d %s", ua, vs.size, v).."\n")
	else
		io.stdout:write(("%12s"):format(v)..(ct % 6 == 0 and "\n" or " "))
	end
end
io.stdout:write("\n")
