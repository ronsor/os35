print("PID UID STATUS     NAME")
for k, v in pairs(os.processes()) do
	print(string.format("%3d %3d %10s %s", v.pid, v.uid, v.status, v.name))
end
