print("\x1B[2J")
io.stdout:write(string.format("\x1B[24;0HESC%8s C^S%8s", "Exit", "Save"))
io.stdout:write("\x1B[1;1H")
while true do
	c = io.stdin:read(1, 'nonblock')
	if c == '\x1B' then
		print("\x1B[2J")
		os.exit(0)
	end
end
