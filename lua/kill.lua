if arg[1] == "-h" then
	print("Usage: kill [-z] pid")
	os.exit(0)
end
pid = arg[1] == "-z" and tonumber(arg[2]) or tonumber(arg[1])
os.kill(pid)
if arg[1] == "-z" then os.wait(pid) end
