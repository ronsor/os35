function dofile(n)
	local f = io.open(n):read('*a')
	if f:byte(4) == 0 then
		fn, err = load(ex.decompress(f), n)
	else
		fn, err = load(f, n)
	end
	if err ~= nil then print(err) end
	fn()
end
print(pcall(function() dofile('%bin/kernel') end))
