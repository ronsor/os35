if #arg == 0 then
	for k, v in pairs(os.mounts()) do
		print(table.concat(v, " "))
	end
	return
end
if arg[1] == "-h" then
	print("Usage: mount [-t vfs disk mountpoint]")
	return
end
