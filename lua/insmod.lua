if #arg == 0 or arg[1] == "-h" then
	print("Usage: insmod (-list | path type)")
	return
end
if arg[1] == "-list" then
	for k, v in pairs(os.listmod()) do
		print(string.format("%d: 0x%x (type %s), size %d bytes", k, v.base, v.type, v.data:len()))
	end
	return
end
if not arg[2] then arg[2] = "unknown" end
f = io.open(arg[1])
if f == nil then
	print("Failed to open module file")
	os.exit(1)
end
local id, base, status = os.loadmod(f:read('*a'), arg[2])
print(string.format("Loaded at base %x (status code %d), module %d", base, status, id))
f:close()
