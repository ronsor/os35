if #arg ~= 2 then
	print("Usage: chmod +s|-s|+p|-p file")
	return
end
if os.chmod(arg[2], arg[1] == "+s" and "sys" or arg[1] == "+p" and "rdonly" or "") > 0 then
	print("chmod failed")
end
