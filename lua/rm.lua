if arg[1] == "-h" then
	print("Usage: rm [-rf] [-r] [-f] path")
	return
end
force = arg[1] == "-f" or arg[1] == "-rf"
recursive = arg[1] == "-r" or arg[1] == "-rf"
if force or recursive then table.remove(arg, 1) end
for k, v in pairs(arg) do
	ret, err = os.remove(v)
	if not ret and not force then
		print(v .. ": " .. err)
	end
end
