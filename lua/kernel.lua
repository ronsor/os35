proctbl = {}
info = {
	"RonsorOS",
	"<hostname>",
	"3.5",
	"lua5.3",
	_KERNELDATE,
	"RNU/Ronsor"
}
vfstypes = {}
dofile("%bin/libvfs")
function decomp(s)
	return s:byte(4) == 0 and ex.decompress(s) or s
end
function loadlua(c, n, e)
	local r, e = load(decomp(c), n, "bt", e) 
	if r == nil then error(e) end
	return r
end
print("Kernel loaded.")
mods = {}
modbase = 0
function loadmod(data)
	modbase2 = ex.modload(modbase, data)
	table.insert(mods, { base = modbase2, data = data })
	modbase = modbase + data:len() + 32
	return #mods, mods[#mods].base
end
bootpath = tostring(bootdrv == 0 and 0 or bootdrv - 0x80+1)
ex.mount(bootpath .. ":")
bootpath = "/mnt/d" .. bootpath
init = "/bin/sh"
vfs = {
	{bootpath, FATvfs(bootdrv == 0 and 0 or bootdrv - 0x80+1, bootpath), "fatfs", bootpath:gsub("%D", "")}
}
function getvfs(p)
	for k, v in pairs(vfs) do
		if p:find("^" .. v[1]) or p == v then
			return v[2]
		end
	end
	return getvfs(bootpath)
end
function deepcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[deepcopy(orig_key)] = deepcopy(orig_value)
        end
        setmetatable(copy, deepcopy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end
function jp(a)
	return pathjoin(curproc.cwd, a)
end
function cgetkey()
	local keycode = 0
	while keycode == 0 do keycode = ex.getkey(); coroutine.yield() end
	return keycode, string.char(keycode)
end
newstdin = {
	read = function(f,c,o)
		if o == 'nonblock' then
			coroutine.yield()
			return string.char(ex.getkey());
		end
		if c == "*l" then
			local outs = {}
			while true do
				local key, char = cgetkey()
				if key == 13 or key == 10 then
					io.stdout:write("\n")
					break
				elseif key == 8 then
					if #outs > 0 then
						io.stdout:write("\b \b")
						table.remove(outs)
					end
				else
					io.stdout:write(char)
					table.insert(outs, char)
				end
			end
			return table.concat(outs)
		else
			local outs = {}
			for n = 1,c do
				local key, char = cgetkey()
				table.insert(outs, char)
			end
			return table.concat(outs)
		end
	end
}
sandbox = {
	io = {
		stdin = newstdin,
		stdout = io.stdout,
		open = function(p,m)
			return getvfs(jp(p)):open(jp(p),m)
		end
	},	
	string = string, table = table, coroutine = coroutine,
	math = math, debug = debug,
 	os = {
		boottime = os.time(),
		info = info,
		time = function() return systime end,
		date = os.date,
		environ = {},
		getenv = function(a)
			return curproc.env.os.environ[a]
		end,
		listdir = function(p)
			coroutine.yield()
			return getvfs(jp(p)):listdir(jp(p))
		end,
		rename = function(p, q)
			coroutine.yield()
			return getvfs(jp(p)):rename(jp(p),jp(q))
		end,
		mounts = function()
			local out = {}
			for k, v in pairs(vfs) do table.insert(out, {v[1], v[3], v[4]}) end
			return out
		end,
		mkdir = function(p)
			coroutine.yield()
			return getvfs(jp(p)):mkdir(jp(p))
		end,
		remove = function(p)
			coroutine.yield()
			return getvfs(jp(p)):remove(jp(p))
		end,
		stat = function(p)
			coroutine.yield()
			return getvfs(jp(p)):stat(jp(p))
		end,
		spawn = function(s, a)
			coroutine.yield()
			local success, pid = pcall(spawnf, jp(s))
			if success == false or pid == nil then return nil end
			local pt = proctbl[pid]
			pt.env.arg = a or {}
			pt.cwd = curproc.cwd
			pt.uid = curproc.uid
			pt.env.io.stdout = curproc.env.io.stdout
			pt.env.io.stdin = curproc.env.io.stdin
			pt.env.os.environ = deepcopy(curproc.env.os.environ)
			return pid
		end,
		kill = function(pid)
			if proctbl[pid] then
				proctbl[pid].thread = nil
				proctbl[pid]._EXITCODE = 33
			end
		end,
		free = function()
			return collectgarbage("count"), 2048
		end,
		processes = function()
			local out = {}
			for k, v in pairs(proctbl) do
				table.insert(out, { name = v.name, status = v.thread and coroutine.status(v.thread) or "dead", pid = k, cwd = v.cwd, uid = v.uid})
			end
			return out
		end,
		spawnp = function(s, a)
			if s:find("%/") then
				return curproc.env.os.spawn(s, a)
			end
			for dir in curproc.env.os.environ.PATH:gmatch("([^%:]+)") do
				local ret = curproc.env.os.spawn(pathjoin(dir, s), a)
				if ret ~= nil then return ret end
			end
			return nil
		end,
		wait = function(s, a)
			coroutine.yield()
			if a then
				return proctbl[s]._EXITCODE ~= -1 and proctbl[s]._EXITCODE or nil
			else
				while proctbl[s]._EXITCODE == -1 do coroutine.yield() end
				local ret = proctbl[s]._EXITCODE
				proctbl[s]._EXITCODE = nil
				return ret
			end
		end,
		reboot = function()
			os.execute("reboot")
		end,
		exit = function(code)
			curproc._EXITCODE = code or 0
			curproc.thread = nil
			coroutine.yield()
		end,
		chdir = function(p)
			curproc.cwd = jp(p)
		end,
		getcwd = function() return curproc.cwd end,
		loadmod = function(data, type)
			local id, base = loadmod(data)
			mods[id].type = type
			status = ex.modcall(0, base, 0)
			return id, base, status
		end,
		listmod = function() return mods end
	},
	print = function(...)
		curproc.env.io.stdout:write(table.concat({...}, " ") .. "\n")
	end,
	error = error, pcall = pcall, tostring = tostring, tonumber = tonumber,
	load = load, assert = assert, pairs = pairs, ipairs = ipairs,
	next = next,
}
function pathjoin(a,b)
	local parts = {}
	for part in b:gmatch("([^%/]+)") do
		if part == "." or part == "" then
		elseif part == ".." then
			table.remove(parts)
		else
			table.insert(parts, part)
		end
	end
	local out = table.concat(parts, "/")
	if not b:find("^%/") then
		out = a .. "/" .. out
	else
		out = "/" .. out
	end
	return out
end
function spawn(code, name)
	local env = deepcopy(sandbox)
	table.insert(proctbl, { cwd = bootpath, name = name, _EXITCODE = -1, thread = coroutine.create(function()
		loadlua(code, name, env)()
	end), env = env, uid = 0 })
	return #proctbl
end
function spawnf(name)
	local s, f = pcall(function() s = getvfs(name):open(name); q = s:read('*a'); s:close(); return q end)
	if s == false then
		return nil
	end
	return spawn(f, name)
end
spawnf(init)
while true do
for k, v in pairs(proctbl) do
	pcall(function() systime = os.time() end)
	curpid = k
	curproc = v
	if v.thread and coroutine.status(v.thread) == 'dead' and proctbl[k]._EXITCODE == -1 then
		proctbl[k]._EXITCODE = 0
	end
	if (v.thread == nil or coroutine.status(v.thread) == 'dead') and proctbl[k]._EXITCODE == nil then
		proctbl[k] = nil
	elseif v.thread and coroutine.status(v.thread) ~= 'dead' then
		local success, err = coroutine.resume(v.thread)
		if success == false then
			proctbl[k]._EXITCODE = 32
			proctbl[k].env.io.stdout:write(err .. "\n")
		end
	end
end
end
