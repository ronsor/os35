print('VFS loaded.')
function FATvfs(drvno, path)
	local drvstr = tostring(drvno) .. ":/"
	local strippath = function(p)
		return string.gsub(p, "^" .. path, ""):gsub("^".. path .."/",""):gsub("^/","")
	end
	local errcodes = {
		"Disk I/O error", "Internal error", "Disk not ready",
		"No such file or directory", "No such file or directory",
		"Bad pathname", "No space left on device", "File exists", "Bad address",
		"Operation not permitted", "No such device",
		"Disk not mounted", "Filesystem corrupt",
		"Mkfs: failed", "Timeout", "Text file busy",
		"Out of memory", "Out of memory"
	}
	local errtr = function(a)
		if a == 0 then return true end
		return false, errcodes[a] or "Unknown error"
	end
	return {
		stat = function(me,p)

			p = strippath(p)
			if p == "" or p == "/" then
				return {size = 0, dir = true, attrib = ""}
			end
			local size, isdir, attrib = ex.stat(drvstr .. p)
			return size and {size = size, dir = isdir, attr = attrib} or nil
		end,
		chmod = function(me,p,z)
			return errtr(ex.chmod(drvstr .. strippath(d), z))
		end,
		mkdir = function(me,d)
			return errtr(ex.mkdir(drvstr .. strippath(d)))
		end,
		remove = function(me,d)
			return errtr(ex.remove(drvstr .. strippath(d)))
		end,
		listdir = function(me,d)
			local out = {}
			local str = ex.rawlistdir(drvstr .. strippath(d))
			for v in str:gmatch("([^\n]+)") do
				if v ~= "" then table.insert(out, v) end
			end
			return out
		end,
		open = function(me,p,m)
			return io.open(drvstr .. strippath(p), m)
		end,
		rename = function(me,a,b)
			return errtr(os.rename(drvstr .. strippath(a), drvstr .. strippath(b)))
		end
	}
end
