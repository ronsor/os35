local SQ = 0x27 -- U+0027 APORSTROPHE
local DQ = 0x22 -- U+0022 QUOTATION MARK
local SP = 0x20 -- U+0020 SPACE
local HT = 0x09 -- U+0009 CHARACTER TABULATION
local LF = 0x0A -- U+000A LINE FEED (LF)
local CR = 0x0D -- U+000D CARRIAGE RETURN (CR)
local BS = 0x5C -- U+005C REVERSE SOLIDUS

local function split(s)
  local token
  local state
  local escape = false
  local result = {}
  for i = 1, #s do
    local c = s:byte(i)
    local v = string.char(c)
    if state == SQ then
      if c == SQ then
        state = nil
      else
        token[#token + 1] = v
      end
    elseif state == DQ then
      if escape then
        if c == DQ or c == BS then
          token[#token + 1] = v
        else
          token[#token + 1] = "\\"
          token[#token + 1] = v
        end
        escape = false
      else
        if c == DQ then
          state = nil
        elseif c == BS then
          escape = true
        else
          token[#token + 1] = v
        end
      end
    else
      if escape then
        token[#token + 1] = v
        escape = false
      else
        if c == SP or c == HT or c == LF or c == CR then
          if token ~= nil then
            result[#result + 1] = table.concat(token)
            token = nil
          end
        else
          if token == nil then token = {} end
          if c == SQ then
            state = SQ
          elseif c == DQ then
            state = DQ
          elseif c == BS then
            escape = true
          else
            token[#token + 1] = v
          end
        end
      end
    end
  end

  if state ~= nil then
    error "no closing quotation"
  end
  if escape then
    error "no escaped character"
  end

  if token ~= nil then
    result[#result + 1] = table.concat(token)
  end
  return result
end

if os.environ["PS1"] == nil then
	os.environ.PS1 = "$ "
	os.environ.PATH = "/bin"
end
builtins = {
	setenv = function(a) os.environ[a[1]] = a[2] end,
	echo = function(a) io.stdout:write(table.concat(a, ' ') .. "\n") end,
	debug = debug.debug,
	cd = function(a) os.chdir(a[1]) end,
	abort = function() error("Abort") end,
	exit = function(a) os.exit(a[1]) end,
	help = function(a)
		if a[1] then print(help[a[1]] or "No such command"); return end
		for k, v in pairs(help) do
			print(k .. " - " .. v)
		end
	end
}
help = {
	setenv = "Usage: setenv variable value",
	echo = "Usage: echo message",
	debug = "This is for debugging purposes only!",
	cd = "Usage: cd dir",
	abort = "Abort the shell (equivalent to C abort())",
	exit = "Usage: exit [status]",
	help = "Usage: help [topic]"
}
istty = true
sfile = io.stdin
if arg and arg[1] ~= nil then
	sfile = io.open(arg[1])
	if sfile == nil then
		print("No such file or directory.")
		os.exit(1)
	end
	istty = false
end
while true do
	if istty then io.stdout:write(os.environ.PS1) end
	coroutine.yield()
	line = sfile:read('*l')
	if line == nil then return end
	line = line:gsub("$([%w%?_]+)", function(a) return os.environ[a] or "" end)
	if line ~= "" then
		args = split(line)
		bg = args[#args] == "&" and table.remove(args) or false
		if builtins[args[1]] ~= nil then
			if args[2] == "-h" then
				builtins.help({args[1]})
			else
				builtins[args[1]](#args > 1 and {table.unpack(args, 2, #args)} or {})
			end
		else
			pid = os.spawnp(args[1], #args > 1 and {table.unpack(args, 2, #args)} or nil)
			if pid ~= nil and bg == false then
				os.environ["?"] = os.wait(pid)
			elseif pid ~= nil then
				if istty then print(pid, "&") end
			else
				io.stdout:write("sh: " .. args[1] .. ": command not found\n")
			end
		end
	end
end
