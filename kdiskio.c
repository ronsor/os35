#include <stdio.h>
#ifdef __SMALLER_C__
int _raw_floppy_read_sector(int d, int s, int c, int h, unsigned int bufseg, unsigned int bufoff, char *buf) {
	asm(	"mov dl,[bp+8]\n"
		"mov al, 1\n"
		"push es\n"
		"mov bx, [bp+24]\n"
		"mov es, bx\n"
		"mov bx, [bp+28]\n"
		"mov cl,[bp+12]\n"
		"mov ch,[bp+16]\n"
		"mov ah, 2\n"
		"mov dh,[bp+20]\n"
		"int 0x13\n"
		"pop es\n"
		"mov [bp+8], ah"
	);
	char *ptr = getbuffer();
	memcpy(buf, ptr, 512);
	return d;
}
int raw_floppy_read_sector(int d, int s, char *buf) {
	int c, h, s2, t;
	t = s / 18;
	s2 = (s % 18) + 1;
	h = t % 2;
	c =  t / 2;
	//printf("addr: %04x:%04x\n\n", getbuffer()>>16, (unsigned short)getbuffer());
	return _raw_floppy_read_sector(d, s2, c, h, getbuffer()/16, getbuffer()%16, buf);
}
#pragma pack()
struct __lba_op {
	unsigned char size;
	unsigned char zero;
	unsigned short num;
	unsigned short off, seg;
	unsigned long lbastart;
	unsigned long lbastart_2;
};
int lba_read_sector(int d, unsigned long s, char *buf) {
	struct __lba_op cmd = { 0x10, 0, 1, 0x0, 0x0, 0, 0 };
	cmd.off = getbuffer()%16;
	cmd.seg = getbuffer()/16;
	cmd.lbastart = s;
	memcpy(0x500, &cmd, 16);
	asm(	"push ds\n"
		"mov si, 0x500\n"
		"mov ah, 0x42\n"
		"mov dl, 0x80\n"
		"mov bx, 0x0\n"
		"mov ds, bx\n"
		"int 0x13\n"
		"mov [bp+8], ah\n"
		"pop ds"
	);
	//memcpy(&cmd, 0x5000+512, 16);
	memcpy(buf, getbuffer(), 512);
	return d;
}
int lba_write_sector(int d, unsigned long s, char *buf) {
	struct __lba_op cmd = { 0x10, 0, 1, 0x0, 0x0, 0, 0 };
	cmd.lbastart = s;
	cmd.off = getbuffer()%16;
	cmd.seg = getbuffer()/16;
	memcpy(getbuffer(), buf, 512);
	memcpy(0x500, &cmd, 16);
	asm(	"push ds\n"
		"mov si, 0x500\n"
		"mov ah, 0x43\n"
		"mov dl, 0x80\n"
		"mov bx, 0x0\n"
		"mov ds, bx\n"
		"int 0x13\n"
		"mov [bp+8], ah\n"
		"pop ds"
	);
	return d;
}
int lba_getsectors(int d) {
	*(short*)(0x500) = 0x1E;
	asm(	"push ds\n"
		"mov bx, 0x0\n"
		"mov si, 0x500\n"
		"mov ds, bx\n"
		"mov ah, 0x48\n"
		"mov dl, [bp+8]\n"
		"int 0x13\n"
		"pop ds"
	);
	return *(long*)(0x500+0x10);
}
#endif
