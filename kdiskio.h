#ifndef KDISKIOH
#define KDISKIOH
int lba_read_sector(int d, unsigned long s, char *buf);
int lba_write_sector(int d, unsigned long s, char *buf);
int raw_floppy_read_sector(int d, int s, char *buf);
#endif
