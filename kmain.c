#include <stdio.h>
#include <lua.h>
#include <lauxlib.h>
#include <time.h>
#include "kdiskio.h"
#include "video.h"
#include <time.h>
char __genericbuf[1024];
char *getbuffer() { return __genericbuf; }
unsigned char *bootdrv_ptr = 0x500;
unsigned char bootdrv;
int getbootdrv() { return bootdrv; }
void exit(int code) {
	printf("Exited: %d", code);
	while(1);
}
void setlocale() {}

void abort() {
	printf("Aborted.");
	while (1);
}
char buf[512];
int main() {
	bootdrv = *bootdrv_ptr;
	add_malloc_block(2048576, 2048576);
	#ifdef EARLYFS
	earlyfs_init();
	#else
	char p[3] = {'0', ':', 0};
	*p = bootdrv > 0 ? bootdrv - 0x80 : 0;
	printf("status(diskmount)=%d\n", disk_mount(p));
	#endif
	printf("genericbuf_base=0x%x\n", __genericbuf);
	//struct video_mode m;
	//m.screen[0] = 0xFFFFFF;
	//printf("%d\n", SetVideoModeEx(800, 600, 32, &m));
	lua_State* L = luaL_newstate();
	luaL_openlibs(L);
	add_extras(L);
	luaL_dofile(L, "%init.lua");
	exit(0);
}
#ifdef __SMALLER_C__
#include "a20.c"
void __start__() { EnableA20(); asm("fninit"); main(); }
int __Irq5Isr() { return 1; }
#endif
