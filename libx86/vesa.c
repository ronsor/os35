#include <stdio.h>
#include "vesa.h"
#include "video.h"
int GetVbeInfo(tVbeInfo* p) {
  asm(
  "mov ax, 0x4f00\n"
  "mov edi, [bp+8]\n"
  "ror edi, 4\n"
  "mov es, di\n"
  "shr edi, 28\n"
  "int 0x10\n"
  "movzx eax, ax\n"
  "push word 0\n"
  "pop es"
  );
}
int GetVbeModeInfo(tVbeModeInfo* p, uint16 mode) {
  asm(
  "mov ax, 0x4f01\n"
  "mov cx, [bp+12]\n"
  "mov edi, [bp+8]\n"
  "ror edi, 4\n"
  "mov es, di\n"
  "shr edi, 28\n"
  "int 0x10\n"
  "movzx eax, ax\n"
  "push word 0\n"
  "pop es"
  );
}
int SetVbeMode(uint16 mode) {
  asm(
  "mov ax, 0x4f02\n"
  "mov bx, [bp+8]\n"
  "int 0x10\n"
  "movzx eax, ax"
  );
}
int ProbeVideo(video_modes out) {
  int i2 = 0;
  for (int i = 0; i < 0xFFF && i2 < 64; i++) {
    tVbeModeInfo info;
    if (GetVbeModeInfo(&info, i) == 0x4F)
      if (!out) printf("Mode %dx%dx%d, linear=%d\n", info.XResolution, info.YResolution, info.BitsPerPixel, info.ModeAttributes&0x80); else if (info.ModeAttributes&0x80)
	{ out[i2].width = info.XResolution; out[i2].height = info.YResolution; out[i2].bpp = info.BitsPerPixel; out[i2].id = i; 
		out[i2].screen = info.PhysBasePtr; i2++; }
  }
}
int SetVideoMode(struct video_mode* vmode) {
  return !SetVbeMode(vmode->id | (1 << 14));
}
