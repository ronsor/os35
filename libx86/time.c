#include <time.h>
typedef unsigned char bcd;
static int bcd2int(unsigned char x) {
    return x - 6 * (x >> 4);
}
struct bios_time {
	bcd year, month, day;
	bcd hour, minute, second;
};
static struct bios_time _time;
void bios_get_datetime(bcd y, bcd m, bcd d, bcd h, bcd m2, bcd s, int flg) {
	flg = 1;
	while (flg) {
	asm("mov ah,4\nint 0x1a\nmov [bp+8], cl\n"
		"mov [bp+12], dh\nmov [bp+16], dl\n");
	asm("mov ah,2\nint 0x1a\nmov [bp+20], ch\n"
		"mov [bp+24], cl\nmov [bp+28], dh\nmov [bp+32], ah\n");
	}
	_time.year = y; _time.month = m; _time.day = d;
	_time.hour = h; _time.minute = m2; _time.second = s;
}
time_t systemtime() {
	struct tm x;
	bios_get_datetime(0,0,0,0,0,0,0);
	x.tm_sec = bcd2int(_time.second) - 1;
	x.tm_min = bcd2int(_time.minute) - 1;
	x.tm_hour = bcd2int(_time.hour) - 1;
	x.tm_mday = bcd2int(_time.day);
	x.tm_mon = bcd2int(_time.month) - 1;
	x.tm_year = bcd2int(_time.year) + 100;
	return mktime(&x);
}


