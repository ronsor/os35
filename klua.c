#include <lua.h>
#include <lauxlib.h>
#include "video.h"
#include "libz/lz.h"
#include "stat.h"
#include "z80/zemu.h"
char console_getc_nonblock();
static struct video_mode vmode;
int video_listmodes(lua_State *L) {
	char out[2048];
	out[0] = 0;
	video_modes vm;
	ProbeVideo(vm);
	for (int i = 0; i < 64; i++) {
		if (vm[i].width < 4096 && vm[i].height < 4096) sprintf(out, "%s%d %d %d %d\n", out, vm[i].width, vm[i].height, vm[i].bpp, vm[i].id);
	}
	lua_pushstring(L, out);
	return 1;
}
int console_getkey(lua_State *L) {
	char c = console_getc_nonblock()&0xFF;
	lua_pushinteger(L, c);
	return 1;
}
int video_setmode(lua_State *L) {
	lua_pushboolean(L, SetVideoModeEx(luaL_checkinteger(L, 1), luaL_checkinteger(L, 2), luaL_checkinteger(L, 3), &vmode));
	return 1;
}
int video_putpixel(lua_State *L) {
	vmode.screen[(luaL_checkinteger(L, 2) * vmode.width) + luaL_checkinteger(L, 1)] = luaL_checkinteger(L, 3);
	return 0;
}
int video_bitblt(lua_State *L) {
	size_t sz;
	unsigned int *ptr = luaL_checklstring(L, 3, &sz);
	int yb = luaL_checkinteger(L, 2);
	int xb = luaL_checkinteger(L, 1);
	int vw = luaL_checkinteger(L, 4);
	int vh = luaL_checkinteger(L, 5);
	#define VADDR(X,Y) vmode.screen[((Y) * vmode.width) + (X)]
	for (int y = 0; y < vh; y++)
		for (int x = 0; x < vw; x++)
			VADDR(x+xb, y+yb) = ptr[(y * vw) + x];
	return 0;
}
int libz_decompress(lua_State *L) {
	size_t sz;
	char *buf = luaL_checklstring(L, 1, &sz);
	if (sz < 4) {
		lua_pushnil(L); return 1;
	}
	int outsz = *(long*)buf;
	char *out = malloc(outsz);
	LZ_Uncompress(buf+4, out, sz);
	lua_pushlstring(L, out, outsz);
	free(out);
	return 1;
}
int libz_compress(lua_State *L) {
	size_t sz;
	char *buf = luaL_checklstring(L, 1, &sz);
	char *out = malloc(sz+16);
	*(long*)out = sz;
	int outsz = LZ_Compress(buf, out+4, sz);
	lua_pushlstring(L, out, outsz+4);
	free(out);
	return 1;
}
int io_stat(lua_State *L) {
	struct stat s;
	if (stat(&s, luaL_checkstring(L, 1))) {
		lua_pushnil(L);
		return 1;
	}
	lua_pushinteger(L, s.size);
	lua_pushboolean(L, s.dir);
	lua_pushstring(L, s.attr);
	return 3;
}
int misc_alloc(lua_State *L) {
	lua_pushinteger(L, malloc(luaL_checkinteger(L, 1)));
	return 1;
}
int misc_writemem(lua_State *L) {
	size_t sz;
	memcpy(luaL_checkinteger(L, 1), luaL_checklstring(L, 2, &sz), sz);
	return 0;
}
int misc_readmem(lua_State *L) {
	lua_pushlstring(L, luaL_checkinteger(L, 1), luaL_checkinteger(L, 2));
	return 1;
}
int misc_memcpy(lua_State *L) {
	memcpy(luaL_checkinteger(L, 1), luaL_checkinteger(L, 2), luaL_checkinteger(L, 3));
	return 0;
}
int misc_free(lua_State *L) {
	free(luaL_checkinteger(L, 1));
	return 0;
}
int misc_peekb(lua_State *L) {
	lua_pushinteger(L, *(unsigned char*)(luaL_checkinteger(L,1)));
	return 1;
}
int misc_peeks(lua_State *L) {
	lua_pushinteger(L, *(unsigned short*)(luaL_checkinteger(L,1)));
	return 1;
}
int misc_peekw(lua_State *L) {
	lua_pushinteger(L, *(unsigned int*)(luaL_checkinteger(L,1)));
	return 1;
}
int misc_pokeb(lua_State *L) {
	*(unsigned char*)(luaL_checkinteger(L,1)) = luaL_checkinteger(L,2);
	return 0;
}
int misc_pokes(lua_State *L) {
	*(unsigned short*)(luaL_checkinteger(L,1)) = luaL_checkinteger(L,2);
	return 0;
}
int misc_pokew(lua_State *L) {
	*(unsigned int*)(luaL_checkinteger(L,1)) = luaL_checkinteger(L,2);
	return 0;
}
/*
int misc_loadmod(lua_State *L) {
	size_t len;
	char *buf = luaL_checklstring(L,2,&len);
	ZEMU* emu = malloc(sizeof(ZEMU));
	memset(emu, 0, sizeof(emu));
	memcpy(emu->memory + 0x100, buf, len);
	Z80Reset(&emu->state);
	emu->state.pc = 0x100;
	lua_pushinteger(L, emu);
	lua_pushinteger(L, emu->memory);
	return 1;
}
void SystemCall(ZEMU *emu) {}
int misc_modcall(lua_State *L) {
	ZEMU *emu = luaL_checkinteger(L, 1);
	int cycles = luaL_checkinteger(L, 2);
	Z80Emulate(&emu->state, cycles, emu);
	return 0;
}*/
int io_mkdir(lua_State *L) {
	lua_pushinteger(L, mkdir(luaL_checkstring(L, 1)));
	return 1;
}
int io_remove(lua_State *L) {
	lua_pushinteger(L, remove(luaL_checkstring(L, 1)));
	return 1;
}
int io_disksize(lua_State *L) {
	int drive = luaL_checkinteger(L, 1);
	lua_pushinteger(L, lba_getsectors(drive));
	return 1;
}
int io_mkfs(lua_State *L) {
	lua_pushinteger(L, mkfs(luaL_checkstring(L,1)));
	return 1;
}
int io_diskwrite(lua_State *L) {
	int drive = luaL_checkinteger(L, 1);
	int sector = luaL_checkinteger(L, 2);
	size_t buflen;
	char *buf = luaL_checklstring(L, 3, &buflen);
	if (buflen < 512) {
		return 0;
	}
	lua_pushinteger(L, lba_write_sector(drive, sector, buf));
	return 1;
}
int io_diskread(lua_State *L) {
	int drive = luaL_checkinteger(L, 1);
	long sector = luaL_checkinteger(L, 2);
	char buf[512];
	int status = lba_read_sector(drive, sector, buf);
	if (!status) lua_pushlstring(L, buf, 512);
	return 1;
}
int io_mount(lua_State *L) {
	lua_pushinteger(L, disk_mount(luaL_checkstring(L,1)));
	return 1;
}
int io_rawlistdir(lua_State *L) {
	char *path = luaL_checkstring(L, 1);
	char *out;
	int ret = listdir(path, &out);
	lua_pushstring(L,out);
	lua_pushinteger(L,ret);
	free(out);
	return 2;
}
void add_extras(lua_State *L) {
	static struct luaL_Reg extras[] = {
		{ "lbaread", io_diskread},
		{ "mkfs", io_mkfs},
		{ "disksize", io_disksize},
		{ "lbawrite", io_diskwrite},
		{ "mount", io_mount},
		{ "setvmode", video_setmode},
		{ "rawlistvmodes", video_listmodes},
		{ "rawlistdir", io_rawlistdir},
		{ "mkdir", io_mkdir},
		{ "malloc", misc_alloc},
		{ "free", misc_free},
		{ "pokeb", misc_pokeb},
		{ "pokew", misc_pokew},
		{ "pokes", misc_pokes},
		{ "peekb", misc_peekb},
		{ "peekw", misc_peekw},
		{ "peeks", misc_peeks},
		{ "memcpy", misc_memcpy},
		{ "readmem", misc_readmem},
		{ "writemem", misc_writemem},
		{ "putpixel", video_putpixel},
		{ "bitblt", video_bitblt},
		{ "compress", libz_compress},
		{ "decompress", libz_decompress},
		{ "getkey", console_getkey},
		{ "stat", io_stat},
		{ "remove", io_remove},
//		{ "modload", misc_loadmod},
//		{ "modcall", misc_modcall},
		{ NULL, NULL} };
	luaL_newlib(L, extras); lua_setglobal(L, "ex");
	lua_pushinteger(L, getbootdrv());
	lua_setglobal(L, "bootdrv");
	lua_pushstring(L, "(" __DATE__ " " __TIME__ ")");
	lua_setglobal(L, "_KERNELDATE");

}
