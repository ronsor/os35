#ifndef VIDEO_H
#define VIDEO_H
typedef unsigned long pixel; // 24bpp
typedef unsigned short pixel16; // 16bpp
typedef unsigned char pixel8_rgb; // 8bpp rgb falsecolor
struct video_mode {
	int width; int height;
	int id; int bpp;
	pixel* screen;
};
typedef struct video_mode video_modes[64];
int ProbeVideo(video_modes);
int SetVideoMode(struct video_mode*);
static int SetVideoModeEx(int width, int height, int bpp, struct video_mode* out) {
	video_modes vm;
	ProbeVideo(vm);
	for (int i = 0; i < 64; i++) {
		if (vm[i].width == width && vm[i].height == height && vm[i].bpp == bpp) {
			SetVideoMode(&vm[i]);
			memcpy(out, &vm[i], sizeof(struct video_mode));
			return 1;
		}
	}
	return 0;
}
#endif
