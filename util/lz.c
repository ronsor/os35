#include "../libz/lz.h"
#include "../libz/lz.c"
#include <stdio.h>
char buf[1048576];
char buf2[1048577];
int main(int argc, char **argv) {
	int len = fread(buf, 1, sizeof(buf), stdin);
	if (argv[1][1] == 'c') {
		*(long*)buf2 = len;
		len = LZ_Compress(buf, buf2+4, len) + 4;
	} else {
		LZ_Uncompress(buf+4, buf2, len-4);
		len = *(long*)buf;
	}
	fwrite(buf2, len, 1, stdout);
}
