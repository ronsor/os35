#include <stddef.h>
#include <stdio.h>
#ifdef __SMALLER_C__
int console_ready() {
	asm("xor eax, eax\nmov ah,0x01\nint 0x16\njnz .done\n");
	asm("xor eax, eax\n.done:");
}
void console_movexy(int line, int col) {
	asm("mov ah, 0x02\nmov bh, 0\nmov dh, [bp+8]\nmov dl, [bp+12]\nint 0x10");
}
void console_clear() {
	asm("mov ah, 0x0\nmov al, 0x03\nint 0x10");
}
char console_getc_nonblock() {
	return console_ready() ? console_getc() : 0;
}
void console_putc(char c) {
	asm("mov ah,0x0E\nmov al,[bp+8]\nint 0x10");
}
char console_getc() {
	asm("mov ah,0x00\nint 0x16\nmov ah,0");
}
#endif
char console_getc_buffered() {
	static char buf[256];
	static int bufpos, buflen;
	char c;
	if (buflen > 0) {
		c = buf[bufpos++];
		buflen--;
		return c;
	}
	bufpos = 0;
	do {
		c = console_getc();
		if (c == 0x08) { console_putc(0x08); console_putc(' '); console_putc(0x08);
			if (buflen > 0) buflen--; continue; }
		console_putc(c);
		if (c == 13) console_putc(10);
		buf[buflen++] = c;
		if (buflen > 250 || c == 13 || c == 10) break;
	} while(1);
	buflen--;
	return buf[bufpos++];
}
char conbuf[1024];
size_t conbuflen;
size_t console_flush(FILE *inst, const char *data, size_t len) {
	size_t olen = len;
	static int beg = 1;
	while (len--) {
		if (*data == '\n') console_putc('\r');
		if (*data == '\x1B') {
			if (*++data == '[') {
				char full[12];
				data++; len--;
				int ct = 0; char cmd;
				beg = 0;
				while (len > 0 && ct < 10) {
					full[ct++] = *data++;
					full[ct] = 0;
					len--;
					if (isalpha(full[ct-1])) {
						cmd = full[ct-1];
						break;
					}
				}
				beg = 1;
				switch(cmd) {
					case 'H': case 'f': {
						int line, col;
						if (sscanf(full, "%d;%d%c", &line, &col, &cmd) == 3)
						console_movexy(line, col);
						break; }
					case 'J':
						console_clear();
						break;
				}
			}
			continue;
		}
		console_putc(*data++);
	}
	return olen;
}
size_t console_read(FILE *inst, char *data, size_t len) {
	size_t olen = len;
	while (len--) {
		*data = inst->flags & F_RAW ? console_getc() : console_getc_buffered();
		if (*data == 13) *data = 10;
		data++;
	}
	return olen;
}
size_t console_write(FILE *inst, const char *data, size_t len) {
	/*int in = 0;
	for (int i = 0; i < len; i++)
		if (data[i] == '\x1B')
			in = 1;
		else if (isalpha(data[i]))
			in = 0;
	memcpy(conbuf, data, len);
	conbuflen += len;
	if (!in) {
		console_flush(inst, conbuf, conbuflen);
		conbuflen = 0;
	}*/
	console_flush(inst, data, len);
	return len;
}
struct File_methods _cfuncs = {
	&console_write,
	&console_read
};
FILE _conio = {
	&_cfuncs
};
FILE *stdout = &_conio;
FILE *stderr = &_conio;
FILE *stdin = &_conio;
