bits 16
__buffer:
times 4096 db 0
__start:
mov bx, cs
mov ds, bx
mov es, bx
mov ax, [ebp+8]
mov [_cmd], ax
call _modinit
xor bx, bx
mov es, bx
mov ds, bx
retf
__syscall:
mov ax, [bp+4]
mov [0x500], ax
call 0:50Fh
ret
bits 16

; glb cmd : int
section .bss
	alignb 2
	global	_cmd
_cmd:
	resb	2

; RPN'ized expression: "1024 "
; Expanded expression: "1024 "
; Expression value: 1024
; glb get_buffer : () * struct module_msg
section .text
_get_buffer:
	push	bp
	mov	bp, sp
	;sub	sp,          0
mov ax, __buffer
L1:
	leave
	ret

; glb bios_putc : (
; prm     c : char
;     ) void
section .text
	global	_bios_putc
_bios_putc:
	push	bp
	mov	bp, sp
	;sub	sp,          0
; loc     c : (@4): char
mov al, [bp+4]
mov ah, 0x0e
int 0x10
L3:
	leave
	ret

; glb modinit : () int
section .text
	global	_modinit
_modinit:
	push	bp
	mov	bp, sp
	;sub	sp,          0
; RPN'ized expression: "( 72 bios_putc ) "
; Expanded expression: " 72  bios_putc ()2 "
; Fused expression:    "( 72 , bios_putc )2 "
	push	72
	call	_bios_putc
	sub	sp, -2
; RPN'ized expression: "( 105 bios_putc ) "
; Expanded expression: " 105  bios_putc ()2 "
; Fused expression:    "( 105 , bios_putc )2 "
	push	105
	call	_bios_putc
	sub	sp, -2
; return
; RPN'ized expression: "cmd "
; Expanded expression: "cmd *(2) "
; Fused expression:    "*(2) cmd  "
	mov	ax, [_cmd]
L5:
	leave
	ret



; Syntax/declaration table/stack:
; Bytes used: 220/168960


; Macro table:
; Macro __SMALLER_C__ = `0x0100`
; Macro __SMALLER_C_16__ = ``
; Macro __SMALLER_C_SCHAR__ = ``
; Bytes used: 63/5120


; Identifier table:
; Ident __floatsisf
; Ident __floatunsisf
; Ident __fixsfsi
; Ident __fixunssfsi
; Ident __addsf3
; Ident __subsf3
; Ident __negsf2
; Ident __mulsf3
; Ident __divsf3
; Ident __lesf2
; Ident __gesf2
; Ident cmd
; Ident module_msg
; Ident datalen
; Ident data
; Ident <something>
; Ident get_buffer
; Ident bios_putc
; Ident c
; Ident modinit
; Bytes used: 201/129536

; Next label number: 7
; Compilation succeeded.
