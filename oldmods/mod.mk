$(MOD): $(MOD).c
	smlrpp -D__SMALLER_C__ -D_OSMODULE_ -o $(MOD).i.tmp $(MOD).c -I$(BASE)/.
	smlrc -seg16 $(MOD).i.tmp $(MOD).asm.tmp
	cat $(BASE)preamble.asm $(MOD).asm.tmp > $(MOD).asm
#	sed -i -E -e 's/mov.*sp, -(.*)/mov esp, -(\1+\1)/g' \
##		-e 's/push.*bp/push ebp/g' -e 's/mov.*bp, sp/mov ebp, esp/g' \
#		-e 's/bits 16/bits 32/g' $(MOD).asm
	nasm -f bin -o $(MOD).bin $(MOD).asm
	#rm -f $(MOD).asm.tmp
