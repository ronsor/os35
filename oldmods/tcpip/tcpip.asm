bits 16
__buffer:
times 4096 db 0
__start:
mov bx, cs
mov ds, bx
mov es, bx
mov ax, [ebp+8]
mov [_cmd], ax
call _modinit
xor bx, bx
mov es, bx
mov ds, bx
retf
__syscall:
mov ax, [bp+4]
mov [0x500], ax
call 0:50Fh
ret
bits 16

; glb cmd : int
section .bss
	alignb 2
	global	_cmd
_cmd:
	resb	2

; RPN'ized expression: "1024 "
; Expanded expression: "1024 "
; Expression value: 1024
; glb get_buffer : () * struct module_msg
section .text
_get_buffer:
	push	bp
	mov	bp, sp
	;sub	sp,          0
mov ax, __buffer
L1:
	leave
	ret

; glb u8_t : unsigned char
; glb u16_t : unsigned
; glb uip_stats_t : unsigned
; glb uip_log : (
; prm     msg : * char
;     ) void
; glb uip_init : (void) void
; RPN'ized expression: "1500 2 + "
; Expanded expression: "1502 "
; Expression value: 1502
; glb uip_buf : [1502u] unsigned char
; glb uip_listen : (
; prm     port : unsigned
;     ) void
; glb uip_unlisten : (
; prm     port : unsigned
;     ) void
; glb uip_connect : (
; prm     ripaddr : * unsigned
; prm     port : unsigned
;     ) * struct uip_conn
; glb uip_udp_new : (
; prm     ripaddr : * unsigned
; prm     rport : unsigned
;     ) * struct uip_udp_conn
; glb htons : (
; prm     val : unsigned
;     ) unsigned
; glb uip_appdata : * unsigned char
; glb uip_sappdata : * unsigned char
; glb uip_urgdata : * unsigned char
; glb uip_len : unsigned
; glb uip_slen : unsigned
; glb uip_urglen : unsigned char
; glb uip_surglen : unsigned char
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; RPN'ized expression: "4 "
; Expanded expression: "4 "
; Expression value: 4
; RPN'ized expression: "4 "
; Expanded expression: "4 "
; Expression value: 4
; RPN'ized expression: "128 "
; Expanded expression: "128 "
; Expression value: 128
; glb uip_conn : * struct uip_conn
; RPN'ized expression: "128 "
; Expanded expression: "128 "
; Expression value: 128
; glb uip_conns : [128u] struct uip_conn
; RPN'ized expression: "4 "
; Expanded expression: "4 "
; Expression value: 4
; glb uip_acc32 : [4u] unsigned char
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; glb uip_udp_conn : * struct uip_udp_conn
; RPN'ized expression: "128 "
; Expanded expression: "128 "
; Expression value: 128
; glb uip_udp_conns : [128u] struct uip_udp_conn
; glb uip_stat : struct uip_stats
; glb uip_flags : unsigned char
; glb uip_process : (
; prm     flag : unsigned char
;     ) void
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; RPN'ized expression: "4 "
; Expanded expression: "4 "
; Expression value: 4
; RPN'ized expression: "4 "
; Expanded expression: "4 "
; Expression value: 4
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; RPN'ized expression: "4 "
; Expanded expression: "4 "
; Expression value: 4
; glb uip_tcpip_hdr : struct <something>
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; glb uip_icmpip_hdr : struct <something>
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; glb uip_udpip_hdr : struct <something>
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; glb uip_hostaddr : [2u] unsigned
; glb uip_add32 : (
; prm     op32 : * unsigned char
; prm     op16 : unsigned
;     ) void
; glb uip_chksum : (
; prm     buf : * unsigned
; prm     len : unsigned
;     ) unsigned
; glb uip_ipchksum : (void) unsigned
; glb uip_tcpchksum : (void) unsigned
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; glb uip_hostaddr : [2u] unsigned
section .bss
	alignb 2
	global	_uip_hostaddr
_uip_hostaddr:
	resb	4

; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; glb uip_arp_draddr : [2u] unsigned
section .bss
	alignb 2
	global	_uip_arp_draddr
_uip_arp_draddr:
	resb	4

; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; glb uip_arp_netmask : [2u] unsigned
section .bss
	alignb 2
	global	_uip_arp_netmask
_uip_arp_netmask:
	resb	4

; RPN'ized expression: "1500 2 + "
; Expanded expression: "1502 "
; Expression value: 1502
; glb uip_buf : [1502u] unsigned char
section .bss
	global	_uip_buf
_uip_buf:
	resb	1502

; glb uip_appdata : * unsigned char
section .bss
	alignb 2
	global	_uip_appdata
_uip_appdata:
	resb	2

; glb uip_sappdata : * unsigned char
section .bss
	alignb 2
	global	_uip_sappdata
_uip_sappdata:
	resb	2

; glb uip_urgdata : * unsigned char
section .bss
	alignb 2
	global	_uip_urgdata
_uip_urgdata:
	resb	2

; glb uip_urglen : unsigned char
section .bss
	global	_uip_urglen
_uip_urglen:
	resb	1

; glb uip_surglen : unsigned char
section .bss
	global	_uip_surglen
_uip_surglen:
	resb	1

; glb uip_len : unsigned
section .bss
	alignb 2
	global	_uip_len
_uip_len:
	resb	2

; glb uip_slen : unsigned
section .bss
	alignb 2
	global	_uip_slen
_uip_slen:
	resb	2

; glb uip_flags : unsigned char
section .bss
	global	_uip_flags
_uip_flags:
	resb	1

; glb uip_conn : * struct uip_conn
section .bss
	alignb 2
	global	_uip_conn
_uip_conn:
	resb	2

; RPN'ized expression: "128 "
; Expanded expression: "128 "
; Expression value: 128
; glb uip_conns : [128u] struct uip_conn
section .bss
	alignb 2
	global	_uip_conns
_uip_conns:
	resb	19968

; RPN'ized expression: "10 "
; Expanded expression: "10 "
; Expression value: 10
; glb uip_listenports : [10u] unsigned
section .bss
	alignb 2
	global	_uip_listenports
_uip_listenports:
	resb	20

; glb uip_udp_conn : * struct uip_udp_conn
section .bss
	alignb 2
	global	_uip_udp_conn
_uip_udp_conn:
	resb	2

; RPN'ized expression: "128 "
; Expanded expression: "128 "
; Expression value: 128
; glb uip_udp_conns : [128u] struct uip_udp_conn
section .bss
	alignb 2
	global	_uip_udp_conns
_uip_udp_conns:
	resb	1024

; glb ipid : unsigned
section .bss
	alignb 2
_ipid:
	resb	2

; RPN'ized expression: "4 "
; Expanded expression: "4 "
; Expression value: 4
; glb iss : [4u] unsigned char
section .bss
_iss:
	resb	4

; glb lastport : unsigned
section .bss
	alignb 2
_lastport:
	resb	2

; RPN'ized expression: "4 "
; Expanded expression: "4 "
; Expression value: 4
; glb uip_acc32 : [4u] unsigned char
section .bss
	global	_uip_acc32
_uip_acc32:
	resb	4

; glb c : unsigned char
section .bss
_c:
	resb	1

; glb opt : unsigned char
section .bss
_opt:
	resb	1

; glb tmp16 : unsigned
section .bss
	alignb 2
_tmp16:
	resb	2

; glb uip_stat : struct uip_stats
section .bss
	alignb 2
	global	_uip_stat
_uip_stat:
	resb	44

; glb uip_init : (void) void
section .text
	global	_uip_init
_uip_init:
	push	bp
	mov	bp, sp
	;sub	sp,          0
; for
; RPN'ized expression: "c 0 = "
; Expanded expression: "c 0 =(1) "
; Fused expression:    "=(154) *c 0 "
	mov	ax, 0
	mov	[_c], al
	mov	ah, 0
L5:
; RPN'ized expression: "c 10 < "
; Expanded expression: "c *(1) 10 < "
; Fused expression:    "< *c 10 IF! "
	mov	al, [_c]
	mov	ah, 0
	cmp	ax, 10
	jge	L8
; RPN'ized expression: "c ++ "
; Expanded expression: "c ++(1) "
; {
; RPN'ized expression: "uip_listenports c + *u 0 = "
; Expanded expression: "uip_listenports c *(1) 2 * + 0 =(2) "
; Fused expression:    "* *c 2 + uip_listenports ax =(170) *ax 0 "
	mov	al, [_c]
	mov	ah, 0
	imul	ax, ax, 2
	mov	cx, ax
	mov	ax, _uip_listenports
	add	ax, cx
	mov	bx, ax
	mov	ax, 0
	mov	[bx], ax
; }
L6:
; Fused expression:    "++(1) *c "
	inc	byte [_c]
	mov	al, [_c]
	mov	ah, 0
	jmp	L5
L8:
; for
; RPN'ized expression: "c 0 = "
; Expanded expression: "c 0 =(1) "
; Fused expression:    "=(154) *c 0 "
	mov	ax, 0
	mov	[_c], al
	mov	ah, 0
L9:
; RPN'ized expression: "c 128 < "
; Expanded expression: "c *(1) 128 < "
; Fused expression:    "< *c 128 IF! "
	mov	al, [_c]
	mov	ah, 0
	cmp	ax, 128
	jge	L12
; RPN'ized expression: "c ++ "
; Expanded expression: "c ++(1) "
; {
; RPN'ized expression: "uip_conns c + *u &u tcpstateflags -> *u 0 = "
; Expanded expression: "uip_conns c *(1) 156 * + 25 + 0 =(1) "
; Fused expression:    "* *c 156 + uip_conns ax + ax 25 =(154) *ax 0 "
	mov	al, [_c]
	mov	ah, 0
	imul	ax, ax, 156
	mov	cx, ax
	mov	ax, _uip_conns
	add	ax, cx
	add	ax, 25
	mov	bx, ax
	mov	ax, 0
	mov	[bx], al
	mov	ah, 0
; }
L10:
; Fused expression:    "++(1) *c "
	inc	byte [_c]
	mov	al, [_c]
	mov	ah, 0
	jmp	L9
L12:
; RPN'ized expression: "lastport 1024 = "
; Expanded expression: "lastport 1024 =(2) "
; Fused expression:    "=(170) *lastport 1024 "
	mov	ax, 1024
	mov	[_lastport], ax
; for
; RPN'ized expression: "c 0 = "
; Expanded expression: "c 0 =(1) "
; Fused expression:    "=(154) *c 0 "
	mov	ax, 0
	mov	[_c], al
	mov	ah, 0
L13:
; RPN'ized expression: "c 128 < "
; Expanded expression: "c *(1) 128 < "
; Fused expression:    "< *c 128 IF! "
	mov	al, [_c]
	mov	ah, 0
	cmp	ax, 128
	jge	L16
; RPN'ized expression: "c ++ "
; Expanded expression: "c ++(1) "
; {
; RPN'ized expression: "uip_udp_conns c + *u &u lport -> *u 0 = "
; Expanded expression: "uip_udp_conns c *(1) 8 * + 4 + 0 =(2) "
; Fused expression:    "* *c 8 + uip_udp_conns ax + ax 4 =(170) *ax 0 "
	mov	al, [_c]
	mov	ah, 0
	imul	ax, ax, 8
	mov	cx, ax
	mov	ax, _uip_udp_conns
	add	ax, cx
	add	ax, 4
	mov	bx, ax
	mov	ax, 0
	mov	[bx], ax
; }
L14:
; Fused expression:    "++(1) *c "
	inc	byte [_c]
	mov	al, [_c]
	mov	ah, 0
	jmp	L13
L16:
; RPN'ized expression: "uip_hostaddr 0 + *u uip_hostaddr 1 + *u 0 = = "
; Expanded expression: "uip_hostaddr 0 + uip_hostaddr 2 + 0 =(2) =(2) "
; Fused expression:    "+ uip_hostaddr 0 push-ax + uip_hostaddr 2 =(170) *ax 0 =(170) **sp ax "
	mov	ax, _uip_hostaddr
	push	ax
	mov	ax, _uip_hostaddr
	add	ax, 2
	mov	bx, ax
	mov	ax, 0
	mov	[bx], ax
	pop	bx
	mov	[bx], ax
L3:
	leave
	ret

; glb uip_connect : (
; prm     ripaddr : * unsigned
; prm     rport : unsigned
;     ) * struct uip_conn
section .text
	global	_uip_connect
_uip_connect:
	push	bp
	mov	bp, sp
	 sub	sp,          4
; loc     ripaddr : (@4): * unsigned
; loc     rport : (@6): unsigned
; loc     conn : (@-2): * struct uip_conn
; loc     cconn : (@-4): * struct uip_conn
; again:
L19:
; RPN'ized expression: "lastport ++ "
; Expanded expression: "lastport ++(2) "
; Fused expression:    "++(2) *lastport "
	inc	word [_lastport]
	mov	ax, [_lastport]
; if
; RPN'ized expression: "lastport 32000 >= "
; Expanded expression: "lastport *(2) 32000 >=u "
; Fused expression:    ">=u *lastport 32000 IF! "
	mov	ax, [_lastport]
	cmp	ax, 32000
	jb	L20
; {
; RPN'ized expression: "lastport 4096 = "
; Expanded expression: "lastport 4096 =(2) "
; Fused expression:    "=(170) *lastport 4096 "
	mov	ax, 4096
	mov	[_lastport], ax
; }
L20:
; for
; RPN'ized expression: "c 0 = "
; Expanded expression: "c 0 =(1) "
; Fused expression:    "=(154) *c 0 "
	mov	ax, 0
	mov	[_c], al
	mov	ah, 0
L22:
; RPN'ized expression: "c 128 < "
; Expanded expression: "c *(1) 128 < "
; Fused expression:    "< *c 128 IF! "
	mov	al, [_c]
	mov	ah, 0
	cmp	ax, 128
	jge	L25
; RPN'ized expression: "c ++ "
; Expanded expression: "c ++(1) "
; {
; RPN'ized expression: "conn uip_conns c + *u &u = "
; Expanded expression: "(@-2) uip_conns c *(1) 156 * + =(2) "
; Fused expression:    "* *c 156 + uip_conns ax =(170) *(@-2) ax "
	mov	al, [_c]
	mov	ah, 0
	imul	ax, ax, 156
	mov	cx, ax
	mov	ax, _uip_conns
	add	ax, cx
	mov	[bp-2], ax
; if
; RPN'ized expression: "conn tcpstateflags -> *u 0 != conn lport -> *u ( lastport htons ) == && "
; Expanded expression: "(@-2) *(2) 25 + *(1) 0 != [sh&&->28] (@-2) *(2) 4 + *(2)  lastport *(2)  htons ()2 == &&[28] "
; Fused expression:    "+ *(@-2) 25 != *ax 0 [sh&&->28] + *(@-2) 4 push-ax ( *(2) lastport , htons )2 == **sp ax &&[28]  "
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 0
	setne	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L28
	mov	ax, [bp-2]
	add	ax, 4
	push	ax
	push	word [_lastport]
	call	_htons
	sub	sp, -2
	mov	cx, ax
	pop	bx
	mov	ax, [bx]
	cmp	ax, cx
	sete	al
	cbw
L28:
; JumpIfZero
	test	ax, ax
	je	L26
; {
; goto again
	jmp	L19
; }
L26:
; }
L23:
; Fused expression:    "++(1) *c "
	inc	byte [_c]
	mov	al, [_c]
	mov	ah, 0
	jmp	L22
L25:
; RPN'ized expression: "conn 0 = "
; Expanded expression: "(@-2) 0 =(2) "
; Fused expression:    "=(170) *(@-2) 0 "
	mov	ax, 0
	mov	[bp-2], ax
; for
; RPN'ized expression: "c 0 = "
; Expanded expression: "c 0 =(1) "
; Fused expression:    "=(154) *c 0 "
	mov	ax, 0
	mov	[_c], al
	mov	ah, 0
L29:
; RPN'ized expression: "c 128 < "
; Expanded expression: "c *(1) 128 < "
; Fused expression:    "< *c 128 IF! "
	mov	al, [_c]
	mov	ah, 0
	cmp	ax, 128
	jge	L32
; RPN'ized expression: "c ++ "
; Expanded expression: "c ++(1) "
; {
; RPN'ized expression: "cconn uip_conns c + *u &u = "
; Expanded expression: "(@-4) uip_conns c *(1) 156 * + =(2) "
; Fused expression:    "* *c 156 + uip_conns ax =(170) *(@-4) ax "
	mov	al, [_c]
	mov	ah, 0
	imul	ax, ax, 156
	mov	cx, ax
	mov	ax, _uip_conns
	add	ax, cx
	mov	[bp-4], ax
; if
; RPN'ized expression: "cconn tcpstateflags -> *u 0 == "
; Expanded expression: "(@-4) *(2) 25 + *(1) 0 == "
; Fused expression:    "+ *(@-4) 25 == *ax 0 IF! "
	mov	ax, [bp-4]
	add	ax, 25
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 0
	jne	L33
; {
; RPN'ized expression: "conn cconn = "
; Expanded expression: "(@-2) (@-4) *(2) =(2) "
; Fused expression:    "=(170) *(@-2) *(@-4) "
	mov	ax, [bp-4]
	mov	[bp-2], ax
; break
	jmp	L32
; }
L33:
; if
; RPN'ized expression: "cconn tcpstateflags -> *u 7 == "
; Expanded expression: "(@-4) *(2) 25 + *(1) 7 == "
; Fused expression:    "+ *(@-4) 25 == *ax 7 IF! "
	mov	ax, [bp-4]
	add	ax, 25
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 7
	jne	L35
; {
; if
; RPN'ized expression: "conn 0 == cconn timer -> *u uip_conn timer -> *u > || "
; Expanded expression: "(@-2) *(2) 0 == [sh||->39] (@-4) *(2) 26 + *(1) uip_conn *(2) 26 + *(1) > ||[39] "
; Fused expression:    "== *(@-2) 0 [sh||->39] + *(@-4) 26 push-ax + *uip_conn 26 > **sp *ax ||[39]  "
	mov	ax, [bp-2]
	cmp	ax, 0
	sete	al
	cbw
; JumpIfNotZero
	test	ax, ax
	jne	L39
	mov	ax, [bp-4]
	add	ax, 26
	push	ax
	mov	ax, [_uip_conn]
	add	ax, 26
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	bx
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, cx
	setg	al
	cbw
L39:
; JumpIfZero
	test	ax, ax
	je	L37
; {
; RPN'ized expression: "conn cconn = "
; Expanded expression: "(@-2) (@-4) *(2) =(2) "
; Fused expression:    "=(170) *(@-2) *(@-4) "
	mov	ax, [bp-4]
	mov	[bp-2], ax
; }
L37:
; }
L35:
; }
L30:
; Fused expression:    "++(1) *c "
	inc	byte [_c]
	mov	al, [_c]
	mov	ah, 0
	jmp	L29
L32:
; if
; RPN'ized expression: "conn 0 == "
; Expanded expression: "(@-2) *(2) 0 == "
; Fused expression:    "== *(@-2) 0 IF! "
	mov	ax, [bp-2]
	cmp	ax, 0
	jne	L40
; {
; return
; RPN'ized expression: "0 "
; Expanded expression: "0 "
; Expression value: 0
; Fused expression:    "0  "
	mov	ax, 0
	jmp	L17
; }
L40:
; RPN'ized expression: "conn tcpstateflags -> *u 2 = "
; Expanded expression: "(@-2) *(2) 25 + 2 =(1) "
; Fused expression:    "+ *(@-2) 25 =(154) *ax 2 "
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	ax, 2
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "conn snd_nxt -> *u 0 + *u iss 0 + *u = "
; Expanded expression: "(@-2) *(2) 12 + iss 0 + *(1) =(1) "
; Fused expression:    "+ *(@-2) 12 push-ax + iss 0 =(153) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 12
	push	ax
	mov	ax, _iss
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "conn snd_nxt -> *u 1 + *u iss 1 + *u = "
; Expanded expression: "(@-2) *(2) 13 + iss 1 + *(1) =(1) "
; Fused expression:    "+ *(@-2) 13 push-ax + iss 1 =(153) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 13
	push	ax
	mov	ax, _iss
	inc	ax
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "conn snd_nxt -> *u 2 + *u iss 2 + *u = "
; Expanded expression: "(@-2) *(2) 14 + iss 2 + *(1) =(1) "
; Fused expression:    "+ *(@-2) 14 push-ax + iss 2 =(153) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 14
	push	ax
	mov	ax, _iss
	add	ax, 2
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "conn snd_nxt -> *u 3 + *u iss 3 + *u = "
; Expanded expression: "(@-2) *(2) 15 + iss 3 + *(1) =(1) "
; Fused expression:    "+ *(@-2) 15 push-ax + iss 3 =(153) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 15
	push	ax
	mov	ax, _iss
	add	ax, 3
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "conn initialmss -> *u conn mss -> *u 1500 14 - 40 - = = "
; Expanded expression: "(@-2) *(2) 20 + (@-2) *(2) 18 + 1446 =(2) =(2) "
; Fused expression:    "+ *(@-2) 20 push-ax + *(@-2) 18 =(170) *ax 1446 =(170) **sp ax "
	mov	ax, [bp-2]
	add	ax, 20
	push	ax
	mov	ax, [bp-2]
	add	ax, 18
	mov	bx, ax
	mov	ax, 1446
	mov	[bx], ax
	pop	bx
	mov	[bx], ax
; RPN'ized expression: "conn len -> *u 1 = "
; Expanded expression: "(@-2) *(2) 16 + 1 =(2) "
; Fused expression:    "+ *(@-2) 16 =(170) *ax 1 "
	mov	ax, [bp-2]
	add	ax, 16
	mov	bx, ax
	mov	ax, 1
	mov	[bx], ax
; RPN'ized expression: "conn nrtx -> *u 0 = "
; Expanded expression: "(@-2) *(2) 27 + 0 =(1) "
; Fused expression:    "+ *(@-2) 27 =(154) *ax 0 "
	mov	ax, [bp-2]
	add	ax, 27
	mov	bx, ax
	mov	ax, 0
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "conn timer -> *u 1 = "
; Expanded expression: "(@-2) *(2) 26 + 1 =(1) "
; Fused expression:    "+ *(@-2) 26 =(154) *ax 1 "
	mov	ax, [bp-2]
	add	ax, 26
	mov	bx, ax
	mov	ax, 1
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "conn rto -> *u 3 = "
; Expanded expression: "(@-2) *(2) 24 + 3 =(1) "
; Fused expression:    "+ *(@-2) 24 =(154) *ax 3 "
	mov	ax, [bp-2]
	add	ax, 24
	mov	bx, ax
	mov	ax, 3
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "conn sa -> *u 0 = "
; Expanded expression: "(@-2) *(2) 22 + 0 =(1) "
; Fused expression:    "+ *(@-2) 22 =(154) *ax 0 "
	mov	ax, [bp-2]
	add	ax, 22
	mov	bx, ax
	mov	ax, 0
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "conn sv -> *u 16 = "
; Expanded expression: "(@-2) *(2) 23 + 16 =(1) "
; Fused expression:    "+ *(@-2) 23 =(154) *ax 16 "
	mov	ax, [bp-2]
	add	ax, 23
	mov	bx, ax
	mov	ax, 16
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "conn lport -> *u ( lastport htons ) = "
; Expanded expression: "(@-2) *(2) 4 +  lastport *(2)  htons ()2 =(2) "
; Fused expression:    "+ *(@-2) 4 push-ax ( *(2) lastport , htons )2 =(170) **sp ax "
	mov	ax, [bp-2]
	add	ax, 4
	push	ax
	push	word [_lastport]
	call	_htons
	sub	sp, -2
	pop	bx
	mov	[bx], ax
; RPN'ized expression: "conn rport -> *u rport = "
; Expanded expression: "(@-2) *(2) 6 + (@6) *(2) =(2) "
; Fused expression:    "+ *(@-2) 6 =(170) *ax *(@6) "
	mov	ax, [bp-2]
	add	ax, 6
	mov	bx, ax
	mov	ax, [bp+6]
	mov	[bx], ax
; RPN'ized expression: "conn ripaddr -> *u 0 + *u ripaddr 0 + *u = "
; Expanded expression: "(@-2) *(2) 0 + (@4) *(2) 0 + *(2) =(2) "
; Fused expression:    "+ *(@-2) 0 push-ax + *(@4) 0 =(170) **sp *ax "
	mov	ax, [bp-2]
	push	ax
	mov	ax, [bp+4]
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; RPN'ized expression: "conn ripaddr -> *u 1 + *u ripaddr 1 + *u = "
; Expanded expression: "(@-2) *(2) 2 + (@4) *(2) 2 + *(2) =(2) "
; Fused expression:    "+ *(@-2) 2 push-ax + *(@4) 2 =(170) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 2
	push	ax
	mov	ax, [bp+4]
	add	ax, 2
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; return
; RPN'ized expression: "conn "
; Expanded expression: "(@-2) *(2) "
; Fused expression:    "*(2) (@-2)  "
	mov	ax, [bp-2]
L17:
	leave
	ret

; glb uip_udp_new : (
; prm     ripaddr : * unsigned
; prm     rport : unsigned
;     ) * struct uip_udp_conn
section .text
	global	_uip_udp_new
_uip_udp_new:
	push	bp
	mov	bp, sp
	 sub	sp,          2
; loc     ripaddr : (@4): * unsigned
; loc     rport : (@6): unsigned
; loc     conn : (@-2): * struct uip_udp_conn
; again:
L44:
; RPN'ized expression: "lastport ++ "
; Expanded expression: "lastport ++(2) "
; Fused expression:    "++(2) *lastport "
	inc	word [_lastport]
	mov	ax, [_lastport]
; if
; RPN'ized expression: "lastport 32000 >= "
; Expanded expression: "lastport *(2) 32000 >=u "
; Fused expression:    ">=u *lastport 32000 IF! "
	mov	ax, [_lastport]
	cmp	ax, 32000
	jb	L45
; {
; RPN'ized expression: "lastport 4096 = "
; Expanded expression: "lastport 4096 =(2) "
; Fused expression:    "=(170) *lastport 4096 "
	mov	ax, 4096
	mov	[_lastport], ax
; }
L45:
; for
; RPN'ized expression: "c 0 = "
; Expanded expression: "c 0 =(1) "
; Fused expression:    "=(154) *c 0 "
	mov	ax, 0
	mov	[_c], al
	mov	ah, 0
L47:
; RPN'ized expression: "c 128 < "
; Expanded expression: "c *(1) 128 < "
; Fused expression:    "< *c 128 IF! "
	mov	al, [_c]
	mov	ah, 0
	cmp	ax, 128
	jge	L50
; RPN'ized expression: "c ++ "
; Expanded expression: "c ++(1) "
; {
; if
; RPN'ized expression: "uip_udp_conns c + *u &u lport -> *u lastport == "
; Expanded expression: "uip_udp_conns c *(1) 8 * + 4 + *(2) lastport *(2) == "
; Fused expression:    "* *c 8 + uip_udp_conns ax + ax 4 == *ax *lastport IF! "
	mov	al, [_c]
	mov	ah, 0
	imul	ax, ax, 8
	mov	cx, ax
	mov	ax, _uip_udp_conns
	add	ax, cx
	add	ax, 4
	mov	bx, ax
	mov	ax, [bx]
	cmp	ax, [_lastport]
	jne	L51
; {
; goto again
	jmp	L44
; }
L51:
; }
L48:
; Fused expression:    "++(1) *c "
	inc	byte [_c]
	mov	al, [_c]
	mov	ah, 0
	jmp	L47
L50:
; RPN'ized expression: "conn 0 = "
; Expanded expression: "(@-2) 0 =(2) "
; Fused expression:    "=(170) *(@-2) 0 "
	mov	ax, 0
	mov	[bp-2], ax
; for
; RPN'ized expression: "c 0 = "
; Expanded expression: "c 0 =(1) "
; Fused expression:    "=(154) *c 0 "
	mov	ax, 0
	mov	[_c], al
	mov	ah, 0
L53:
; RPN'ized expression: "c 128 < "
; Expanded expression: "c *(1) 128 < "
; Fused expression:    "< *c 128 IF! "
	mov	al, [_c]
	mov	ah, 0
	cmp	ax, 128
	jge	L56
; RPN'ized expression: "c ++ "
; Expanded expression: "c ++(1) "
; {
; if
; RPN'ized expression: "uip_udp_conns c + *u &u lport -> *u 0 == "
; Expanded expression: "uip_udp_conns c *(1) 8 * + 4 + *(2) 0 == "
; Fused expression:    "* *c 8 + uip_udp_conns ax + ax 4 == *ax 0 IF! "
	mov	al, [_c]
	mov	ah, 0
	imul	ax, ax, 8
	mov	cx, ax
	mov	ax, _uip_udp_conns
	add	ax, cx
	add	ax, 4
	mov	bx, ax
	mov	ax, [bx]
	cmp	ax, 0
	jne	L57
; {
; RPN'ized expression: "conn uip_udp_conns c + *u &u = "
; Expanded expression: "(@-2) uip_udp_conns c *(1) 8 * + =(2) "
; Fused expression:    "* *c 8 + uip_udp_conns ax =(170) *(@-2) ax "
	mov	al, [_c]
	mov	ah, 0
	imul	ax, ax, 8
	mov	cx, ax
	mov	ax, _uip_udp_conns
	add	ax, cx
	mov	[bp-2], ax
; break
	jmp	L56
; }
L57:
; }
L54:
; Fused expression:    "++(1) *c "
	inc	byte [_c]
	mov	al, [_c]
	mov	ah, 0
	jmp	L53
L56:
; if
; RPN'ized expression: "conn 0 == "
; Expanded expression: "(@-2) *(2) 0 == "
; Fused expression:    "== *(@-2) 0 IF! "
	mov	ax, [bp-2]
	cmp	ax, 0
	jne	L59
; {
; return
; RPN'ized expression: "0 "
; Expanded expression: "0 "
; Expression value: 0
; Fused expression:    "0  "
	mov	ax, 0
	jmp	L42
; }
L59:
; loc     <something> : unsigned
; RPN'ized expression: "conn lport -> *u lastport 255 & (something61) 8 << lastport 65280u & 8 >> | = "
; Expanded expression: "(@-2) *(2) 4 + lastport *(2) 255 & 8 << lastport *(2) 65280u & 8 >>u | =(2) "
; Fused expression:    "+ *(@-2) 4 push-ax & *lastport 255 << ax 8 push-ax & *lastport 65280u >>u ax 8 | *sp ax =(170) **sp ax "
	mov	ax, [bp-2]
	add	ax, 4
	push	ax
	mov	ax, [_lastport]
	and	ax, 255
	shl	ax, 8
	push	ax
	mov	ax, [_lastport]
	and	ax, -256
	shr	ax, 8
	mov	cx, ax
	pop	ax
	or	ax, cx
	pop	bx
	mov	[bx], ax
; loc     <something> : unsigned
; RPN'ized expression: "conn rport -> *u rport 255 & (something62) 8 << rport 65280u & 8 >> | = "
; Expanded expression: "(@-2) *(2) 6 + (@6) *(2) 255 & 8 << (@6) *(2) 65280u & 8 >>u | =(2) "
; Fused expression:    "+ *(@-2) 6 push-ax & *(@6) 255 << ax 8 push-ax & *(@6) 65280u >>u ax 8 | *sp ax =(170) **sp ax "
	mov	ax, [bp-2]
	add	ax, 6
	push	ax
	mov	ax, [bp+6]
	and	ax, 255
	shl	ax, 8
	push	ax
	mov	ax, [bp+6]
	and	ax, -256
	shr	ax, 8
	mov	cx, ax
	pop	ax
	or	ax, cx
	pop	bx
	mov	[bx], ax
; RPN'ized expression: "conn ripaddr -> *u 0 + *u ripaddr 0 + *u = "
; Expanded expression: "(@-2) *(2) 0 + (@4) *(2) 0 + *(2) =(2) "
; Fused expression:    "+ *(@-2) 0 push-ax + *(@4) 0 =(170) **sp *ax "
	mov	ax, [bp-2]
	push	ax
	mov	ax, [bp+4]
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; RPN'ized expression: "conn ripaddr -> *u 1 + *u ripaddr 1 + *u = "
; Expanded expression: "(@-2) *(2) 2 + (@4) *(2) 2 + *(2) =(2) "
; Fused expression:    "+ *(@-2) 2 push-ax + *(@4) 2 =(170) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 2
	push	ax
	mov	ax, [bp+4]
	add	ax, 2
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; return
; RPN'ized expression: "conn "
; Expanded expression: "(@-2) *(2) "
; Fused expression:    "*(2) (@-2)  "
	mov	ax, [bp-2]
L42:
	leave
	ret

; glb uip_unlisten : (
; prm     port : unsigned
;     ) void
section .text
	global	_uip_unlisten
_uip_unlisten:
	push	bp
	mov	bp, sp
	;sub	sp,          0
; loc     port : (@4): unsigned
; for
; RPN'ized expression: "c 0 = "
; Expanded expression: "c 0 =(1) "
; Fused expression:    "=(154) *c 0 "
	mov	ax, 0
	mov	[_c], al
	mov	ah, 0
L65:
; RPN'ized expression: "c 10 < "
; Expanded expression: "c *(1) 10 < "
; Fused expression:    "< *c 10 IF! "
	mov	al, [_c]
	mov	ah, 0
	cmp	ax, 10
	jge	L68
; RPN'ized expression: "c ++ "
; Expanded expression: "c ++(1) "
; {
; if
; RPN'ized expression: "uip_listenports c + *u port == "
; Expanded expression: "uip_listenports c *(1) 2 * + *(2) (@4) *(2) == "
; Fused expression:    "* *c 2 + uip_listenports ax == *ax *(@4) IF! "
	mov	al, [_c]
	mov	ah, 0
	imul	ax, ax, 2
	mov	cx, ax
	mov	ax, _uip_listenports
	add	ax, cx
	mov	bx, ax
	mov	ax, [bx]
	cmp	ax, [bp+4]
	jne	L69
; {
; RPN'ized expression: "uip_listenports c + *u 0 = "
; Expanded expression: "uip_listenports c *(1) 2 * + 0 =(2) "
; Fused expression:    "* *c 2 + uip_listenports ax =(170) *ax 0 "
	mov	al, [_c]
	mov	ah, 0
	imul	ax, ax, 2
	mov	cx, ax
	mov	ax, _uip_listenports
	add	ax, cx
	mov	bx, ax
	mov	ax, 0
	mov	[bx], ax
; return
	jmp	L63
; }
L69:
; }
L66:
; Fused expression:    "++(1) *c "
	inc	byte [_c]
	mov	al, [_c]
	mov	ah, 0
	jmp	L65
L68:
L63:
	leave
	ret

; glb uip_listen : (
; prm     port : unsigned
;     ) void
section .text
	global	_uip_listen
_uip_listen:
	push	bp
	mov	bp, sp
	;sub	sp,          0
; loc     port : (@4): unsigned
; for
; RPN'ized expression: "c 0 = "
; Expanded expression: "c 0 =(1) "
; Fused expression:    "=(154) *c 0 "
	mov	ax, 0
	mov	[_c], al
	mov	ah, 0
L73:
; RPN'ized expression: "c 10 < "
; Expanded expression: "c *(1) 10 < "
; Fused expression:    "< *c 10 IF! "
	mov	al, [_c]
	mov	ah, 0
	cmp	ax, 10
	jge	L76
; RPN'ized expression: "c ++ "
; Expanded expression: "c ++(1) "
; {
; if
; RPN'ized expression: "uip_listenports c + *u 0 == "
; Expanded expression: "uip_listenports c *(1) 2 * + *(2) 0 == "
; Fused expression:    "* *c 2 + uip_listenports ax == *ax 0 IF! "
	mov	al, [_c]
	mov	ah, 0
	imul	ax, ax, 2
	mov	cx, ax
	mov	ax, _uip_listenports
	add	ax, cx
	mov	bx, ax
	mov	ax, [bx]
	cmp	ax, 0
	jne	L77
; {
; RPN'ized expression: "uip_listenports c + *u port = "
; Expanded expression: "uip_listenports c *(1) 2 * + (@4) *(2) =(2) "
; Fused expression:    "* *c 2 + uip_listenports ax =(170) *ax *(@4) "
	mov	al, [_c]
	mov	ah, 0
	imul	ax, ax, 2
	mov	cx, ax
	mov	ax, _uip_listenports
	add	ax, cx
	mov	bx, ax
	mov	ax, [bp+4]
	mov	[bx], ax
; return
	jmp	L71
; }
L77:
; }
L74:
; Fused expression:    "++(1) *c "
	inc	byte [_c]
	mov	al, [_c]
	mov	ah, 0
	jmp	L73
L76:
L71:
	leave
	ret

; RPN'ized expression: "1500 14 - "
; Expanded expression: "1486 "
; Expression value: 1486
; glb uip_reassbuf : [1486u] unsigned char
section .bss
_uip_reassbuf:
	resb	1486

; RPN'ized expression: "1500 14 - 8 8 * / "
; Expanded expression: "23 "
; Expression value: 23
; glb uip_reassbitmap : [23u] unsigned char
section .bss
_uip_reassbitmap:
	resb	23

; RPN'ized expression: "8 "
; Expanded expression: "8 "
; Expression value: 8
; glb bitmap_bits : [8u] unsigned char
section .data
_bitmap_bits:
; =
; RPN'ized expression: "255 "
; Expanded expression: "255 "
; Expression value: 255
	db	255
; RPN'ized expression: "127 "
; Expanded expression: "127 "
; Expression value: 127
	db	127
; RPN'ized expression: "63 "
; Expanded expression: "63 "
; Expression value: 63
	db	63
; RPN'ized expression: "31 "
; Expanded expression: "31 "
; Expression value: 31
	db	31
; RPN'ized expression: "15 "
; Expanded expression: "15 "
; Expression value: 15
	db	15
; RPN'ized expression: "7 "
; Expanded expression: "7 "
; Expression value: 7
	db	7
; RPN'ized expression: "3 "
; Expanded expression: "3 "
; Expression value: 3
	db	3
; RPN'ized expression: "1 "
; Expanded expression: "1 "
; Expression value: 1
	db	1

; glb uip_reasslen : unsigned
section .bss
	alignb 2
_uip_reasslen:
	resb	2

; glb uip_reassflags : unsigned char
section .bss
_uip_reassflags:
	resb	1

; glb uip_reasstmr : unsigned char
section .bss
_uip_reasstmr:
	resb	1

; glb uip_reass : (void) unsigned char
section .text
_uip_reass:
	push	bp
	mov	bp, sp
	 sub	sp,          6
; loc     offset : (@-2): unsigned
; loc     len : (@-4): unsigned
; loc     i : (@-6): unsigned
; if
; RPN'ized expression: "uip_reasstmr 0 == "
; Expanded expression: "uip_reasstmr *(1) 0 == "
; Fused expression:    "== *uip_reasstmr 0 IF! "
	mov	al, [_uip_reasstmr]
	mov	ah, 0
	cmp	ax, 0
	jne	L81
; {
; loc         <something> : * struct <something>
; RPN'ized expression: "( 20 , uip_buf 14 + *u &u (something83) vhl -> *u &u , uip_reassbuf memcpy ) "
; Expanded expression: " 20  uip_buf 14 + 0 +  uip_reassbuf  memcpy ()6 "
; Fused expression:    "( 20 , + uip_buf 14 + ax 0 , uip_reassbuf , memcpy )6 "
	push	20
	mov	ax, _uip_buf
	add	ax, 14
	push	ax
	push	_uip_reassbuf
	call	_memcpy
	sub	sp, -6
; RPN'ized expression: "uip_reasstmr 40 = "
; Expanded expression: "uip_reasstmr 40 =(1) "
; Fused expression:    "=(154) *uip_reasstmr 40 "
	mov	ax, 40
	mov	[_uip_reasstmr], al
	mov	ah, 0
; RPN'ized expression: "uip_reassflags 0 = "
; Expanded expression: "uip_reassflags 0 =(1) "
; Fused expression:    "=(154) *uip_reassflags 0 "
	mov	ax, 0
	mov	[_uip_reassflags], al
	mov	ah, 0
; RPN'ized expression: "( 0 , uip_reassbitmap sizeof , uip_reassbitmap memset ) "
; Expanded expression: " 0  23u  uip_reassbitmap  memset ()6 "
; Fused expression:    "( 0 , 23u , uip_reassbitmap , memset )6 "
	push	0
	push	23
	push	_uip_reassbitmap
	call	_memset
	sub	sp, -6
; }
L81:
; if
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something86) srcipaddr -> *u 0 + *u uip_reassbuf 0 + *u &u (something87) srcipaddr -> *u 0 + *u == uip_buf 14 + *u &u (something88) srcipaddr -> *u 1 + *u uip_reassbuf 0 + *u &u (something89) srcipaddr -> *u 1 + *u == && uip_buf 14 + *u &u (something90) destipaddr -> *u 0 + *u uip_reassbuf 0 + *u &u (something91) destipaddr -> *u 0 + *u == && uip_buf 14 + *u &u (something92) destipaddr -> *u 1 + *u uip_reassbuf 0 + *u &u (something93) destipaddr -> *u 1 + *u == && uip_buf 14 + *u &u (something94) ipid -> *u 0 + *u uip_reassbuf 0 + *u &u (something95) ipid -> *u 0 + *u == && uip_buf 14 + *u &u (something96) ipid -> *u 1 + *u uip_reassbuf 0 + *u &u (something97) ipid -> *u 1 + *u == && "
; Expanded expression: "uip_buf 14 + 12 + *(2) uip_reassbuf 0 + 12 + *(2) == [sh&&->102] uip_buf 14 + 14 + *(2) uip_reassbuf 0 + 14 + *(2) == &&[102] _Bool [sh&&->101] uip_buf 14 + 16 + *(2) uip_reassbuf 0 + 16 + *(2) == &&[101] _Bool [sh&&->100] uip_buf 14 + 18 + *(2) uip_reassbuf 0 + 18 + *(2) == &&[100] _Bool [sh&&->99] uip_buf 14 + 4 + *(1) uip_reassbuf 0 + 4 + *(1) == &&[99] _Bool [sh&&->98] uip_buf 14 + 5 + *(1) uip_reassbuf 0 + 5 + *(1) == &&[98] "
; Fused expression:    "+ uip_buf 14 + ax 12 push-ax + uip_reassbuf 0 + ax 12 == **sp *ax [sh&&->102] + uip_buf 14 + ax 14 push-ax + uip_reassbuf 0 + ax 14 == **sp *ax &&[102] _Bool [sh&&->101] + uip_buf 14 + ax 16 push-ax + uip_reassbuf 0 + ax 16 == **sp *ax &&[101] _Bool [sh&&->100] + uip_buf 14 + ax 18 push-ax + uip_reassbuf 0 + ax 18 == **sp *ax &&[100] _Bool [sh&&->99] + uip_buf 14 + ax 4 push-ax + uip_reassbuf 0 + ax 4 == **sp *ax &&[99] _Bool [sh&&->98] + uip_buf 14 + ax 5 push-ax + uip_reassbuf 0 + ax 5 == **sp *ax &&[98]  "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 12
	push	ax
	mov	ax, _uip_reassbuf
	add	ax, 12
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	cmp	ax, cx
	sete	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L102
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 14
	push	ax
	mov	ax, _uip_reassbuf
	add	ax, 14
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	cmp	ax, cx
	sete	al
	cbw
L102:
	test	ax, ax
	setne	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L101
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 16
	push	ax
	mov	ax, _uip_reassbuf
	add	ax, 16
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	cmp	ax, cx
	sete	al
	cbw
L101:
	test	ax, ax
	setne	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L100
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 18
	push	ax
	mov	ax, _uip_reassbuf
	add	ax, 18
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	cmp	ax, cx
	sete	al
	cbw
L100:
	test	ax, ax
	setne	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L99
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 4
	push	ax
	mov	ax, _uip_reassbuf
	add	ax, 4
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	bx
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, cx
	sete	al
	cbw
L99:
	test	ax, ax
	setne	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L98
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 5
	push	ax
	mov	ax, _uip_reassbuf
	add	ax, 5
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	bx
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, cx
	sete	al
	cbw
L98:
; JumpIfZero
	test	ax, ax
	je	L84
; {
; loc         <something> : * struct <something>
; loc         <something> : * struct <something>
; loc         <something> : * struct <something>
; RPN'ized expression: "len uip_buf 14 + *u &u (something103) len -> *u 0 + *u 8 << uip_buf 14 + *u &u (something104) len -> *u 1 + *u + uip_buf 14 + *u &u (something105) vhl -> *u 15 & 4 * - = "
; Expanded expression: "(@-4) uip_buf 14 + 2 + *(1) 8 << uip_buf 14 + 3 + *(1) + uip_buf 14 + 0 + *(1) 15 & 4 * - =(2) "
; Fused expression:    "+ uip_buf 14 + ax 2 << *ax 8 push-ax + uip_buf 14 + ax 3 + *sp *ax push-ax + uip_buf 14 + ax 0 & *ax 15 * ax 4 - *sp ax =(170) *(@-4) ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 2
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	shl	ax, 8
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 3
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	ax
	add	ax, cx
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	and	ax, 15
	imul	ax, ax, 4
	mov	cx, ax
	pop	ax
	sub	ax, cx
	mov	[bp-4], ax
; loc         <something> : * struct <something>
; loc         <something> : * struct <something>
; RPN'ized expression: "offset uip_buf 14 + *u &u (something106) ipoffset -> *u 0 + *u 63 & 8 << uip_buf 14 + *u &u (something107) ipoffset -> *u 1 + *u + 8 * = "
; Expanded expression: "(@-2) uip_buf 14 + 6 + *(1) 63 & 8 << uip_buf 14 + 7 + *(1) + 8 * =(2) "
; Fused expression:    "+ uip_buf 14 + ax 6 & *ax 63 << ax 8 push-ax + uip_buf 14 + ax 7 + *sp *ax * ax 8 =(170) *(@-2) ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 6
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	and	ax, 63
	shl	ax, 8
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 7
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	ax
	add	ax, cx
	imul	ax, ax, 8
	mov	[bp-2], ax
; if
; RPN'ized expression: "offset 1500 14 - > offset len + 1500 14 - > || "
; Expanded expression: "(@-2) *(2) 1486 >u [sh||->110] (@-2) *(2) (@-4) *(2) + 1486 >u ||[110] "
; Fused expression:    ">u *(@-2) 1486 [sh||->110] + *(@-2) *(@-4) >u ax 1486 ||[110]  "
	mov	ax, [bp-2]
	cmp	ax, 1486
	seta	al
	cbw
; JumpIfNotZero
	test	ax, ax
	jne	L110
	mov	ax, [bp-2]
	add	ax, [bp-4]
	cmp	ax, 1486
	seta	al
	cbw
L110:
; JumpIfZero
	test	ax, ax
	je	L108
; {
; RPN'ized expression: "uip_reasstmr 0 = "
; Expanded expression: "uip_reasstmr 0 =(1) "
; Fused expression:    "=(154) *uip_reasstmr 0 "
	mov	ax, 0
	mov	[_uip_reasstmr], al
	mov	ah, 0
; goto nullreturn
	jmp	L111
; }
L108:
; loc         <something> : * char
; loc         <something> : * struct <something>
; loc         <something> : int
; loc         <something> : * struct <something>
; RPN'ized expression: "( len , uip_buf 14 + *u &u (something113) (something112) uip_buf 14 + *u &u (something115) vhl -> *u 15 & 4 * (something114) + , uip_reassbuf 20 offset + + *u &u memcpy ) "
; Expanded expression: " (@-4) *(2)  uip_buf 14 + uip_buf 14 + 0 + *(1) 15 & 4 * +  uip_reassbuf 20 (@-2) *(2) + +  memcpy ()6 "
; Fused expression:    "( *(2) (@-4) , + uip_buf 14 push-ax + uip_buf 14 + ax 0 & *ax 15 * ax 4 + *sp ax , + 20 *(@-2) + uip_reassbuf ax , memcpy )6 "
	push	word [bp-4]
	mov	ax, _uip_buf
	add	ax, 14
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	and	ax, 15
	imul	ax, ax, 4
	mov	cx, ax
	pop	ax
	add	ax, cx
	push	ax
	mov	ax, 20
	add	ax, [bp-2]
	mov	cx, ax
	mov	ax, _uip_reassbuf
	add	ax, cx
	push	ax
	call	_memcpy
	sub	sp, -6
; if
; RPN'ized expression: "offset 8 8 * / offset len + 8 8 * / == "
; Expanded expression: "(@-2) *(2) 64 /u (@-2) *(2) (@-4) *(2) + 64 /u == "
; Fused expression:    "/u *(@-2) 64 push-ax + *(@-2) *(@-4) /u ax 64 == *sp ax IF! "
	mov	ax, [bp-2]
	mov	dx, 0
	mov	cx, 64
	div	cx
	push	ax
	mov	ax, [bp-2]
	add	ax, [bp-4]
	mov	dx, 0
	mov	cx, 64
	div	cx
	mov	cx, ax
	pop	ax
	cmp	ax, cx
	jne	L116
; {
; RPN'ized expression: "uip_reassbitmap offset 8 8 * / + *u bitmap_bits offset 8 / 7 & + *u bitmap_bits offset len + 8 / 7 & + *u ~ & |= "
; Expanded expression: "uip_reassbitmap (@-2) *(2) 64 /u + bitmap_bits (@-2) *(2) 8 /u 7 & + *(1) bitmap_bits (@-2) *(2) (@-4) *(2) + 8 /u 7 & + *(1) ~ & |=(1) "
; Fused expression:    "/u *(@-2) 64 + uip_reassbitmap ax push-ax /u *(@-2) 8 & ax 7 + bitmap_bits ax push-ax + *(@-2) *(@-4) /u ax 8 & ax 7 + bitmap_bits ax *(1) ax ~ & **sp ax |=(154) **sp ax "
	mov	ax, [bp-2]
	mov	dx, 0
	mov	cx, 64
	div	cx
	mov	cx, ax
	mov	ax, _uip_reassbitmap
	add	ax, cx
	push	ax
	mov	ax, [bp-2]
	mov	dx, 0
	mov	cx, 8
	div	cx
	and	ax, 7
	mov	cx, ax
	mov	ax, _bitmap_bits
	add	ax, cx
	push	ax
	mov	ax, [bp-2]
	add	ax, [bp-4]
	mov	dx, 0
	mov	cx, 8
	div	cx
	and	ax, 7
	mov	cx, ax
	mov	ax, _bitmap_bits
	add	ax, cx
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	not	ax
	mov	cx, ax
	pop	bx
	mov	al, [bx]
	mov	ah, 0
	and	ax, cx
	mov	cx, ax
	pop	bx
	mov	al, [bx]
	mov	ah, 0
	or	ax, cx
	mov	[bx], al
	mov	ah, 0
; }
	jmp	L117
L116:
; else
; {
; RPN'ized expression: "uip_reassbitmap offset 8 8 * / + *u bitmap_bits offset 8 / 7 & + *u |= "
; Expanded expression: "uip_reassbitmap (@-2) *(2) 64 /u + bitmap_bits (@-2) *(2) 8 /u 7 & + *(1) |=(1) "
; Fused expression:    "/u *(@-2) 64 + uip_reassbitmap ax push-ax /u *(@-2) 8 & ax 7 + bitmap_bits ax |=(153) **sp *ax "
	mov	ax, [bp-2]
	mov	dx, 0
	mov	cx, 64
	div	cx
	mov	cx, ax
	mov	ax, _uip_reassbitmap
	add	ax, cx
	push	ax
	mov	ax, [bp-2]
	mov	dx, 0
	mov	cx, 8
	div	cx
	and	ax, 7
	mov	cx, ax
	mov	ax, _bitmap_bits
	add	ax, cx
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	bx
	mov	al, [bx]
	mov	ah, 0
	or	ax, cx
	mov	[bx], al
	mov	ah, 0
; for
; RPN'ized expression: "i 1 offset 8 8 * / + = "
; Expanded expression: "(@-6) 1 (@-2) *(2) 64 /u + =(2) "
; Fused expression:    "/u *(@-2) 64 + 1 ax =(170) *(@-6) ax "
	mov	ax, [bp-2]
	mov	dx, 0
	mov	cx, 64
	div	cx
	mov	cx, ax
	mov	ax, 1
	add	ax, cx
	mov	[bp-6], ax
L118:
; RPN'ized expression: "i offset len + 8 8 * / < "
; Expanded expression: "(@-6) *(2) (@-2) *(2) (@-4) *(2) + 64 /u <u "
; Fused expression:    "+ *(@-2) *(@-4) /u ax 64 <u *(@-6) ax IF! "
	mov	ax, [bp-2]
	add	ax, [bp-4]
	mov	dx, 0
	mov	cx, 64
	div	cx
	mov	cx, ax
	mov	ax, [bp-6]
	cmp	ax, cx
	jae	L121
; RPN'ized expression: "i ++ "
; Expanded expression: "(@-6) ++(2) "
; {
; RPN'ized expression: "uip_reassbitmap i + *u 255 = "
; Expanded expression: "uip_reassbitmap (@-6) *(2) + 255 =(1) "
; Fused expression:    "+ uip_reassbitmap *(@-6) =(154) *ax 255 "
	mov	ax, _uip_reassbitmap
	add	ax, [bp-6]
	mov	bx, ax
	mov	ax, 255
	mov	[bx], al
	mov	ah, 0
; }
L119:
; Fused expression:    "++(2) *(@-6) "
	inc	word [bp-6]
	mov	ax, [bp-6]
	jmp	L118
L121:
; RPN'ized expression: "uip_reassbitmap offset len + 8 8 * / + *u bitmap_bits offset len + 8 / 7 & + *u ~ |= "
; Expanded expression: "uip_reassbitmap (@-2) *(2) (@-4) *(2) + 64 /u + bitmap_bits (@-2) *(2) (@-4) *(2) + 8 /u 7 & + *(1) ~ |=(1) "
; Fused expression:    "+ *(@-2) *(@-4) /u ax 64 + uip_reassbitmap ax push-ax + *(@-2) *(@-4) /u ax 8 & ax 7 + bitmap_bits ax *(1) ax ~ |=(154) **sp ax "
	mov	ax, [bp-2]
	add	ax, [bp-4]
	mov	dx, 0
	mov	cx, 64
	div	cx
	mov	cx, ax
	mov	ax, _uip_reassbitmap
	add	ax, cx
	push	ax
	mov	ax, [bp-2]
	add	ax, [bp-4]
	mov	dx, 0
	mov	cx, 8
	div	cx
	and	ax, 7
	mov	cx, ax
	mov	ax, _bitmap_bits
	add	ax, cx
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	not	ax
	mov	cx, ax
	pop	bx
	mov	al, [bx]
	mov	ah, 0
	or	ax, cx
	mov	[bx], al
	mov	ah, 0
; }
L117:
; if
; loc         <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something124) ipoffset -> *u 0 + *u 32 & 0 == "
; Expanded expression: "uip_buf 14 + 6 + *(1) 32 & 0 == "
; Fused expression:    "+ uip_buf 14 + ax 6 & *ax 32 == ax 0 IF! "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 6
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	and	ax, 32
	cmp	ax, 0
	jne	L122
; {
; RPN'ized expression: "uip_reassflags 1 |= "
; Expanded expression: "uip_reassflags 1 |=(1) "
; Fused expression:    "|=(154) *uip_reassflags 1 "
	mov	al, [_uip_reassflags]
	mov	ah, 0
	or	ax, 1
	mov	[_uip_reassflags], al
	mov	ah, 0
; RPN'ized expression: "uip_reasslen offset len + = "
; Expanded expression: "uip_reasslen (@-2) *(2) (@-4) *(2) + =(2) "
; Fused expression:    "+ *(@-2) *(@-4) =(170) *uip_reasslen ax "
	mov	ax, [bp-2]
	add	ax, [bp-4]
	mov	[_uip_reasslen], ax
; }
L122:
; if
; RPN'ized expression: "uip_reassflags 1 & "
; Expanded expression: "uip_reassflags *(1) 1 & "
; Fused expression:    "& *uip_reassflags 1  "
	mov	al, [_uip_reassflags]
	mov	ah, 0
	and	ax, 1
; JumpIfZero
	test	ax, ax
	je	L125
; {
; for
; RPN'ized expression: "i 0 = "
; Expanded expression: "(@-6) 0 =(2) "
; Fused expression:    "=(170) *(@-6) 0 "
	mov	ax, 0
	mov	[bp-6], ax
L127:
; RPN'ized expression: "i uip_reasslen 8 8 * / 1 - < "
; Expanded expression: "(@-6) *(2) uip_reasslen *(2) 64 /u 1 - <u "
; Fused expression:    "/u *uip_reasslen 64 - ax 1 <u *(@-6) ax IF! "
	mov	ax, [_uip_reasslen]
	mov	dx, 0
	mov	cx, 64
	div	cx
	dec	ax
	mov	cx, ax
	mov	ax, [bp-6]
	cmp	ax, cx
	jae	L130
; RPN'ized expression: "i ++ "
; Expanded expression: "(@-6) ++(2) "
; {
; if
; RPN'ized expression: "uip_reassbitmap i + *u 255 != "
; Expanded expression: "uip_reassbitmap (@-6) *(2) + *(1) 255 != "
; Fused expression:    "+ uip_reassbitmap *(@-6) != *ax 255 IF! "
	mov	ax, _uip_reassbitmap
	add	ax, [bp-6]
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 255
	je	L131
; {
; goto nullreturn
	jmp	L111
; }
L131:
; }
L128:
; Fused expression:    "++(2) *(@-6) "
	inc	word [bp-6]
	mov	ax, [bp-6]
	jmp	L127
L130:
; if
; loc             <something> : unsigned char
; RPN'ized expression: "uip_reassbitmap uip_reasslen 8 8 * / + *u bitmap_bits uip_reasslen 8 / 7 & + *u ~ (something135) != "
; Expanded expression: "uip_reassbitmap uip_reasslen *(2) 64 /u + *(1) bitmap_bits uip_reasslen *(2) 8 /u 7 & + *(1) ~ unsigned char != "
; Fused expression:    "/u *uip_reasslen 64 + uip_reassbitmap ax push-ax /u *uip_reasslen 8 & ax 7 + bitmap_bits ax *(1) ax ~ unsigned char != **sp ax IF! "
	mov	ax, [_uip_reasslen]
	mov	dx, 0
	mov	cx, 64
	div	cx
	mov	cx, ax
	mov	ax, _uip_reassbitmap
	add	ax, cx
	push	ax
	mov	ax, [_uip_reasslen]
	mov	dx, 0
	mov	cx, 8
	div	cx
	and	ax, 7
	mov	cx, ax
	mov	ax, _bitmap_bits
	add	ax, cx
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	not	ax
	and	ax, 255
	mov	cx, ax
	pop	bx
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, cx
	je	L133
; {
; goto nullreturn
	jmp	L111
; }
L133:
; RPN'ized expression: "uip_reasstmr 0 = "
; Expanded expression: "uip_reasstmr 0 =(1) "
; Fused expression:    "=(154) *uip_reasstmr 0 "
	mov	ax, 0
	mov	[_uip_reasstmr], al
	mov	ah, 0
; loc             <something> : * struct <something>
; loc             <something> : * struct <something>
; RPN'ized expression: "( uip_reasslen , uip_reassbuf 0 + *u &u (something137) , uip_buf 14 + *u &u (something136) memcpy ) "
; Expanded expression: " uip_reasslen *(2)  uip_reassbuf 0 +  uip_buf 14 +  memcpy ()6 "
; Fused expression:    "( *(2) uip_reasslen , + uip_reassbuf 0 , + uip_buf 14 , memcpy )6 "
	push	word [_uip_reasslen]
	mov	ax, _uip_reassbuf
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	push	ax
	call	_memcpy
	sub	sp, -6
; loc             <something> : * struct <something>
; loc             <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something138) ipoffset -> *u 0 + *u uip_buf 14 + *u &u (something139) ipoffset -> *u 1 + *u 0 = = "
; Expanded expression: "uip_buf 14 + 6 + uip_buf 14 + 7 + 0 =(1) =(1) "
; Fused expression:    "+ uip_buf 14 + ax 6 push-ax + uip_buf 14 + ax 7 =(154) *ax 0 =(154) **sp ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 6
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 7
	mov	bx, ax
	mov	ax, 0
	mov	[bx], al
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc             <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something140) len -> *u 0 + *u uip_reasslen 8 >> = "
; Expanded expression: "uip_buf 14 + 2 + uip_reasslen *(2) 8 >>u =(1) "
; Fused expression:    "+ uip_buf 14 + ax 2 push-ax >>u *uip_reasslen 8 =(154) **sp ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 2
	push	ax
	mov	ax, [_uip_reasslen]
	shr	ax, 8
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc             <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something141) len -> *u 1 + *u uip_reasslen 255 & = "
; Expanded expression: "uip_buf 14 + 3 + uip_reasslen *(2) 255 & =(1) "
; Fused expression:    "+ uip_buf 14 + ax 3 push-ax & *uip_reasslen 255 =(154) **sp ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 3
	push	ax
	mov	ax, [_uip_reasslen]
	and	ax, 255
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc             <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something142) ipchksum -> *u 0 = "
; Expanded expression: "uip_buf 14 + 10 + 0 =(2) "
; Fused expression:    "+ uip_buf 14 + ax 10 =(170) *ax 0 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 10
	mov	bx, ax
	mov	ax, 0
	mov	[bx], ax
; loc             <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something143) ipchksum -> *u ( uip_ipchksum ) ~ = "
; Expanded expression: "uip_buf 14 + 10 +  uip_ipchksum ()0 ~ =(2) "
; Fused expression:    "+ uip_buf 14 + ax 10 push-ax ( uip_ipchksum )0 ~ =(170) **sp ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 10
	push	ax
	call	_uip_ipchksum
	not	ax
	pop	bx
	mov	[bx], ax
; return
; RPN'ized expression: "uip_reasslen "
; Expanded expression: "uip_reasslen *(2) "
; Fused expression:    "*(2) uip_reasslen unsigned char  "
	mov	ax, [_uip_reasslen]
	and	ax, 255
	jmp	L79
; }
L125:
; }
L84:
; nullreturn:
L111:
; return
; RPN'ized expression: "0 "
; Expanded expression: "0 "
; Expression value: 0
; Fused expression:    "0  "
	mov	ax, 0
L79:
	leave
	ret

; glb uip_add_rcv_nxt : (
; prm     n : unsigned
;     ) void
section .text
_uip_add_rcv_nxt:
	push	bp
	mov	bp, sp
	;sub	sp,          0
; loc     n : (@4): unsigned
; RPN'ized expression: "( n , uip_conn rcv_nxt -> *u uip_add32 ) "
; Expanded expression: " (@4) *(2)  uip_conn *(2) 8 +  uip_add32 ()4 "
; Fused expression:    "( *(2) (@4) , + *uip_conn 8 , uip_add32 )4 "
	push	word [bp+4]
	mov	ax, [_uip_conn]
	add	ax, 8
	push	ax
	call	_uip_add32
	sub	sp, -4
; RPN'ized expression: "uip_conn rcv_nxt -> *u 0 + *u uip_acc32 0 + *u = "
; Expanded expression: "uip_conn *(2) 8 + uip_acc32 0 + *(1) =(1) "
; Fused expression:    "+ *uip_conn 8 push-ax + uip_acc32 0 =(153) **sp *ax "
	mov	ax, [_uip_conn]
	add	ax, 8
	push	ax
	mov	ax, _uip_acc32
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_conn rcv_nxt -> *u 1 + *u uip_acc32 1 + *u = "
; Expanded expression: "uip_conn *(2) 9 + uip_acc32 1 + *(1) =(1) "
; Fused expression:    "+ *uip_conn 9 push-ax + uip_acc32 1 =(153) **sp *ax "
	mov	ax, [_uip_conn]
	add	ax, 9
	push	ax
	mov	ax, _uip_acc32
	inc	ax
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_conn rcv_nxt -> *u 2 + *u uip_acc32 2 + *u = "
; Expanded expression: "uip_conn *(2) 10 + uip_acc32 2 + *(1) =(1) "
; Fused expression:    "+ *uip_conn 10 push-ax + uip_acc32 2 =(153) **sp *ax "
	mov	ax, [_uip_conn]
	add	ax, 10
	push	ax
	mov	ax, _uip_acc32
	add	ax, 2
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_conn rcv_nxt -> *u 3 + *u uip_acc32 3 + *u = "
; Expanded expression: "uip_conn *(2) 11 + uip_acc32 3 + *(1) =(1) "
; Fused expression:    "+ *uip_conn 11 push-ax + uip_acc32 3 =(153) **sp *ax "
	mov	ax, [_uip_conn]
	add	ax, 11
	push	ax
	mov	ax, _uip_acc32
	add	ax, 3
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
L144:
	leave
	ret

; glb uip_process : (
; prm     flag : unsigned char
;     ) void
section .text
	global	_uip_process
_uip_process:
	push	bp
	mov	bp, sp
	 sub	sp,          4
; loc     flag : (@4): unsigned char
; loc     uip_connr : (@-2): * struct uip_conn
; RPN'ized expression: "uip_connr uip_conn = "
; Expanded expression: "(@-2) uip_conn *(2) =(2) "
; Fused expression:    "=(170) *(@-2) *uip_conn "
	mov	ax, [_uip_conn]
	mov	[bp-2], ax
; RPN'ized expression: "uip_appdata uip_buf 40 14 + + *u &u = "
; Expanded expression: "uip_appdata uip_buf 54 + =(2) "
; Fused expression:    "+ uip_buf 54 =(170) *uip_appdata ax "
	mov	ax, _uip_buf
	add	ax, 54
	mov	[_uip_appdata], ax
; if
; RPN'ized expression: "flag 2 == "
; Expanded expression: "(@4) *(1) 2 == "
; Fused expression:    "== *(@4) 2 IF! "
	mov	al, [bp+4]
	mov	ah, 0
	cmp	ax, 2
	jne	L148
; {
; if
; RPN'ized expression: "uip_reasstmr 0 != "
; Expanded expression: "uip_reasstmr *(1) 0 != "
; Fused expression:    "!= *uip_reasstmr 0 IF! "
	mov	al, [_uip_reasstmr]
	mov	ah, 0
	cmp	ax, 0
	je	L150
; {
; RPN'ized expression: "uip_reasstmr -- "
; Expanded expression: "uip_reasstmr --(1) "
; Fused expression:    "--(1) *uip_reasstmr "
	dec	byte [_uip_reasstmr]
	mov	al, [_uip_reasstmr]
	mov	ah, 0
; }
L150:
; if
; RPN'ized expression: "iss 3 + *u ++ 0 == "
; Expanded expression: "iss 3 + ++(1) 0 == "
; Fused expression:    "+ iss 3 ++(1) *ax == ax 0 IF! "
	mov	ax, _iss
	add	ax, 3
	mov	bx, ax
	inc	byte [bx]
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 0
	jne	L152
; {
; if
; RPN'ized expression: "iss 2 + *u ++ 0 == "
; Expanded expression: "iss 2 + ++(1) 0 == "
; Fused expression:    "+ iss 2 ++(1) *ax == ax 0 IF! "
	mov	ax, _iss
	add	ax, 2
	mov	bx, ax
	inc	byte [bx]
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 0
	jne	L154
; {
; if
; RPN'ized expression: "iss 1 + *u ++ 0 == "
; Expanded expression: "iss 1 + ++(1) 0 == "
; Fused expression:    "+ iss 1 ++(1) *ax == ax 0 IF! "
	mov	ax, _iss
	inc	ax
	mov	bx, ax
	inc	byte [bx]
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 0
	jne	L156
; {
; RPN'ized expression: "iss 0 + *u ++ "
; Expanded expression: "iss 0 + ++(1) "
; Fused expression:    "+ iss 0 ++(1) *ax "
	mov	ax, _iss
	mov	bx, ax
	inc	byte [bx]
	mov	al, [bx]
	mov	ah, 0
; }
L156:
; }
L154:
; }
L152:
; RPN'ized expression: "uip_len 0 = "
; Expanded expression: "uip_len 0 =(2) "
; Fused expression:    "=(170) *uip_len 0 "
	mov	ax, 0
	mov	[_uip_len], ax
; if
; RPN'ized expression: "uip_connr tcpstateflags -> *u 7 == uip_connr tcpstateflags -> *u 5 == || "
; Expanded expression: "(@-2) *(2) 25 + *(1) 7 == [sh||->160] (@-2) *(2) 25 + *(1) 5 == ||[160] "
; Fused expression:    "+ *(@-2) 25 == *ax 7 [sh||->160] + *(@-2) 25 == *ax 5 ||[160]  "
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 7
	sete	al
	cbw
; JumpIfNotZero
	test	ax, ax
	jne	L160
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 5
	sete	al
	cbw
L160:
; JumpIfZero
	test	ax, ax
	je	L158
; {
; RPN'ized expression: "uip_connr timer -> *u ++ "
; Expanded expression: "(@-2) *(2) 26 + ++(1) "
; Fused expression:    "+ *(@-2) 26 ++(1) *ax "
	mov	ax, [bp-2]
	add	ax, 26
	mov	bx, ax
	inc	byte [bx]
	mov	al, [bx]
	mov	ah, 0
; if
; RPN'ized expression: "uip_connr timer -> *u 120 == "
; Expanded expression: "(@-2) *(2) 26 + *(1) 120 == "
; Fused expression:    "+ *(@-2) 26 == *ax 120 IF! "
	mov	ax, [bp-2]
	add	ax, 26
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 120
	jne	L161
; {
; RPN'ized expression: "uip_connr tcpstateflags -> *u 0 = "
; Expanded expression: "(@-2) *(2) 25 + 0 =(1) "
; Fused expression:    "+ *(@-2) 25 =(154) *ax 0 "
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	ax, 0
	mov	[bx], al
	mov	ah, 0
; }
L161:
; }
	jmp	L159
L158:
; else
; if
; RPN'ized expression: "uip_connr tcpstateflags -> *u 0 != "
; Expanded expression: "(@-2) *(2) 25 + *(1) 0 != "
; Fused expression:    "+ *(@-2) 25 != *ax 0 IF! "
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 0
	je	L163
; {
; if
; RPN'ized expression: "uip_connr len -> *u "
; Expanded expression: "(@-2) *(2) 16 + *(2) "
; Fused expression:    "+ *(@-2) 16 *(2) ax  "
	mov	ax, [bp-2]
	add	ax, 16
	mov	bx, ax
	mov	ax, [bx]
; JumpIfZero
	test	ax, ax
	je	L165
; {
; if
; RPN'ized expression: "uip_connr timer -> *u --p 0 == "
; Expanded expression: "(@-2) *(2) 26 + --p(1) 0 == "
; Fused expression:    "+ *(@-2) 26 --p(1) *ax == ax 0 IF! "
	mov	ax, [bp-2]
	add	ax, 26
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	dec	byte [bx]
	cmp	ax, 0
	jne	L167
; {
; if
; RPN'ized expression: "uip_connr nrtx -> *u 8 == uip_connr tcpstateflags -> *u 2 == uip_connr tcpstateflags -> *u 1 == || uip_connr nrtx -> *u 3 == && || "
; Expanded expression: "(@-2) *(2) 27 + *(1) 8 == [sh||->171] (@-2) *(2) 25 + *(1) 2 == [sh||->173] (@-2) *(2) 25 + *(1) 1 == ||[173] _Bool [sh&&->172] (@-2) *(2) 27 + *(1) 3 == &&[172] _Bool ||[171] "
; Fused expression:    "+ *(@-2) 27 == *ax 8 [sh||->171] + *(@-2) 25 == *ax 2 [sh||->173] + *(@-2) 25 == *ax 1 ||[173] _Bool [sh&&->172] + *(@-2) 27 == *ax 3 &&[172] _Bool ||[171]  "
	mov	ax, [bp-2]
	add	ax, 27
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 8
	sete	al
	cbw
; JumpIfNotZero
	test	ax, ax
	jne	L171
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 2
	sete	al
	cbw
; JumpIfNotZero
	test	ax, ax
	jne	L173
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 1
	sete	al
	cbw
L173:
	test	ax, ax
	setne	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L172
	mov	ax, [bp-2]
	add	ax, 27
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 3
	sete	al
	cbw
L172:
	test	ax, ax
	setne	al
	cbw
L171:
; JumpIfZero
	test	ax, ax
	je	L169
; {
; RPN'ized expression: "uip_connr tcpstateflags -> *u 0 = "
; Expanded expression: "(@-2) *(2) 25 + 0 =(1) "
; Fused expression:    "+ *(@-2) 25 =(154) *ax 0 "
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	ax, 0
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_flags 128 = "
; Expanded expression: "uip_flags 128 =(1) "
; Fused expression:    "=(154) *uip_flags 128 "
	mov	ax, 128
	mov	[_uip_flags], al
	mov	ah, 0
; RPN'ized expression: "( tcp_appcall ) "
; Expanded expression: " tcp_appcall ()0 "
; Fused expression:    "( tcp_appcall )0 "
	call	_tcp_appcall
; loc                         <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something174) flags -> *u 4 16 | = "
; Expanded expression: "uip_buf 14 + 33 + 20 =(1) "
; Fused expression:    "+ uip_buf 14 + ax 33 =(154) *ax 20 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 33
	mov	bx, ax
	mov	ax, 20
	mov	[bx], al
	mov	ah, 0
; goto tcp_send_nodata
	jmp	L175
; }
L169:
; RPN'ized expression: "uip_connr timer -> *u 3 uip_connr nrtx -> *u 4 > uip_connr nrtx -> *u 4 ? << = "
; Expanded expression: "(@-2) *(2) 26 + 3 (@-2) *(2) 27 + *(1) 4 > [sh||->176] (@-2) *(2) 27 + *(1) goto &&[176] 4 &&[177] << =(1) "
; Fused expression:    "+ *(@-2) 26 push-ax + *(@-2) 27 > *ax 4 [sh||->176] + *(@-2) 27 *(1) ax [goto->177] &&[176] 4 &&[177] << 3 ax =(154) **sp ax "
	mov	ax, [bp-2]
	add	ax, 26
	push	ax
	mov	ax, [bp-2]
	add	ax, 27
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 4
	setg	al
	cbw
; JumpIfNotZero
	test	ax, ax
	jne	L176
	mov	ax, [bp-2]
	add	ax, 27
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	jmp	L177
L176:
	mov	ax, 4
L177:
	mov	cx, ax
	mov	ax, 3
	shl	ax, cl
	pop	bx
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_connr nrtx -> *u ++ "
; Expanded expression: "(@-2) *(2) 27 + ++(1) "
; Fused expression:    "+ *(@-2) 27 ++(1) *ax "
	mov	ax, [bp-2]
	add	ax, 27
	mov	bx, ax
	inc	byte [bx]
	mov	al, [bx]
	mov	ah, 0
; RPN'ized expression: "uip_stat &u tcp -> *u &u rexmit -> *u ++ "
; Expanded expression: "uip_stat 26 + 12 + ++(2) "
; Fused expression:    "+ uip_stat 26 + ax 12 ++(2) *ax "
	mov	ax, _uip_stat
	add	ax, 26
	add	ax, 12
	mov	bx, ax
	inc	word [bx]
	mov	ax, [bx]
; switch
; RPN'ized expression: "uip_connr tcpstateflags -> *u 15 & "
; Expanded expression: "(@-2) *(2) 25 + *(1) 15 & "
; Fused expression:    "+ *(@-2) 25 & *ax 15  "
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	and	ax, 15
	jmp	L179
; {
; case
; RPN'ized expression: "1 "
; Expanded expression: "1 "
; Expression value: 1
L180:
; goto tcp_send_synack
	jmp	L181
; case
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
L182:
; loc                         <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something183) flags -> *u 0 = "
; Expanded expression: "uip_buf 14 + 33 + 0 =(1) "
; Fused expression:    "+ uip_buf 14 + ax 33 =(154) *ax 0 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 33
	mov	bx, ax
	mov	ax, 0
	mov	[bx], al
	mov	ah, 0
; goto tcp_send_syn
	jmp	L184
; case
; RPN'ized expression: "3 "
; Expanded expression: "3 "
; Expression value: 3
L185:
; RPN'ized expression: "uip_len 0 = "
; Expanded expression: "uip_len 0 =(2) "
; Fused expression:    "=(170) *uip_len 0 "
	mov	ax, 0
	mov	[_uip_len], ax
; RPN'ized expression: "uip_slen 0 = "
; Expanded expression: "uip_slen 0 =(2) "
; Fused expression:    "=(170) *uip_slen 0 "
	mov	ax, 0
	mov	[_uip_slen], ax
; RPN'ized expression: "uip_flags 4 = "
; Expanded expression: "uip_flags 4 =(1) "
; Fused expression:    "=(154) *uip_flags 4 "
	mov	ax, 4
	mov	[_uip_flags], al
	mov	ah, 0
; RPN'ized expression: "( tcp_appcall ) "
; Expanded expression: " tcp_appcall ()0 "
; Fused expression:    "( tcp_appcall )0 "
	call	_tcp_appcall
; goto apprexmit
	jmp	L186
; case
; RPN'ized expression: "4 "
; Expanded expression: "4 "
; Expression value: 4
L187:
; case
; RPN'ized expression: "6 "
; Expanded expression: "6 "
; Expression value: 6
L188:
; case
; RPN'ized expression: "8 "
; Expanded expression: "8 "
; Expression value: 8
L189:
; goto tcp_send_finack
	jmp	L190
; }
	jmp	L178
L179:
	cmp	ax, 1
	je	L180
	cmp	ax, 2
	je	L182
	cmp	ax, 3
	je	L185
	cmp	ax, 4
	je	L187
	cmp	ax, 6
	je	L188
	cmp	ax, 8
	je	L189
L178:
; }
L167:
; }
	jmp	L166
L165:
; else
; if
; RPN'ized expression: "uip_connr tcpstateflags -> *u 15 & 3 == "
; Expanded expression: "(@-2) *(2) 25 + *(1) 15 & 3 == "
; Fused expression:    "+ *(@-2) 25 & *ax 15 == ax 3 IF! "
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	and	ax, 15
	cmp	ax, 3
	jne	L191
; {
; RPN'ized expression: "uip_len 0 = "
; Expanded expression: "uip_len 0 =(2) "
; Fused expression:    "=(170) *uip_len 0 "
	mov	ax, 0
	mov	[_uip_len], ax
; RPN'ized expression: "uip_slen 0 = "
; Expanded expression: "uip_slen 0 =(2) "
; Fused expression:    "=(170) *uip_slen 0 "
	mov	ax, 0
	mov	[_uip_slen], ax
; RPN'ized expression: "uip_flags 8 = "
; Expanded expression: "uip_flags 8 =(1) "
; Fused expression:    "=(154) *uip_flags 8 "
	mov	ax, 8
	mov	[_uip_flags], al
	mov	ah, 0
; RPN'ized expression: "( tcp_appcall ) "
; Expanded expression: " tcp_appcall ()0 "
; Fused expression:    "( tcp_appcall )0 "
	call	_tcp_appcall
; goto appsend
	jmp	L193
; }
L191:
L166:
; }
L163:
L159:
; goto drop
	jmp	L194
; }
L148:
; if
; RPN'ized expression: "flag 3 == "
; Expanded expression: "(@4) *(1) 3 == "
; Fused expression:    "== *(@4) 3 IF! "
	mov	al, [bp+4]
	mov	ah, 0
	cmp	ax, 3
	jne	L195
; {
; if
; RPN'ized expression: "uip_udp_conn lport -> *u 0 != "
; Expanded expression: "uip_udp_conn *(2) 4 + *(2) 0 != "
; Fused expression:    "+ *uip_udp_conn 4 != *ax 0 IF! "
	mov	ax, [_uip_udp_conn]
	add	ax, 4
	mov	bx, ax
	mov	ax, [bx]
	cmp	ax, 0
	je	L197
; {
; RPN'ized expression: "uip_appdata uip_buf 14 28 + + *u &u = "
; Expanded expression: "uip_appdata uip_buf 42 + =(2) "
; Fused expression:    "+ uip_buf 42 =(170) *uip_appdata ax "
	mov	ax, _uip_buf
	add	ax, 42
	mov	[_uip_appdata], ax
; RPN'ized expression: "uip_len uip_slen 0 = = "
; Expanded expression: "uip_len uip_slen 0 =(2) =(2) "
; Fused expression:    "=(170) *uip_slen 0 =(170) *uip_len ax "
	mov	ax, 0
	mov	[_uip_slen], ax
	mov	[_uip_len], ax
; RPN'ized expression: "uip_flags 8 = "
; Expanded expression: "uip_flags 8 =(1) "
; Fused expression:    "=(154) *uip_flags 8 "
	mov	ax, 8
	mov	[_uip_flags], al
	mov	ah, 0
; RPN'ized expression: "( udp_appcall ) "
; Expanded expression: " udp_appcall ()0 "
; Fused expression:    "( udp_appcall )0 "
	call	_udp_appcall
; goto udp_send
	jmp	L199
; }
	jmp	L198
L197:
; else
; {
; goto drop
	jmp	L194
; }
L198:
; }
L195:
; RPN'ized expression: "uip_stat &u ip -> *u &u recv -> *u ++ "
; Expanded expression: "uip_stat 0 + 2 + ++(2) "
; Fused expression:    "+ uip_stat 0 + ax 2 ++(2) *ax "
	mov	ax, _uip_stat
	add	ax, 2
	mov	bx, ax
	inc	word [bx]
	mov	ax, [bx]
; if
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something202) vhl -> *u 69 != "
; Expanded expression: "uip_buf 14 + 0 + *(1) 69 != "
; Fused expression:    "+ uip_buf 14 + ax 0 != *ax 69 IF! "
	mov	ax, _uip_buf
	add	ax, 14
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 69
	je	L200
; {
; RPN'ized expression: "uip_stat &u ip -> *u &u drop -> *u ++ "
; Expanded expression: "uip_stat 0 + 0 + ++(2) "
; Fused expression:    "+ uip_stat 0 + ax 0 ++(2) *ax "
	mov	ax, _uip_stat
	mov	bx, ax
	inc	word [bx]
	mov	ax, [bx]
; RPN'ized expression: "uip_stat &u ip -> *u &u vhlerr -> *u ++ "
; Expanded expression: "uip_stat 0 + 6 + ++(2) "
; Fused expression:    "+ uip_stat 0 + ax 6 ++(2) *ax "
	mov	ax, _uip_stat
	add	ax, 6
	mov	bx, ax
	inc	word [bx]
	mov	ax, [bx]
; goto drop
	jmp	L194
; }
L200:
; if
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something205) len -> *u 0 + *u uip_len 8 >> != "
; Expanded expression: "uip_buf 14 + 2 + *(1) uip_len *(2) 8 >>u != "
; Fused expression:    "+ uip_buf 14 + ax 2 push-ax >>u *uip_len 8 != **sp ax IF! "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 2
	push	ax
	mov	ax, [_uip_len]
	shr	ax, 8
	mov	cx, ax
	pop	bx
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, cx
	je	L203
; {
; loc         <something> : * struct <something>
; RPN'ized expression: "uip_len uip_len 255 & uip_buf 14 + *u &u (something206) len -> *u 0 + *u 8 << | = "
; Expanded expression: "uip_len uip_len *(2) 255 & uip_buf 14 + 2 + *(1) 8 << | =(2) "
; Fused expression:    "& *uip_len 255 push-ax + uip_buf 14 + ax 2 << *ax 8 | *sp ax =(170) *uip_len ax "
	mov	ax, [_uip_len]
	and	ax, 255
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 2
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	shl	ax, 8
	mov	cx, ax
	pop	ax
	or	ax, cx
	mov	[_uip_len], ax
; }
L203:
; if
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something209) len -> *u 1 + *u uip_len 255 & != "
; Expanded expression: "uip_buf 14 + 3 + *(1) uip_len *(2) 255 & != "
; Fused expression:    "+ uip_buf 14 + ax 3 push-ax & *uip_len 255 != **sp ax IF! "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 3
	push	ax
	mov	ax, [_uip_len]
	and	ax, 255
	mov	cx, ax
	pop	bx
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, cx
	je	L207
; {
; loc         <something> : * struct <something>
; RPN'ized expression: "uip_len uip_len 65280u & uip_buf 14 + *u &u (something210) len -> *u 1 + *u | = "
; Expanded expression: "uip_len uip_len *(2) 65280u & uip_buf 14 + 3 + *(1) | =(2) "
; Fused expression:    "& *uip_len 65280u push-ax + uip_buf 14 + ax 3 | *sp *ax =(170) *uip_len ax "
	mov	ax, [_uip_len]
	and	ax, -256
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 3
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	ax
	or	ax, cx
	mov	[_uip_len], ax
; }
L207:
; if
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something213) ipoffset -> *u 0 + *u 63 & 0 != uip_buf 14 + *u &u (something214) ipoffset -> *u 1 + *u 0 != || "
; Expanded expression: "uip_buf 14 + 6 + *(1) 63 & 0 != [sh||->215] uip_buf 14 + 7 + *(1) 0 != ||[215] "
; Fused expression:    "+ uip_buf 14 + ax 6 & *ax 63 != ax 0 [sh||->215] + uip_buf 14 + ax 7 != *ax 0 ||[215]  "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 6
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	and	ax, 63
	cmp	ax, 0
	setne	al
	cbw
; JumpIfNotZero
	test	ax, ax
	jne	L215
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 7
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 0
	setne	al
	cbw
L215:
; JumpIfZero
	test	ax, ax
	je	L211
; {
; RPN'ized expression: "uip_len ( uip_reass ) = "
; Expanded expression: "uip_len  uip_reass ()0 =(2) "
; Fused expression:    "( uip_reass )0 =(170) *uip_len ax "
	call	_uip_reass
	mov	[_uip_len], ax
; if
; RPN'ized expression: "uip_len 0 == "
; Expanded expression: "uip_len *(2) 0 == "
; Fused expression:    "== *uip_len 0 IF! "
	mov	ax, [_uip_len]
	cmp	ax, 0
	jne	L216
; {
; goto drop
	jmp	L194
; }
L216:
; }
L211:
; if
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something220) destipaddr -> *u 0 + *u uip_hostaddr 0 + *u != "
; Expanded expression: "uip_buf 14 + 16 + *(2) uip_hostaddr 0 + *(2) != "
; Fused expression:    "+ uip_buf 14 + ax 16 push-ax + uip_hostaddr 0 != **sp *ax IF! "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 16
	push	ax
	mov	ax, _uip_hostaddr
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	cmp	ax, cx
	je	L218
; {
; RPN'ized expression: "uip_stat &u ip -> *u &u drop -> *u ++ "
; Expanded expression: "uip_stat 0 + 0 + ++(2) "
; Fused expression:    "+ uip_stat 0 + ax 0 ++(2) *ax "
	mov	ax, _uip_stat
	mov	bx, ax
	inc	word [bx]
	mov	ax, [bx]
; goto drop
	jmp	L194
; }
L218:
; if
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something223) destipaddr -> *u 1 + *u uip_hostaddr 1 + *u != "
; Expanded expression: "uip_buf 14 + 18 + *(2) uip_hostaddr 2 + *(2) != "
; Fused expression:    "+ uip_buf 14 + ax 18 push-ax + uip_hostaddr 2 != **sp *ax IF! "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 18
	push	ax
	mov	ax, _uip_hostaddr
	add	ax, 2
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	cmp	ax, cx
	je	L221
; {
; RPN'ized expression: "uip_stat &u ip -> *u &u drop -> *u ++ "
; Expanded expression: "uip_stat 0 + 0 + ++(2) "
; Fused expression:    "+ uip_stat 0 + ax 0 ++(2) *ax "
	mov	ax, _uip_stat
	mov	bx, ax
	inc	word [bx]
	mov	ax, [bx]
; goto drop
	jmp	L194
; }
L221:
; if
; RPN'ized expression: "( uip_ipchksum ) 65535u != "
; Expanded expression: " uip_ipchksum ()0 65535u != "
; Fused expression:    "( uip_ipchksum )0 != ax 65535u IF! "
	call	_uip_ipchksum
	cmp	ax, -1
	je	L224
; {
; RPN'ized expression: "uip_stat &u ip -> *u &u drop -> *u ++ "
; Expanded expression: "uip_stat 0 + 0 + ++(2) "
; Fused expression:    "+ uip_stat 0 + ax 0 ++(2) *ax "
	mov	ax, _uip_stat
	mov	bx, ax
	inc	word [bx]
	mov	ax, [bx]
; RPN'ized expression: "uip_stat &u ip -> *u &u chkerr -> *u ++ "
; Expanded expression: "uip_stat 0 + 14 + ++(2) "
; Fused expression:    "+ uip_stat 0 + ax 14 ++(2) *ax "
	mov	ax, _uip_stat
	add	ax, 14
	mov	bx, ax
	inc	word [bx]
	mov	ax, [bx]
; goto drop
	jmp	L194
; }
L224:
; if
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something228) proto -> *u 6 == "
; Expanded expression: "uip_buf 14 + 9 + *(1) 6 == "
; Fused expression:    "+ uip_buf 14 + ax 9 == *ax 6 IF! "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 9
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 6
	jne	L226
; goto tcp_input
	jmp	L229
L226:
; if
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something232) proto -> *u 17 == "
; Expanded expression: "uip_buf 14 + 9 + *(1) 17 == "
; Fused expression:    "+ uip_buf 14 + ax 9 == *ax 17 IF! "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 9
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 17
	jne	L230
; goto udp_input
	jmp	L233
L230:
; if
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something236) proto -> *u 1 != "
; Expanded expression: "uip_buf 14 + 9 + *(1) 1 != "
; Fused expression:    "+ uip_buf 14 + ax 9 != *ax 1 IF! "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 9
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 1
	je	L234
; {
; RPN'ized expression: "uip_stat &u ip -> *u &u drop -> *u ++ "
; Expanded expression: "uip_stat 0 + 0 + ++(2) "
; Fused expression:    "+ uip_stat 0 + ax 0 ++(2) *ax "
	mov	ax, _uip_stat
	mov	bx, ax
	inc	word [bx]
	mov	ax, [bx]
; RPN'ized expression: "uip_stat &u ip -> *u &u protoerr -> *u ++ "
; Expanded expression: "uip_stat 0 + 16 + ++(2) "
; Fused expression:    "+ uip_stat 0 + ax 16 ++(2) *ax "
	mov	ax, _uip_stat
	add	ax, 16
	mov	bx, ax
	inc	word [bx]
	mov	ax, [bx]
; goto drop
	jmp	L194
; }
L234:
; icmp_input:
L237:
; RPN'ized expression: "uip_stat &u icmp -> *u &u recv -> *u ++ "
; Expanded expression: "uip_stat 18 + 2 + ++(2) "
; Fused expression:    "+ uip_stat 18 + ax 2 ++(2) *ax "
	mov	ax, _uip_stat
	add	ax, 18
	add	ax, 2
	mov	bx, ax
	inc	word [bx]
	mov	ax, [bx]
; if
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something240) type -> *u 8 != "
; Expanded expression: "uip_buf 14 + 20 + *(1) 8 != "
; Fused expression:    "+ uip_buf 14 + ax 20 != *ax 8 IF! "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 20
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 8
	je	L238
; {
; RPN'ized expression: "uip_stat &u icmp -> *u &u drop -> *u ++ "
; Expanded expression: "uip_stat 18 + 0 + ++(2) "
; Fused expression:    "+ uip_stat 18 + ax 0 ++(2) *ax "
	mov	ax, _uip_stat
	add	ax, 18
	mov	bx, ax
	inc	word [bx]
	mov	ax, [bx]
; RPN'ized expression: "uip_stat &u icmp -> *u &u typeerr -> *u ++ "
; Expanded expression: "uip_stat 18 + 6 + ++(2) "
; Fused expression:    "+ uip_stat 18 + ax 6 ++(2) *ax "
	mov	ax, _uip_stat
	add	ax, 18
	add	ax, 6
	mov	bx, ax
	inc	word [bx]
	mov	ax, [bx]
; goto drop
	jmp	L194
; }
L238:
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something241) type -> *u 0 = "
; Expanded expression: "uip_buf 14 + 20 + 0 =(1) "
; Fused expression:    "+ uip_buf 14 + ax 20 =(154) *ax 0 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 20
	mov	bx, ax
	mov	ax, 0
	mov	[bx], al
	mov	ah, 0
; if
; loc     <something> : * struct <something>
; loc     <something> : unsigned
; RPN'ized expression: "uip_buf 14 + *u &u (something244) icmpchksum -> *u 65535u 8 8 << - 255 & (something245) 8 << 65535u 8 8 << - 65280u & 8 >> | >= "
; Expanded expression: "uip_buf 14 + 22 + *(2) 65527u >=u "
; Fused expression:    "+ uip_buf 14 + ax 22 >=u *ax 65527u IF! "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 22
	mov	bx, ax
	mov	ax, [bx]
	cmp	ax, -9
	jb	L242
; {
; loc         <something> : * struct <something>
; loc         <something> : unsigned
; RPN'ized expression: "uip_buf 14 + *u &u (something246) icmpchksum -> *u 8 8 << 255 & (something247) 8 << 8 8 << 65280u & 8 >> | 1 + += "
; Expanded expression: "uip_buf 14 + 22 + 9u +=(2) "
; Fused expression:    "+ uip_buf 14 + ax 22 +=(170) *ax 9u "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 22
	mov	bx, ax
	mov	ax, [bx]
	add	ax, 9
	mov	[bx], ax
; }
	jmp	L243
L242:
; else
; {
; loc         <something> : * struct <something>
; loc         <something> : unsigned
; RPN'ized expression: "uip_buf 14 + *u &u (something248) icmpchksum -> *u 8 8 << 255 & (something249) 8 << 8 8 << 65280u & 8 >> | += "
; Expanded expression: "uip_buf 14 + 22 + 8u +=(2) "
; Fused expression:    "+ uip_buf 14 + ax 22 +=(170) *ax 8u "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 22
	mov	bx, ax
	mov	ax, [bx]
	add	ax, 8
	mov	[bx], ax
; }
L243:
; loc     <something> : * struct <something>
; RPN'ized expression: "tmp16 uip_buf 14 + *u &u (something250) destipaddr -> *u 0 + *u = "
; Expanded expression: "tmp16 uip_buf 14 + 16 + *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 16 =(170) *tmp16 *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 16
	mov	bx, ax
	mov	ax, [bx]
	mov	[_tmp16], ax
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something251) destipaddr -> *u 0 + *u uip_buf 14 + *u &u (something252) srcipaddr -> *u 0 + *u = "
; Expanded expression: "uip_buf 14 + 16 + uip_buf 14 + 12 + *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 16 push-ax + uip_buf 14 + ax 12 =(170) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 16
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 12
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something253) srcipaddr -> *u 0 + *u tmp16 = "
; Expanded expression: "uip_buf 14 + 12 + tmp16 *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 12 =(170) *ax *tmp16 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 12
	mov	bx, ax
	mov	ax, [_tmp16]
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "tmp16 uip_buf 14 + *u &u (something254) destipaddr -> *u 1 + *u = "
; Expanded expression: "tmp16 uip_buf 14 + 18 + *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 18 =(170) *tmp16 *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 18
	mov	bx, ax
	mov	ax, [bx]
	mov	[_tmp16], ax
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something255) destipaddr -> *u 1 + *u uip_buf 14 + *u &u (something256) srcipaddr -> *u 1 + *u = "
; Expanded expression: "uip_buf 14 + 18 + uip_buf 14 + 14 + *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 18 push-ax + uip_buf 14 + ax 14 =(170) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 18
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 14
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something257) srcipaddr -> *u 1 + *u tmp16 = "
; Expanded expression: "uip_buf 14 + 14 + tmp16 *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 14 =(170) *ax *tmp16 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 14
	mov	bx, ax
	mov	ax, [_tmp16]
	mov	[bx], ax
; RPN'ized expression: "uip_stat &u icmp -> *u &u sent -> *u ++ "
; Expanded expression: "uip_stat 18 + 4 + ++(2) "
; Fused expression:    "+ uip_stat 18 + ax 4 ++(2) *ax "
	mov	ax, _uip_stat
	add	ax, 18
	add	ax, 4
	mov	bx, ax
	inc	word [bx]
	mov	ax, [bx]
; goto send
	jmp	L258
; udp_input:
L233:
; for
; RPN'ized expression: "uip_udp_conn uip_udp_conns 0 + *u &u = "
; Expanded expression: "uip_udp_conn uip_udp_conns 0 + =(2) "
; Fused expression:    "+ uip_udp_conns 0 =(170) *uip_udp_conn ax "
	mov	ax, _uip_udp_conns
	mov	[_uip_udp_conn], ax
L259:
; RPN'ized expression: "uip_udp_conn uip_udp_conns 128 + *u &u < "
; Expanded expression: "uip_udp_conn *(2) uip_udp_conns 1024 + <u "
; Fused expression:    "+ uip_udp_conns 1024 <u *uip_udp_conn ax IF! "
	mov	ax, _uip_udp_conns
	add	ax, 1024
	mov	cx, ax
	mov	ax, [_uip_udp_conn]
	cmp	ax, cx
	jae	L262
; RPN'ized expression: "uip_udp_conn ++ "
; Expanded expression: "uip_udp_conn 8 +=(2) "
; {
; if
; loc         <something> : * struct <something>
; loc         <something> : * struct <something>
; loc         <something> : * struct <something>
; loc         <something> : * struct <something>
; RPN'ized expression: "uip_udp_conn lport -> *u 0 != uip_buf 14 + *u &u (something265) destport -> *u uip_udp_conn lport -> *u == && uip_udp_conn rport -> *u 0 == uip_buf 14 + *u &u (something266) srcport -> *u uip_udp_conn rport -> *u == || && uip_buf 14 + *u &u (something267) srcipaddr -> *u 0 + *u uip_udp_conn ripaddr -> *u 0 + *u == && uip_buf 14 + *u &u (something268) srcipaddr -> *u 1 + *u uip_udp_conn ripaddr -> *u 1 + *u == && "
; Expanded expression: "uip_udp_conn *(2) 4 + *(2) 0 != [sh&&->273] uip_buf 14 + 22 + *(2) uip_udp_conn *(2) 4 + *(2) == &&[273] _Bool [sh&&->271] uip_udp_conn *(2) 6 + *(2) 0 == [sh||->272] uip_buf 14 + 20 + *(2) uip_udp_conn *(2) 6 + *(2) == ||[272] _Bool &&[271] _Bool [sh&&->270] uip_buf 14 + 12 + *(2) uip_udp_conn *(2) 0 + *(2) == &&[270] _Bool [sh&&->269] uip_buf 14 + 14 + *(2) uip_udp_conn *(2) 2 + *(2) == &&[269] "
; Fused expression:    "+ *uip_udp_conn 4 != *ax 0 [sh&&->273] + uip_buf 14 + ax 22 push-ax + *uip_udp_conn 4 == **sp *ax &&[273] _Bool [sh&&->271] + *uip_udp_conn 6 == *ax 0 [sh||->272] + uip_buf 14 + ax 20 push-ax + *uip_udp_conn 6 == **sp *ax ||[272] _Bool &&[271] _Bool [sh&&->270] + uip_buf 14 + ax 12 push-ax + *uip_udp_conn 0 == **sp *ax &&[270] _Bool [sh&&->269] + uip_buf 14 + ax 14 push-ax + *uip_udp_conn 2 == **sp *ax &&[269]  "
	mov	ax, [_uip_udp_conn]
	add	ax, 4
	mov	bx, ax
	mov	ax, [bx]
	cmp	ax, 0
	setne	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L273
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 22
	push	ax
	mov	ax, [_uip_udp_conn]
	add	ax, 4
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	cmp	ax, cx
	sete	al
	cbw
L273:
	test	ax, ax
	setne	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L271
	mov	ax, [_uip_udp_conn]
	add	ax, 6
	mov	bx, ax
	mov	ax, [bx]
	cmp	ax, 0
	sete	al
	cbw
; JumpIfNotZero
	test	ax, ax
	jne	L272
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 20
	push	ax
	mov	ax, [_uip_udp_conn]
	add	ax, 6
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	cmp	ax, cx
	sete	al
	cbw
L272:
	test	ax, ax
	setne	al
	cbw
L271:
	test	ax, ax
	setne	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L270
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 12
	push	ax
	mov	ax, [_uip_udp_conn]
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	cmp	ax, cx
	sete	al
	cbw
L270:
	test	ax, ax
	setne	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L269
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 14
	push	ax
	mov	ax, [_uip_udp_conn]
	add	ax, 2
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	cmp	ax, cx
	sete	al
	cbw
L269:
; JumpIfZero
	test	ax, ax
	je	L263
; {
; goto udp_found
	jmp	L274
; }
L263:
; }
L260:
; Fused expression:    "+=(170) *uip_udp_conn 8 "
	mov	ax, [_uip_udp_conn]
	add	ax, 8
	mov	[_uip_udp_conn], ax
	jmp	L259
L262:
; goto drop
	jmp	L194
; udp_found:
L274:
; RPN'ized expression: "uip_len uip_len 28 - = "
; Expanded expression: "uip_len uip_len *(2) 28 - =(2) "
; Fused expression:    "- *uip_len 28 =(170) *uip_len ax "
	mov	ax, [_uip_len]
	sub	ax, 28
	mov	[_uip_len], ax
; RPN'ized expression: "uip_appdata uip_buf 14 28 + + *u &u = "
; Expanded expression: "uip_appdata uip_buf 42 + =(2) "
; Fused expression:    "+ uip_buf 42 =(170) *uip_appdata ax "
	mov	ax, _uip_buf
	add	ax, 42
	mov	[_uip_appdata], ax
; RPN'ized expression: "uip_flags 2 = "
; Expanded expression: "uip_flags 2 =(1) "
; Fused expression:    "=(154) *uip_flags 2 "
	mov	ax, 2
	mov	[_uip_flags], al
	mov	ah, 0
; RPN'ized expression: "uip_slen 0 = "
; Expanded expression: "uip_slen 0 =(2) "
; Fused expression:    "=(170) *uip_slen 0 "
	mov	ax, 0
	mov	[_uip_slen], ax
; RPN'ized expression: "( udp_appcall ) "
; Expanded expression: " udp_appcall ()0 "
; Fused expression:    "( udp_appcall )0 "
	call	_udp_appcall
; udp_send:
L199:
; if
; RPN'ized expression: "uip_slen 0 == "
; Expanded expression: "uip_slen *(2) 0 == "
; Fused expression:    "== *uip_slen 0 IF! "
	mov	ax, [_uip_slen]
	cmp	ax, 0
	jne	L275
; {
; goto drop
	jmp	L194
; }
L275:
; RPN'ized expression: "uip_len uip_slen 28 + = "
; Expanded expression: "uip_len uip_slen *(2) 28 + =(2) "
; Fused expression:    "+ *uip_slen 28 =(170) *uip_len ax "
	mov	ax, [_uip_slen]
	add	ax, 28
	mov	[_uip_len], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something277) len -> *u 0 + *u uip_len 8 >> = "
; Expanded expression: "uip_buf 14 + 2 + uip_len *(2) 8 >>u =(1) "
; Fused expression:    "+ uip_buf 14 + ax 2 push-ax >>u *uip_len 8 =(154) **sp ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 2
	push	ax
	mov	ax, [_uip_len]
	shr	ax, 8
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something278) len -> *u 1 + *u uip_len 255 & = "
; Expanded expression: "uip_buf 14 + 3 + uip_len *(2) 255 & =(1) "
; Fused expression:    "+ uip_buf 14 + ax 3 push-ax & *uip_len 255 =(154) **sp ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 3
	push	ax
	mov	ax, [_uip_len]
	and	ax, 255
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something279) proto -> *u 17 = "
; Expanded expression: "uip_buf 14 + 9 + 17 =(1) "
; Fused expression:    "+ uip_buf 14 + ax 9 =(154) *ax 17 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 9
	mov	bx, ax
	mov	ax, 17
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; loc     <something> : unsigned
; RPN'ized expression: "uip_buf 14 + *u &u (something280) udplen -> *u uip_slen 8 + 255 & (something281) 8 << uip_slen 8 + 65280u & 8 >> | = "
; Expanded expression: "uip_buf 14 + 24 + uip_slen *(2) 8 + 255 & 8 << uip_slen *(2) 8 + 65280u & 8 >>u | =(2) "
; Fused expression:    "+ uip_buf 14 + ax 24 push-ax + *uip_slen 8 & ax 255 << ax 8 push-ax + *uip_slen 8 & ax 65280u >>u ax 8 | *sp ax =(170) **sp ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 24
	push	ax
	mov	ax, [_uip_slen]
	add	ax, 8
	and	ax, 255
	shl	ax, 8
	push	ax
	mov	ax, [_uip_slen]
	add	ax, 8
	and	ax, -256
	shr	ax, 8
	mov	cx, ax
	pop	ax
	or	ax, cx
	pop	bx
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something282) udpchksum -> *u 0 = "
; Expanded expression: "uip_buf 14 + 26 + 0 =(2) "
; Fused expression:    "+ uip_buf 14 + ax 26 =(170) *ax 0 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 26
	mov	bx, ax
	mov	ax, 0
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something283) srcport -> *u uip_udp_conn lport -> *u = "
; Expanded expression: "uip_buf 14 + 20 + uip_udp_conn *(2) 4 + *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 20 push-ax + *uip_udp_conn 4 =(170) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 20
	push	ax
	mov	ax, [_uip_udp_conn]
	add	ax, 4
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something284) destport -> *u uip_udp_conn rport -> *u = "
; Expanded expression: "uip_buf 14 + 22 + uip_udp_conn *(2) 6 + *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 22 push-ax + *uip_udp_conn 6 =(170) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 22
	push	ax
	mov	ax, [_uip_udp_conn]
	add	ax, 6
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something285) srcipaddr -> *u 0 + *u uip_hostaddr 0 + *u = "
; Expanded expression: "uip_buf 14 + 12 + uip_hostaddr 0 + *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 12 push-ax + uip_hostaddr 0 =(170) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 12
	push	ax
	mov	ax, _uip_hostaddr
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something286) srcipaddr -> *u 1 + *u uip_hostaddr 1 + *u = "
; Expanded expression: "uip_buf 14 + 14 + uip_hostaddr 2 + *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 14 push-ax + uip_hostaddr 2 =(170) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 14
	push	ax
	mov	ax, _uip_hostaddr
	add	ax, 2
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something287) destipaddr -> *u 0 + *u uip_udp_conn ripaddr -> *u 0 + *u = "
; Expanded expression: "uip_buf 14 + 16 + uip_udp_conn *(2) 0 + *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 16 push-ax + *uip_udp_conn 0 =(170) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 16
	push	ax
	mov	ax, [_uip_udp_conn]
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something288) destipaddr -> *u 1 + *u uip_udp_conn ripaddr -> *u 1 + *u = "
; Expanded expression: "uip_buf 14 + 18 + uip_udp_conn *(2) 2 + *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 18 push-ax + *uip_udp_conn 2 =(170) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 18
	push	ax
	mov	ax, [_uip_udp_conn]
	add	ax, 2
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; RPN'ized expression: "uip_appdata uip_buf 14 40 + + *u &u = "
; Expanded expression: "uip_appdata uip_buf 54 + =(2) "
; Fused expression:    "+ uip_buf 54 =(170) *uip_appdata ax "
	mov	ax, _uip_buf
	add	ax, 54
	mov	[_uip_appdata], ax
; goto ip_send_nolen
	jmp	L289
; tcp_input:
L229:
; RPN'ized expression: "uip_stat &u tcp -> *u &u recv -> *u ++ "
; Expanded expression: "uip_stat 26 + 2 + ++(2) "
; Fused expression:    "+ uip_stat 26 + ax 2 ++(2) *ax "
	mov	ax, _uip_stat
	add	ax, 26
	add	ax, 2
	mov	bx, ax
	inc	word [bx]
	mov	ax, [bx]
; if
; RPN'ized expression: "( uip_tcpchksum ) 65535u != "
; Expanded expression: " uip_tcpchksum ()0 65535u != "
; Fused expression:    "( uip_tcpchksum )0 != ax 65535u IF! "
	call	_uip_tcpchksum
	cmp	ax, -1
	je	L290
; {
; RPN'ized expression: "uip_stat &u tcp -> *u &u drop -> *u ++ "
; Expanded expression: "uip_stat 26 + 0 + ++(2) "
; Fused expression:    "+ uip_stat 26 + ax 0 ++(2) *ax "
	mov	ax, _uip_stat
	add	ax, 26
	mov	bx, ax
	inc	word [bx]
	mov	ax, [bx]
; RPN'ized expression: "uip_stat &u tcp -> *u &u chkerr -> *u ++ "
; Expanded expression: "uip_stat 26 + 6 + ++(2) "
; Fused expression:    "+ uip_stat 26 + ax 6 ++(2) *ax "
	mov	ax, _uip_stat
	add	ax, 26
	add	ax, 6
	mov	bx, ax
	inc	word [bx]
	mov	ax, [bx]
; goto drop
	jmp	L194
; }
L290:
; for
; RPN'ized expression: "uip_connr uip_conns 0 + *u &u = "
; Expanded expression: "(@-2) uip_conns 0 + =(2) "
; Fused expression:    "+ uip_conns 0 =(170) *(@-2) ax "
	mov	ax, _uip_conns
	mov	[bp-2], ax
L292:
; RPN'ized expression: "uip_connr uip_conns 128 + *u &u < "
; Expanded expression: "(@-2) *(2) uip_conns 19968 + <u "
; Fused expression:    "+ uip_conns 19968 <u *(@-2) ax IF! "
	mov	ax, _uip_conns
	add	ax, 19968
	mov	cx, ax
	mov	ax, [bp-2]
	cmp	ax, cx
	jae	L295
; RPN'ized expression: "uip_connr ++ "
; Expanded expression: "(@-2) 156 +=(2) "
; {
; if
; loc         <something> : * struct <something>
; loc         <something> : * struct <something>
; loc         <something> : * struct <something>
; loc         <something> : * struct <something>
; RPN'ized expression: "uip_connr tcpstateflags -> *u 0 != uip_buf 14 + *u &u (something298) destport -> *u uip_connr lport -> *u == && uip_buf 14 + *u &u (something299) srcport -> *u uip_connr rport -> *u == && uip_buf 14 + *u &u (something300) srcipaddr -> *u 0 + *u uip_connr ripaddr -> *u 0 + *u == && uip_buf 14 + *u &u (something301) srcipaddr -> *u 1 + *u uip_connr ripaddr -> *u 1 + *u == && "
; Expanded expression: "(@-2) *(2) 25 + *(1) 0 != [sh&&->305] uip_buf 14 + 22 + *(2) (@-2) *(2) 4 + *(2) == &&[305] _Bool [sh&&->304] uip_buf 14 + 20 + *(2) (@-2) *(2) 6 + *(2) == &&[304] _Bool [sh&&->303] uip_buf 14 + 12 + *(2) (@-2) *(2) 0 + *(2) == &&[303] _Bool [sh&&->302] uip_buf 14 + 14 + *(2) (@-2) *(2) 2 + *(2) == &&[302] "
; Fused expression:    "+ *(@-2) 25 != *ax 0 [sh&&->305] + uip_buf 14 + ax 22 push-ax + *(@-2) 4 == **sp *ax &&[305] _Bool [sh&&->304] + uip_buf 14 + ax 20 push-ax + *(@-2) 6 == **sp *ax &&[304] _Bool [sh&&->303] + uip_buf 14 + ax 12 push-ax + *(@-2) 0 == **sp *ax &&[303] _Bool [sh&&->302] + uip_buf 14 + ax 14 push-ax + *(@-2) 2 == **sp *ax &&[302]  "
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 0
	setne	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L305
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 22
	push	ax
	mov	ax, [bp-2]
	add	ax, 4
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	cmp	ax, cx
	sete	al
	cbw
L305:
	test	ax, ax
	setne	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L304
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 20
	push	ax
	mov	ax, [bp-2]
	add	ax, 6
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	cmp	ax, cx
	sete	al
	cbw
L304:
	test	ax, ax
	setne	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L303
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 12
	push	ax
	mov	ax, [bp-2]
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	cmp	ax, cx
	sete	al
	cbw
L303:
	test	ax, ax
	setne	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L302
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 14
	push	ax
	mov	ax, [bp-2]
	add	ax, 2
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	cmp	ax, cx
	sete	al
	cbw
L302:
; JumpIfZero
	test	ax, ax
	je	L296
; {
; goto found
	jmp	L306
; }
L296:
; }
L293:
; Fused expression:    "+=(170) *(@-2) 156 "
	mov	ax, [bp-2]
	add	ax, 156
	mov	[bp-2], ax
	jmp	L292
L295:
; if
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something309) flags -> *u 63 & 2 != "
; Expanded expression: "uip_buf 14 + 33 + *(1) 63 & 2 != "
; Fused expression:    "+ uip_buf 14 + ax 33 & *ax 63 != ax 2 IF! "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 33
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	and	ax, 63
	cmp	ax, 2
	je	L307
; goto reset
	jmp	L310
L307:
; loc     <something> : * struct <something>
; RPN'ized expression: "tmp16 uip_buf 14 + *u &u (something311) destport -> *u = "
; Expanded expression: "tmp16 uip_buf 14 + 22 + *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 22 =(170) *tmp16 *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 22
	mov	bx, ax
	mov	ax, [bx]
	mov	[_tmp16], ax
; for
; RPN'ized expression: "c 0 = "
; Expanded expression: "c 0 =(1) "
; Fused expression:    "=(154) *c 0 "
	mov	ax, 0
	mov	[_c], al
	mov	ah, 0
L312:
; RPN'ized expression: "c 10 < "
; Expanded expression: "c *(1) 10 < "
; Fused expression:    "< *c 10 IF! "
	mov	al, [_c]
	mov	ah, 0
	cmp	ax, 10
	jge	L315
; RPN'ized expression: "c ++ "
; Expanded expression: "c ++(1) "
; {
; if
; RPN'ized expression: "tmp16 uip_listenports c + *u == "
; Expanded expression: "tmp16 *(2) uip_listenports c *(1) 2 * + *(2) == "
; Fused expression:    "* *c 2 + uip_listenports ax == *tmp16 *ax IF! "
	mov	al, [_c]
	mov	ah, 0
	imul	ax, ax, 2
	mov	cx, ax
	mov	ax, _uip_listenports
	add	ax, cx
	mov	bx, ax
	mov	cx, [bx]
	mov	ax, [_tmp16]
	cmp	ax, cx
	jne	L316
; goto found_listen
	jmp	L318
L316:
; }
L313:
; Fused expression:    "++(1) *c "
	inc	byte [_c]
	mov	al, [_c]
	mov	ah, 0
	jmp	L312
L315:
; RPN'ized expression: "uip_stat &u tcp -> *u &u synrst -> *u ++ "
; Expanded expression: "uip_stat 26 + 16 + ++(2) "
; Fused expression:    "+ uip_stat 26 + ax 16 ++(2) *ax "
	mov	ax, _uip_stat
	add	ax, 26
	add	ax, 16
	mov	bx, ax
	inc	word [bx]
	mov	ax, [bx]
; reset:
L310:
; if
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something321) flags -> *u 4 & "
; Expanded expression: "uip_buf 14 + 33 + *(1) 4 & "
; Fused expression:    "+ uip_buf 14 + ax 33 & *ax 4  "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 33
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	and	ax, 4
; JumpIfZero
	test	ax, ax
	je	L319
; goto drop
	jmp	L194
L319:
; RPN'ized expression: "uip_stat &u tcp -> *u &u rst -> *u ++ "
; Expanded expression: "uip_stat 26 + 10 + ++(2) "
; Fused expression:    "+ uip_stat 26 + ax 10 ++(2) *ax "
	mov	ax, _uip_stat
	add	ax, 26
	add	ax, 10
	mov	bx, ax
	inc	word [bx]
	mov	ax, [bx]
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something322) flags -> *u 4 16 | = "
; Expanded expression: "uip_buf 14 + 33 + 20 =(1) "
; Fused expression:    "+ uip_buf 14 + ax 33 =(154) *ax 20 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 33
	mov	bx, ax
	mov	ax, 20
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_len 40 = "
; Expanded expression: "uip_len 40 =(2) "
; Fused expression:    "=(170) *uip_len 40 "
	mov	ax, 40
	mov	[_uip_len], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something323) tcpoffset -> *u 5 4 << = "
; Expanded expression: "uip_buf 14 + 32 + 80 =(1) "
; Fused expression:    "+ uip_buf 14 + ax 32 =(154) *ax 80 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 32
	mov	bx, ax
	mov	ax, 80
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "c uip_buf 14 + *u &u (something324) seqno -> *u 3 + *u = "
; Expanded expression: "c uip_buf 14 + 27 + *(1) =(1) "
; Fused expression:    "+ uip_buf 14 + ax 27 =(153) *c *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 27
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	mov	[_c], al
	mov	ah, 0
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something325) seqno -> *u 3 + *u uip_buf 14 + *u &u (something326) ackno -> *u 3 + *u = "
; Expanded expression: "uip_buf 14 + 27 + uip_buf 14 + 31 + *(1) =(1) "
; Fused expression:    "+ uip_buf 14 + ax 27 push-ax + uip_buf 14 + ax 31 =(153) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 27
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 31
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something327) ackno -> *u 3 + *u c = "
; Expanded expression: "uip_buf 14 + 31 + c *(1) =(1) "
; Fused expression:    "+ uip_buf 14 + ax 31 =(153) *ax *c "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 31
	mov	bx, ax
	mov	al, [_c]
	mov	ah, 0
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "c uip_buf 14 + *u &u (something328) seqno -> *u 2 + *u = "
; Expanded expression: "c uip_buf 14 + 26 + *(1) =(1) "
; Fused expression:    "+ uip_buf 14 + ax 26 =(153) *c *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 26
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	mov	[_c], al
	mov	ah, 0
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something329) seqno -> *u 2 + *u uip_buf 14 + *u &u (something330) ackno -> *u 2 + *u = "
; Expanded expression: "uip_buf 14 + 26 + uip_buf 14 + 30 + *(1) =(1) "
; Fused expression:    "+ uip_buf 14 + ax 26 push-ax + uip_buf 14 + ax 30 =(153) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 26
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 30
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something331) ackno -> *u 2 + *u c = "
; Expanded expression: "uip_buf 14 + 30 + c *(1) =(1) "
; Fused expression:    "+ uip_buf 14 + ax 30 =(153) *ax *c "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 30
	mov	bx, ax
	mov	al, [_c]
	mov	ah, 0
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "c uip_buf 14 + *u &u (something332) seqno -> *u 1 + *u = "
; Expanded expression: "c uip_buf 14 + 25 + *(1) =(1) "
; Fused expression:    "+ uip_buf 14 + ax 25 =(153) *c *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 25
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	mov	[_c], al
	mov	ah, 0
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something333) seqno -> *u 1 + *u uip_buf 14 + *u &u (something334) ackno -> *u 1 + *u = "
; Expanded expression: "uip_buf 14 + 25 + uip_buf 14 + 29 + *(1) =(1) "
; Fused expression:    "+ uip_buf 14 + ax 25 push-ax + uip_buf 14 + ax 29 =(153) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 25
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 29
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something335) ackno -> *u 1 + *u c = "
; Expanded expression: "uip_buf 14 + 29 + c *(1) =(1) "
; Fused expression:    "+ uip_buf 14 + ax 29 =(153) *ax *c "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 29
	mov	bx, ax
	mov	al, [_c]
	mov	ah, 0
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "c uip_buf 14 + *u &u (something336) seqno -> *u 0 + *u = "
; Expanded expression: "c uip_buf 14 + 24 + *(1) =(1) "
; Fused expression:    "+ uip_buf 14 + ax 24 =(153) *c *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 24
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	mov	[_c], al
	mov	ah, 0
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something337) seqno -> *u 0 + *u uip_buf 14 + *u &u (something338) ackno -> *u 0 + *u = "
; Expanded expression: "uip_buf 14 + 24 + uip_buf 14 + 28 + *(1) =(1) "
; Fused expression:    "+ uip_buf 14 + ax 24 push-ax + uip_buf 14 + ax 28 =(153) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 24
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 28
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something339) ackno -> *u 0 + *u c = "
; Expanded expression: "uip_buf 14 + 28 + c *(1) =(1) "
; Fused expression:    "+ uip_buf 14 + ax 28 =(153) *ax *c "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 28
	mov	bx, ax
	mov	al, [_c]
	mov	ah, 0
	mov	[bx], al
	mov	ah, 0
; if
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something342) ackno -> *u 3 + *u ++ 0 == "
; Expanded expression: "uip_buf 14 + 31 + ++(1) 0 == "
; Fused expression:    "+ uip_buf 14 + ax 31 ++(1) *ax == ax 0 IF! "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 31
	mov	bx, ax
	inc	byte [bx]
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 0
	jne	L340
; {
; if
; loc         <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something345) ackno -> *u 2 + *u ++ 0 == "
; Expanded expression: "uip_buf 14 + 30 + ++(1) 0 == "
; Fused expression:    "+ uip_buf 14 + ax 30 ++(1) *ax == ax 0 IF! "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 30
	mov	bx, ax
	inc	byte [bx]
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 0
	jne	L343
; {
; if
; loc             <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something348) ackno -> *u 1 + *u ++ 0 == "
; Expanded expression: "uip_buf 14 + 29 + ++(1) 0 == "
; Fused expression:    "+ uip_buf 14 + ax 29 ++(1) *ax == ax 0 IF! "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 29
	mov	bx, ax
	inc	byte [bx]
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 0
	jne	L346
; {
; loc                 <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something349) ackno -> *u 0 + *u ++ "
; Expanded expression: "uip_buf 14 + 28 + ++(1) "
; Fused expression:    "+ uip_buf 14 + ax 28 ++(1) *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 28
	mov	bx, ax
	inc	byte [bx]
	mov	al, [bx]
	mov	ah, 0
; }
L346:
; }
L343:
; }
L340:
; loc     <something> : * struct <something>
; RPN'ized expression: "tmp16 uip_buf 14 + *u &u (something350) srcport -> *u = "
; Expanded expression: "tmp16 uip_buf 14 + 20 + *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 20 =(170) *tmp16 *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 20
	mov	bx, ax
	mov	ax, [bx]
	mov	[_tmp16], ax
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something351) srcport -> *u uip_buf 14 + *u &u (something352) destport -> *u = "
; Expanded expression: "uip_buf 14 + 20 + uip_buf 14 + 22 + *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 20 push-ax + uip_buf 14 + ax 22 =(170) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 20
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 22
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something353) destport -> *u tmp16 = "
; Expanded expression: "uip_buf 14 + 22 + tmp16 *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 22 =(170) *ax *tmp16 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 22
	mov	bx, ax
	mov	ax, [_tmp16]
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "tmp16 uip_buf 14 + *u &u (something354) destipaddr -> *u 0 + *u = "
; Expanded expression: "tmp16 uip_buf 14 + 16 + *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 16 =(170) *tmp16 *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 16
	mov	bx, ax
	mov	ax, [bx]
	mov	[_tmp16], ax
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something355) destipaddr -> *u 0 + *u uip_buf 14 + *u &u (something356) srcipaddr -> *u 0 + *u = "
; Expanded expression: "uip_buf 14 + 16 + uip_buf 14 + 12 + *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 16 push-ax + uip_buf 14 + ax 12 =(170) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 16
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 12
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something357) srcipaddr -> *u 0 + *u tmp16 = "
; Expanded expression: "uip_buf 14 + 12 + tmp16 *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 12 =(170) *ax *tmp16 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 12
	mov	bx, ax
	mov	ax, [_tmp16]
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "tmp16 uip_buf 14 + *u &u (something358) destipaddr -> *u 1 + *u = "
; Expanded expression: "tmp16 uip_buf 14 + 18 + *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 18 =(170) *tmp16 *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 18
	mov	bx, ax
	mov	ax, [bx]
	mov	[_tmp16], ax
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something359) destipaddr -> *u 1 + *u uip_buf 14 + *u &u (something360) srcipaddr -> *u 1 + *u = "
; Expanded expression: "uip_buf 14 + 18 + uip_buf 14 + 14 + *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 18 push-ax + uip_buf 14 + ax 14 =(170) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 18
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 14
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something361) srcipaddr -> *u 1 + *u tmp16 = "
; Expanded expression: "uip_buf 14 + 14 + tmp16 *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 14 =(170) *ax *tmp16 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 14
	mov	bx, ax
	mov	ax, [_tmp16]
	mov	[bx], ax
; goto tcp_send_noconn
	jmp	L362
; found_listen:
L318:
; RPN'ized expression: "uip_connr 0 = "
; Expanded expression: "(@-2) 0 =(2) "
; Fused expression:    "=(170) *(@-2) 0 "
	mov	ax, 0
	mov	[bp-2], ax
; for
; RPN'ized expression: "c 0 = "
; Expanded expression: "c 0 =(1) "
; Fused expression:    "=(154) *c 0 "
	mov	ax, 0
	mov	[_c], al
	mov	ah, 0
L363:
; RPN'ized expression: "c 128 < "
; Expanded expression: "c *(1) 128 < "
; Fused expression:    "< *c 128 IF! "
	mov	al, [_c]
	mov	ah, 0
	cmp	ax, 128
	jge	L366
; RPN'ized expression: "c ++ "
; Expanded expression: "c ++(1) "
; {
; if
; RPN'ized expression: "uip_conns c + *u &u tcpstateflags -> *u 0 == "
; Expanded expression: "uip_conns c *(1) 156 * + 25 + *(1) 0 == "
; Fused expression:    "* *c 156 + uip_conns ax + ax 25 == *ax 0 IF! "
	mov	al, [_c]
	mov	ah, 0
	imul	ax, ax, 156
	mov	cx, ax
	mov	ax, _uip_conns
	add	ax, cx
	add	ax, 25
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 0
	jne	L367
; {
; RPN'ized expression: "uip_connr uip_conns c + *u &u = "
; Expanded expression: "(@-2) uip_conns c *(1) 156 * + =(2) "
; Fused expression:    "* *c 156 + uip_conns ax =(170) *(@-2) ax "
	mov	al, [_c]
	mov	ah, 0
	imul	ax, ax, 156
	mov	cx, ax
	mov	ax, _uip_conns
	add	ax, cx
	mov	[bp-2], ax
; break
	jmp	L366
; }
L367:
; if
; RPN'ized expression: "uip_conns c + *u &u tcpstateflags -> *u 7 == "
; Expanded expression: "uip_conns c *(1) 156 * + 25 + *(1) 7 == "
; Fused expression:    "* *c 156 + uip_conns ax + ax 25 == *ax 7 IF! "
	mov	al, [_c]
	mov	ah, 0
	imul	ax, ax, 156
	mov	cx, ax
	mov	ax, _uip_conns
	add	ax, cx
	add	ax, 25
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 7
	jne	L369
; {
; if
; RPN'ized expression: "uip_connr 0 == uip_conns c + *u &u timer -> *u uip_connr timer -> *u > || "
; Expanded expression: "(@-2) *(2) 0 == [sh||->373] uip_conns c *(1) 156 * + 26 + *(1) (@-2) *(2) 26 + *(1) > ||[373] "
; Fused expression:    "== *(@-2) 0 [sh||->373] * *c 156 + uip_conns ax + ax 26 push-ax + *(@-2) 26 > **sp *ax ||[373]  "
	mov	ax, [bp-2]
	cmp	ax, 0
	sete	al
	cbw
; JumpIfNotZero
	test	ax, ax
	jne	L373
	mov	al, [_c]
	mov	ah, 0
	imul	ax, ax, 156
	mov	cx, ax
	mov	ax, _uip_conns
	add	ax, cx
	add	ax, 26
	push	ax
	mov	ax, [bp-2]
	add	ax, 26
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	bx
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, cx
	setg	al
	cbw
L373:
; JumpIfZero
	test	ax, ax
	je	L371
; {
; RPN'ized expression: "uip_connr uip_conns c + *u &u = "
; Expanded expression: "(@-2) uip_conns c *(1) 156 * + =(2) "
; Fused expression:    "* *c 156 + uip_conns ax =(170) *(@-2) ax "
	mov	al, [_c]
	mov	ah, 0
	imul	ax, ax, 156
	mov	cx, ax
	mov	ax, _uip_conns
	add	ax, cx
	mov	[bp-2], ax
; }
L371:
; }
L369:
; }
L364:
; Fused expression:    "++(1) *c "
	inc	byte [_c]
	mov	al, [_c]
	mov	ah, 0
	jmp	L363
L366:
; if
; RPN'ized expression: "uip_connr 0 == "
; Expanded expression: "(@-2) *(2) 0 == "
; Fused expression:    "== *(@-2) 0 IF! "
	mov	ax, [bp-2]
	cmp	ax, 0
	jne	L374
; {
; RPN'ized expression: "uip_stat &u tcp -> *u &u syndrop -> *u ++ "
; Expanded expression: "uip_stat 26 + 14 + ++(2) "
; Fused expression:    "+ uip_stat 26 + ax 14 ++(2) *ax "
	mov	ax, _uip_stat
	add	ax, 26
	add	ax, 14
	mov	bx, ax
	inc	word [bx]
	mov	ax, [bx]
; goto drop
	jmp	L194
; }
L374:
; RPN'ized expression: "uip_conn uip_connr = "
; Expanded expression: "uip_conn (@-2) *(2) =(2) "
; Fused expression:    "=(170) *uip_conn *(@-2) "
	mov	ax, [bp-2]
	mov	[_uip_conn], ax
; RPN'ized expression: "uip_connr rto -> *u uip_connr timer -> *u 3 = = "
; Expanded expression: "(@-2) *(2) 24 + (@-2) *(2) 26 + 3 =(1) =(1) "
; Fused expression:    "+ *(@-2) 24 push-ax + *(@-2) 26 =(154) *ax 3 =(154) **sp ax "
	mov	ax, [bp-2]
	add	ax, 24
	push	ax
	mov	ax, [bp-2]
	add	ax, 26
	mov	bx, ax
	mov	ax, 3
	mov	[bx], al
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_connr sa -> *u 0 = "
; Expanded expression: "(@-2) *(2) 22 + 0 =(1) "
; Fused expression:    "+ *(@-2) 22 =(154) *ax 0 "
	mov	ax, [bp-2]
	add	ax, 22
	mov	bx, ax
	mov	ax, 0
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_connr sv -> *u 4 = "
; Expanded expression: "(@-2) *(2) 23 + 4 =(1) "
; Fused expression:    "+ *(@-2) 23 =(154) *ax 4 "
	mov	ax, [bp-2]
	add	ax, 23
	mov	bx, ax
	mov	ax, 4
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_connr nrtx -> *u 0 = "
; Expanded expression: "(@-2) *(2) 27 + 0 =(1) "
; Fused expression:    "+ *(@-2) 27 =(154) *ax 0 "
	mov	ax, [bp-2]
	add	ax, 27
	mov	bx, ax
	mov	ax, 0
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_connr lport -> *u uip_buf 14 + *u &u (something376) destport -> *u = "
; Expanded expression: "(@-2) *(2) 4 + uip_buf 14 + 22 + *(2) =(2) "
; Fused expression:    "+ *(@-2) 4 push-ax + uip_buf 14 + ax 22 =(170) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 4
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 22
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_connr rport -> *u uip_buf 14 + *u &u (something377) srcport -> *u = "
; Expanded expression: "(@-2) *(2) 6 + uip_buf 14 + 20 + *(2) =(2) "
; Fused expression:    "+ *(@-2) 6 push-ax + uip_buf 14 + ax 20 =(170) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 6
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 20
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_connr ripaddr -> *u 0 + *u uip_buf 14 + *u &u (something378) srcipaddr -> *u 0 + *u = "
; Expanded expression: "(@-2) *(2) 0 + uip_buf 14 + 12 + *(2) =(2) "
; Fused expression:    "+ *(@-2) 0 push-ax + uip_buf 14 + ax 12 =(170) **sp *ax "
	mov	ax, [bp-2]
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 12
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_connr ripaddr -> *u 1 + *u uip_buf 14 + *u &u (something379) srcipaddr -> *u 1 + *u = "
; Expanded expression: "(@-2) *(2) 2 + uip_buf 14 + 14 + *(2) =(2) "
; Fused expression:    "+ *(@-2) 2 push-ax + uip_buf 14 + ax 14 =(170) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 2
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 14
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; RPN'ized expression: "uip_connr tcpstateflags -> *u 1 = "
; Expanded expression: "(@-2) *(2) 25 + 1 =(1) "
; Fused expression:    "+ *(@-2) 25 =(154) *ax 1 "
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	ax, 1
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_connr snd_nxt -> *u 0 + *u iss 0 + *u = "
; Expanded expression: "(@-2) *(2) 12 + iss 0 + *(1) =(1) "
; Fused expression:    "+ *(@-2) 12 push-ax + iss 0 =(153) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 12
	push	ax
	mov	ax, _iss
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_connr snd_nxt -> *u 1 + *u iss 1 + *u = "
; Expanded expression: "(@-2) *(2) 13 + iss 1 + *(1) =(1) "
; Fused expression:    "+ *(@-2) 13 push-ax + iss 1 =(153) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 13
	push	ax
	mov	ax, _iss
	inc	ax
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_connr snd_nxt -> *u 2 + *u iss 2 + *u = "
; Expanded expression: "(@-2) *(2) 14 + iss 2 + *(1) =(1) "
; Fused expression:    "+ *(@-2) 14 push-ax + iss 2 =(153) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 14
	push	ax
	mov	ax, _iss
	add	ax, 2
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_connr snd_nxt -> *u 3 + *u iss 3 + *u = "
; Expanded expression: "(@-2) *(2) 15 + iss 3 + *(1) =(1) "
; Fused expression:    "+ *(@-2) 15 push-ax + iss 3 =(153) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 15
	push	ax
	mov	ax, _iss
	add	ax, 3
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_connr len -> *u 1 = "
; Expanded expression: "(@-2) *(2) 16 + 1 =(2) "
; Fused expression:    "+ *(@-2) 16 =(170) *ax 1 "
	mov	ax, [bp-2]
	add	ax, 16
	mov	bx, ax
	mov	ax, 1
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_connr rcv_nxt -> *u 3 + *u uip_buf 14 + *u &u (something380) seqno -> *u 3 + *u = "
; Expanded expression: "(@-2) *(2) 11 + uip_buf 14 + 27 + *(1) =(1) "
; Fused expression:    "+ *(@-2) 11 push-ax + uip_buf 14 + ax 27 =(153) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 11
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 27
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_connr rcv_nxt -> *u 2 + *u uip_buf 14 + *u &u (something381) seqno -> *u 2 + *u = "
; Expanded expression: "(@-2) *(2) 10 + uip_buf 14 + 26 + *(1) =(1) "
; Fused expression:    "+ *(@-2) 10 push-ax + uip_buf 14 + ax 26 =(153) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 10
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 26
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_connr rcv_nxt -> *u 1 + *u uip_buf 14 + *u &u (something382) seqno -> *u 1 + *u = "
; Expanded expression: "(@-2) *(2) 9 + uip_buf 14 + 25 + *(1) =(1) "
; Fused expression:    "+ *(@-2) 9 push-ax + uip_buf 14 + ax 25 =(153) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 9
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 25
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_connr rcv_nxt -> *u 0 + *u uip_buf 14 + *u &u (something383) seqno -> *u 0 + *u = "
; Expanded expression: "(@-2) *(2) 8 + uip_buf 14 + 24 + *(1) =(1) "
; Fused expression:    "+ *(@-2) 8 push-ax + uip_buf 14 + ax 24 =(153) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 8
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 24
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "( 1 uip_add_rcv_nxt ) "
; Expanded expression: " 1  uip_add_rcv_nxt ()2 "
; Fused expression:    "( 1 , uip_add_rcv_nxt )2 "
	push	1
	call	_uip_add_rcv_nxt
	sub	sp, -2
; if
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something386) tcpoffset -> *u 240 & 80 > "
; Expanded expression: "uip_buf 14 + 32 + *(1) 240 & 80 > "
; Fused expression:    "+ uip_buf 14 + ax 32 & *ax 240 > ax 80 IF! "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 32
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	and	ax, 240
	cmp	ax, 80
	jle	L384
; {
; for
; RPN'ized expression: "c 0 = "
; Expanded expression: "c 0 =(1) "
; Fused expression:    "=(154) *c 0 "
	mov	ax, 0
	mov	[_c], al
	mov	ah, 0
L387:
; loc         <something> : * struct <something>
; RPN'ized expression: "c uip_buf 14 + *u &u (something391) tcpoffset -> *u 4 >> 5 - 2 << < "
; Expanded expression: "c *(1) uip_buf 14 + 32 + *(1) 4 >> 5 - 2 << < "
; Fused expression:    "+ uip_buf 14 + ax 32 >> *ax 4 - ax 5 << ax 2 < *c ax IF! "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 32
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	sar	ax, 4
	sub	ax, 5
	shl	ax, 2
	mov	cx, ax
	mov	al, [_c]
	mov	ah, 0
	cmp	ax, cx
	jge	L390
; {
; RPN'ized expression: "opt uip_buf 40 14 + c + + *u = "
; Expanded expression: "opt uip_buf 54 c *(1) + + *(1) =(1) "
; Fused expression:    "+ 54 *c + uip_buf ax =(153) *opt *ax "
	mov	ax, 54
	movzx	cx, byte [_c]
	add	ax, cx
	mov	cx, ax
	mov	ax, _uip_buf
	add	ax, cx
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	mov	[_opt], al
	mov	ah, 0
; if
; RPN'ized expression: "opt 0 == "
; Expanded expression: "opt *(1) 0 == "
; Fused expression:    "== *opt 0 IF! "
	mov	al, [_opt]
	mov	ah, 0
	cmp	ax, 0
	jne	L392
; {
; break
	jmp	L390
; }
	jmp	L393
L392:
; else
; if
; RPN'ized expression: "opt 1 == "
; Expanded expression: "opt *(1) 1 == "
; Fused expression:    "== *opt 1 IF! "
	mov	al, [_opt]
	mov	ah, 0
	cmp	ax, 1
	jne	L394
; {
; RPN'ized expression: "c ++ "
; Expanded expression: "c ++(1) "
; Fused expression:    "++(1) *c "
	inc	byte [_c]
	mov	al, [_c]
	mov	ah, 0
; }
	jmp	L395
L394:
; else
; if
; RPN'ized expression: "opt 2 == uip_buf 40 14 + 1 + c + + *u 4 == && "
; Expanded expression: "opt *(1) 2 == [sh&&->398] uip_buf 55 c *(1) + + *(1) 4 == &&[398] "
; Fused expression:    "== *opt 2 [sh&&->398] + 55 *c + uip_buf ax == *ax 4 &&[398]  "
	mov	al, [_opt]
	mov	ah, 0
	cmp	ax, 2
	sete	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L398
	mov	ax, 55
	movzx	cx, byte [_c]
	add	ax, cx
	mov	cx, ax
	mov	ax, _uip_buf
	add	ax, cx
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 4
	sete	al
	cbw
L398:
; JumpIfZero
	test	ax, ax
	je	L396
; {
; loc                 <something> : unsigned
; loc                 <something> : unsigned
; RPN'ized expression: "tmp16 uip_buf 40 14 + 2 + c + + *u (something399) 8 << uip_buf 40 14 + 3 + c + + *u (something400) | = "
; Expanded expression: "tmp16 uip_buf 56 c *(1) + + *(1) 8 << uip_buf 57 c *(1) + + *(1) | =(2) "
; Fused expression:    "+ 56 *c + uip_buf ax << *ax 8 push-ax + 57 *c + uip_buf ax | *sp *ax =(170) *tmp16 ax "
	mov	ax, 56
	movzx	cx, byte [_c]
	add	ax, cx
	mov	cx, ax
	mov	ax, _uip_buf
	add	ax, cx
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	shl	ax, 8
	push	ax
	mov	ax, 57
	movzx	cx, byte [_c]
	add	ax, cx
	mov	cx, ax
	mov	ax, _uip_buf
	add	ax, cx
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	ax
	or	ax, cx
	mov	[_tmp16], ax
; RPN'ized expression: "uip_connr initialmss -> *u uip_connr mss -> *u tmp16 1500 14 - 40 - > tmp16 1500 14 - 40 - ? = = "
; Expanded expression: "(@-2) *(2) 20 + (@-2) *(2) 18 + tmp16 *(2) 1446 >u [sh||->401] tmp16 *(2) goto &&[401] 1446 &&[402] =(2) =(2) "
; Fused expression:    "+ *(@-2) 20 push-ax + *(@-2) 18 push-ax >u *tmp16 1446 [sh||->401] *(2) tmp16 [goto->402] &&[401] 1446 &&[402] =(170) **sp ax =(170) **sp ax "
	mov	ax, [bp-2]
	add	ax, 20
	push	ax
	mov	ax, [bp-2]
	add	ax, 18
	push	ax
	mov	ax, [_tmp16]
	cmp	ax, 1446
	seta	al
	cbw
; JumpIfNotZero
	test	ax, ax
	jne	L401
	mov	ax, [_tmp16]
	jmp	L402
L401:
	mov	ax, 1446
L402:
	pop	bx
	mov	[bx], ax
	pop	bx
	mov	[bx], ax
; break
	jmp	L390
; }
	jmp	L397
L396:
; else
; {
; if
; RPN'ized expression: "uip_buf 40 14 + 1 + c + + *u 0 == "
; Expanded expression: "uip_buf 55 c *(1) + + *(1) 0 == "
; Fused expression:    "+ 55 *c + uip_buf ax == *ax 0 IF! "
	mov	ax, 55
	movzx	cx, byte [_c]
	add	ax, cx
	mov	cx, ax
	mov	ax, _uip_buf
	add	ax, cx
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 0
	jne	L403
; {
; break
	jmp	L390
; }
L403:
; RPN'ized expression: "c uip_buf 40 14 + 1 + c + + *u += "
; Expanded expression: "c uip_buf 55 c *(1) + + *(1) +=(1) "
; Fused expression:    "+ 55 *c + uip_buf ax +=(153) *c *ax "
	mov	ax, 55
	movzx	cx, byte [_c]
	add	ax, cx
	mov	cx, ax
	mov	ax, _uip_buf
	add	ax, cx
	mov	bx, ax
	movzx	cx, byte [bx]
	mov	al, [_c]
	mov	ah, 0
	add	ax, cx
	mov	[_c], al
	mov	ah, 0
; }
L397:
L395:
L393:
; }
	jmp	L387
L390:
; }
L384:
; tcp_send_synack:
L181:
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something405) flags -> *u 16 = "
; Expanded expression: "uip_buf 14 + 33 + 16 =(1) "
; Fused expression:    "+ uip_buf 14 + ax 33 =(154) *ax 16 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 33
	mov	bx, ax
	mov	ax, 16
	mov	[bx], al
	mov	ah, 0
; tcp_send_syn:
L184:
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something406) flags -> *u 2 |= "
; Expanded expression: "uip_buf 14 + 33 + 2 |=(1) "
; Fused expression:    "+ uip_buf 14 + ax 33 |=(154) *ax 2 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 33
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	or	ax, 2
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something407) optdata -> *u 0 + *u 2 = "
; Expanded expression: "uip_buf 14 + 40 + 2 =(1) "
; Fused expression:    "+ uip_buf 14 + ax 40 =(154) *ax 2 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 40
	mov	bx, ax
	mov	ax, 2
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something408) optdata -> *u 1 + *u 4 = "
; Expanded expression: "uip_buf 14 + 41 + 4 =(1) "
; Fused expression:    "+ uip_buf 14 + ax 41 =(154) *ax 4 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 41
	mov	bx, ax
	mov	ax, 4
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something409) optdata -> *u 2 + *u 1500 14 - 40 - 256 / = "
; Expanded expression: "uip_buf 14 + 42 + 5 =(1) "
; Fused expression:    "+ uip_buf 14 + ax 42 =(154) *ax 5 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 42
	mov	bx, ax
	mov	ax, 5
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something410) optdata -> *u 3 + *u 1500 14 - 40 - 255 & = "
; Expanded expression: "uip_buf 14 + 43 + 166 =(1) "
; Fused expression:    "+ uip_buf 14 + ax 43 =(154) *ax 166 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 43
	mov	bx, ax
	mov	ax, 166
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_len 44 = "
; Expanded expression: "uip_len 44 =(2) "
; Fused expression:    "=(170) *uip_len 44 "
	mov	ax, 44
	mov	[_uip_len], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something411) tcpoffset -> *u 6 4 << = "
; Expanded expression: "uip_buf 14 + 32 + 96 =(1) "
; Fused expression:    "+ uip_buf 14 + ax 32 =(154) *ax 96 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 32
	mov	bx, ax
	mov	ax, 96
	mov	[bx], al
	mov	ah, 0
; goto tcp_send
	jmp	L412
; found:
L306:
; RPN'ized expression: "uip_conn uip_connr = "
; Expanded expression: "uip_conn (@-2) *(2) =(2) "
; Fused expression:    "=(170) *uip_conn *(@-2) "
	mov	ax, [bp-2]
	mov	[_uip_conn], ax
; RPN'ized expression: "uip_flags 0 = "
; Expanded expression: "uip_flags 0 =(1) "
; Fused expression:    "=(154) *uip_flags 0 "
	mov	ax, 0
	mov	[_uip_flags], al
	mov	ah, 0
; if
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something415) flags -> *u 4 & "
; Expanded expression: "uip_buf 14 + 33 + *(1) 4 & "
; Fused expression:    "+ uip_buf 14 + ax 33 & *ax 4  "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 33
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	and	ax, 4
; JumpIfZero
	test	ax, ax
	je	L413
; {
; RPN'ized expression: "uip_connr tcpstateflags -> *u 0 = "
; Expanded expression: "(@-2) *(2) 25 + 0 =(1) "
; Fused expression:    "+ *(@-2) 25 =(154) *ax 0 "
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	ax, 0
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_flags 32 = "
; Expanded expression: "uip_flags 32 =(1) "
; Fused expression:    "=(154) *uip_flags 32 "
	mov	ax, 32
	mov	[_uip_flags], al
	mov	ah, 0
; RPN'ized expression: "( tcp_appcall ) "
; Expanded expression: " tcp_appcall ()0 "
; Fused expression:    "( tcp_appcall )0 "
	call	_tcp_appcall
; goto drop
	jmp	L194
; }
L413:
; loc     <something> : * struct <something>
; RPN'ized expression: "c uip_buf 14 + *u &u (something416) tcpoffset -> *u 4 >> 2 << = "
; Expanded expression: "c uip_buf 14 + 32 + *(1) 4 >> 2 << =(1) "
; Fused expression:    "+ uip_buf 14 + ax 32 >> *ax 4 << ax 2 =(154) *c ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 32
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	sar	ax, 4
	shl	ax, 2
	mov	[_c], al
	mov	ah, 0
; RPN'ized expression: "uip_len uip_len c - 20 - = "
; Expanded expression: "uip_len uip_len *(2) c *(1) - 20 - =(2) "
; Fused expression:    "- *uip_len *c - ax 20 =(170) *uip_len ax "
	mov	ax, [_uip_len]
	movzx	cx, byte [_c]
	sub	ax, cx
	sub	ax, 20
	mov	[_uip_len], ax
; if
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_len 0 > uip_buf 14 + *u &u (something419) seqno -> *u 0 + *u uip_connr rcv_nxt -> *u 0 + *u != uip_buf 14 + *u &u (something420) seqno -> *u 1 + *u uip_connr rcv_nxt -> *u 1 + *u != || uip_buf 14 + *u &u (something421) seqno -> *u 2 + *u uip_connr rcv_nxt -> *u 2 + *u != || uip_buf 14 + *u &u (something422) seqno -> *u 3 + *u uip_connr rcv_nxt -> *u 3 + *u != || && "
; Expanded expression: "uip_len *(2) 0 >u [sh&&->423] uip_buf 14 + 24 + *(1) (@-2) *(2) 8 + *(1) != [sh||->426] uip_buf 14 + 25 + *(1) (@-2) *(2) 9 + *(1) != ||[426] _Bool [sh||->425] uip_buf 14 + 26 + *(1) (@-2) *(2) 10 + *(1) != ||[425] _Bool [sh||->424] uip_buf 14 + 27 + *(1) (@-2) *(2) 11 + *(1) != ||[424] _Bool &&[423] "
; Fused expression:    ">u *uip_len 0 [sh&&->423] + uip_buf 14 + ax 24 push-ax + *(@-2) 8 != **sp *ax [sh||->426] + uip_buf 14 + ax 25 push-ax + *(@-2) 9 != **sp *ax ||[426] _Bool [sh||->425] + uip_buf 14 + ax 26 push-ax + *(@-2) 10 != **sp *ax ||[425] _Bool [sh||->424] + uip_buf 14 + ax 27 push-ax + *(@-2) 11 != **sp *ax ||[424] _Bool &&[423]  "
	mov	ax, [_uip_len]
	cmp	ax, 0
	seta	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L423
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 24
	push	ax
	mov	ax, [bp-2]
	add	ax, 8
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	bx
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, cx
	setne	al
	cbw
; JumpIfNotZero
	test	ax, ax
	jne	L426
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 25
	push	ax
	mov	ax, [bp-2]
	add	ax, 9
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	bx
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, cx
	setne	al
	cbw
L426:
	test	ax, ax
	setne	al
	cbw
; JumpIfNotZero
	test	ax, ax
	jne	L425
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 26
	push	ax
	mov	ax, [bp-2]
	add	ax, 10
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	bx
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, cx
	setne	al
	cbw
L425:
	test	ax, ax
	setne	al
	cbw
; JumpIfNotZero
	test	ax, ax
	jne	L424
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 27
	push	ax
	mov	ax, [bp-2]
	add	ax, 11
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	bx
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, cx
	setne	al
	cbw
L424:
	test	ax, ax
	setne	al
	cbw
L423:
; JumpIfZero
	test	ax, ax
	je	L417
; {
; goto tcp_send_ack
	jmp	L427
; }
L417:
; if
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something430) flags -> *u 16 & uip_connr len -> *u && "
; Expanded expression: "uip_buf 14 + 33 + *(1) 16 & _Bool [sh&&->431] (@-2) *(2) 16 + *(2) _Bool &&[431] "
; Fused expression:    "+ uip_buf 14 + ax 33 & *ax 16 _Bool [sh&&->431] + *(@-2) 16 *(2) ax _Bool &&[431]  "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 33
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	and	ax, 16
	test	ax, ax
	setne	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L431
	mov	ax, [bp-2]
	add	ax, 16
	mov	bx, ax
	mov	ax, [bx]
	test	ax, ax
	setne	al
	cbw
L431:
; JumpIfZero
	test	ax, ax
	je	L428
; {
; RPN'ized expression: "( uip_connr len -> *u , uip_connr snd_nxt -> *u uip_add32 ) "
; Expanded expression: " (@-2) *(2) 16 + *(2)  (@-2) *(2) 12 +  uip_add32 ()4 "
; Fused expression:    "( + *(@-2) 16 *(2) ax , + *(@-2) 12 , uip_add32 )4 "
	mov	ax, [bp-2]
	add	ax, 16
	mov	bx, ax
	push	word [bx]
	mov	ax, [bp-2]
	add	ax, 12
	push	ax
	call	_uip_add32
	sub	sp, -4
; if
; loc         <something> : * struct <something>
; loc         <something> : * struct <something>
; loc         <something> : * struct <something>
; loc         <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something434) ackno -> *u 0 + *u uip_acc32 0 + *u == uip_buf 14 + *u &u (something435) ackno -> *u 1 + *u uip_acc32 1 + *u == && uip_buf 14 + *u &u (something436) ackno -> *u 2 + *u uip_acc32 2 + *u == && uip_buf 14 + *u &u (something437) ackno -> *u 3 + *u uip_acc32 3 + *u == && "
; Expanded expression: "uip_buf 14 + 28 + *(1) uip_acc32 0 + *(1) == [sh&&->440] uip_buf 14 + 29 + *(1) uip_acc32 1 + *(1) == &&[440] _Bool [sh&&->439] uip_buf 14 + 30 + *(1) uip_acc32 2 + *(1) == &&[439] _Bool [sh&&->438] uip_buf 14 + 31 + *(1) uip_acc32 3 + *(1) == &&[438] "
; Fused expression:    "+ uip_buf 14 + ax 28 push-ax + uip_acc32 0 == **sp *ax [sh&&->440] + uip_buf 14 + ax 29 push-ax + uip_acc32 1 == **sp *ax &&[440] _Bool [sh&&->439] + uip_buf 14 + ax 30 push-ax + uip_acc32 2 == **sp *ax &&[439] _Bool [sh&&->438] + uip_buf 14 + ax 31 push-ax + uip_acc32 3 == **sp *ax &&[438]  "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 28
	push	ax
	mov	ax, _uip_acc32
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	bx
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, cx
	sete	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L440
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 29
	push	ax
	mov	ax, _uip_acc32
	inc	ax
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	bx
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, cx
	sete	al
	cbw
L440:
	test	ax, ax
	setne	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L439
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 30
	push	ax
	mov	ax, _uip_acc32
	add	ax, 2
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	bx
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, cx
	sete	al
	cbw
L439:
	test	ax, ax
	setne	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L438
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 31
	push	ax
	mov	ax, _uip_acc32
	add	ax, 3
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	bx
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, cx
	sete	al
	cbw
L438:
; JumpIfZero
	test	ax, ax
	je	L432
; {
; RPN'ized expression: "uip_connr snd_nxt -> *u 0 + *u uip_acc32 0 + *u = "
; Expanded expression: "(@-2) *(2) 12 + uip_acc32 0 + *(1) =(1) "
; Fused expression:    "+ *(@-2) 12 push-ax + uip_acc32 0 =(153) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 12
	push	ax
	mov	ax, _uip_acc32
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_connr snd_nxt -> *u 1 + *u uip_acc32 1 + *u = "
; Expanded expression: "(@-2) *(2) 13 + uip_acc32 1 + *(1) =(1) "
; Fused expression:    "+ *(@-2) 13 push-ax + uip_acc32 1 =(153) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 13
	push	ax
	mov	ax, _uip_acc32
	inc	ax
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_connr snd_nxt -> *u 2 + *u uip_acc32 2 + *u = "
; Expanded expression: "(@-2) *(2) 14 + uip_acc32 2 + *(1) =(1) "
; Fused expression:    "+ *(@-2) 14 push-ax + uip_acc32 2 =(153) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 14
	push	ax
	mov	ax, _uip_acc32
	add	ax, 2
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_connr snd_nxt -> *u 3 + *u uip_acc32 3 + *u = "
; Expanded expression: "(@-2) *(2) 15 + uip_acc32 3 + *(1) =(1) "
; Fused expression:    "+ *(@-2) 15 push-ax + uip_acc32 3 =(153) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 15
	push	ax
	mov	ax, _uip_acc32
	add	ax, 3
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; if
; RPN'ized expression: "uip_connr nrtx -> *u 0 == "
; Expanded expression: "(@-2) *(2) 27 + *(1) 0 == "
; Fused expression:    "+ *(@-2) 27 == *ax 0 IF! "
	mov	ax, [bp-2]
	add	ax, 27
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 0
	jne	L441
; {
; loc                 m : (@-4): signed char
; RPN'ized expression: "m uip_connr rto -> *u uip_connr timer -> *u - = "
; Expanded expression: "(@-4) (@-2) *(2) 24 + *(1) (@-2) *(2) 26 + *(1) - =(-1) "
; Fused expression:    "+ *(@-2) 24 push-ax + *(@-2) 26 - **sp *ax =(122) *(@-4) ax "
	mov	ax, [bp-2]
	add	ax, 24
	push	ax
	mov	ax, [bp-2]
	add	ax, 26
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	bx
	mov	al, [bx]
	mov	ah, 0
	sub	ax, cx
	mov	[bp-4], al
	cbw
; RPN'ized expression: "m m uip_connr sa -> *u 3 >> - = "
; Expanded expression: "(@-4) (@-4) *(-1) (@-2) *(2) 22 + *(1) 3 >> - =(-1) "
; Fused expression:    "+ *(@-2) 22 >> *ax 3 - *(@-4) ax =(122) *(@-4) ax "
	mov	ax, [bp-2]
	add	ax, 22
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	sar	ax, 3
	mov	cx, ax
	mov	al, [bp-4]
	cbw
	sub	ax, cx
	mov	[bp-4], al
	cbw
; RPN'ized expression: "uip_connr sa -> *u m += "
; Expanded expression: "(@-2) *(2) 22 + (@-4) *(-1) +=(1) "
; Fused expression:    "+ *(@-2) 22 +=(151) *ax *(@-4) "
	mov	ax, [bp-2]
	add	ax, 22
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	movsx	cx, byte [bp-4]
	add	ax, cx
	mov	[bx], al
	mov	ah, 0
; if
; RPN'ized expression: "m 0 < "
; Expanded expression: "(@-4) *(-1) 0 < "
; Fused expression:    "< *(@-4) 0 IF! "
	mov	al, [bp-4]
	cbw
	cmp	ax, 0
	jge	L443
; {
; RPN'ized expression: "m m -u = "
; Expanded expression: "(@-4) (@-4) *(-1) -u =(-1) "
; Fused expression:    "*(-1) (@-4) -u =(122) *(@-4) ax "
	mov	al, [bp-4]
	cbw
	neg	ax
	mov	[bp-4], al
	cbw
; }
L443:
; RPN'ized expression: "m m uip_connr sv -> *u 2 >> - = "
; Expanded expression: "(@-4) (@-4) *(-1) (@-2) *(2) 23 + *(1) 2 >> - =(-1) "
; Fused expression:    "+ *(@-2) 23 >> *ax 2 - *(@-4) ax =(122) *(@-4) ax "
	mov	ax, [bp-2]
	add	ax, 23
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	sar	ax, 2
	mov	cx, ax
	mov	al, [bp-4]
	cbw
	sub	ax, cx
	mov	[bp-4], al
	cbw
; RPN'ized expression: "uip_connr sv -> *u m += "
; Expanded expression: "(@-2) *(2) 23 + (@-4) *(-1) +=(1) "
; Fused expression:    "+ *(@-2) 23 +=(151) *ax *(@-4) "
	mov	ax, [bp-2]
	add	ax, 23
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	movsx	cx, byte [bp-4]
	add	ax, cx
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_connr rto -> *u uip_connr sa -> *u 3 >> uip_connr sv -> *u + = "
; Expanded expression: "(@-2) *(2) 24 + (@-2) *(2) 22 + *(1) 3 >> (@-2) *(2) 23 + *(1) + =(1) "
; Fused expression:    "+ *(@-2) 24 push-ax + *(@-2) 22 >> *ax 3 push-ax + *(@-2) 23 + *sp *ax =(154) **sp ax "
	mov	ax, [bp-2]
	add	ax, 24
	push	ax
	mov	ax, [bp-2]
	add	ax, 22
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	sar	ax, 3
	push	ax
	mov	ax, [bp-2]
	add	ax, 23
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	ax
	add	ax, cx
	pop	bx
	mov	[bx], al
	mov	ah, 0
; }
L441:
; RPN'ized expression: "uip_flags 1 = "
; Expanded expression: "uip_flags 1 =(1) "
; Fused expression:    "=(154) *uip_flags 1 "
	mov	ax, 1
	mov	[_uip_flags], al
	mov	ah, 0
; RPN'ized expression: "uip_connr timer -> *u uip_connr rto -> *u = "
; Expanded expression: "(@-2) *(2) 26 + (@-2) *(2) 24 + *(1) =(1) "
; Fused expression:    "+ *(@-2) 26 push-ax + *(@-2) 24 =(153) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 26
	push	ax
	mov	ax, [bp-2]
	add	ax, 24
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; }
L432:
; }
L428:
; switch
; RPN'ized expression: "uip_connr tcpstateflags -> *u 15 & "
; Expanded expression: "(@-2) *(2) 25 + *(1) 15 & "
; Fused expression:    "+ *(@-2) 25 & *ax 15  "
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	and	ax, 15
	jmp	L446
; {
; case
; RPN'ized expression: "1 "
; Expanded expression: "1 "
; Expression value: 1
L447:
; if
; RPN'ized expression: "uip_flags 1 & "
; Expanded expression: "uip_flags *(1) 1 & "
; Fused expression:    "& *uip_flags 1  "
	mov	al, [_uip_flags]
	mov	ah, 0
	and	ax, 1
; JumpIfZero
	test	ax, ax
	je	L448
; {
; RPN'ized expression: "uip_connr tcpstateflags -> *u 3 = "
; Expanded expression: "(@-2) *(2) 25 + 3 =(1) "
; Fused expression:    "+ *(@-2) 25 =(154) *ax 3 "
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	ax, 3
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_flags 64 = "
; Expanded expression: "uip_flags 64 =(1) "
; Fused expression:    "=(154) *uip_flags 64 "
	mov	ax, 64
	mov	[_uip_flags], al
	mov	ah, 0
; RPN'ized expression: "uip_connr len -> *u 0 = "
; Expanded expression: "(@-2) *(2) 16 + 0 =(2) "
; Fused expression:    "+ *(@-2) 16 =(170) *ax 0 "
	mov	ax, [bp-2]
	add	ax, 16
	mov	bx, ax
	mov	ax, 0
	mov	[bx], ax
; if
; RPN'ized expression: "uip_len 0 > "
; Expanded expression: "uip_len *(2) 0 >u "
; Fused expression:    ">u *uip_len 0 IF! "
	mov	ax, [_uip_len]
	cmp	ax, 0
	jbe	L450
; {
; RPN'ized expression: "uip_flags 2 |= "
; Expanded expression: "uip_flags 2 |=(1) "
; Fused expression:    "|=(154) *uip_flags 2 "
	mov	al, [_uip_flags]
	mov	ah, 0
	or	ax, 2
	mov	[_uip_flags], al
	mov	ah, 0
; RPN'ized expression: "( uip_len uip_add_rcv_nxt ) "
; Expanded expression: " uip_len *(2)  uip_add_rcv_nxt ()2 "
; Fused expression:    "( *(2) uip_len , uip_add_rcv_nxt )2 "
	push	word [_uip_len]
	call	_uip_add_rcv_nxt
	sub	sp, -2
; }
L450:
; RPN'ized expression: "uip_slen 0 = "
; Expanded expression: "uip_slen 0 =(2) "
; Fused expression:    "=(170) *uip_slen 0 "
	mov	ax, 0
	mov	[_uip_slen], ax
; RPN'ized expression: "( tcp_appcall ) "
; Expanded expression: " tcp_appcall ()0 "
; Fused expression:    "( tcp_appcall )0 "
	call	_tcp_appcall
; goto appsend
	jmp	L193
; }
L448:
; goto drop
	jmp	L194
; case
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
L452:
; if
; loc         <something> : * struct <something>
; RPN'ized expression: "uip_flags 1 & uip_buf 14 + *u &u (something455) flags -> *u 2 16 | == && "
; Expanded expression: "uip_flags *(1) 1 & _Bool [sh&&->456] uip_buf 14 + 33 + *(1) 18 == &&[456] "
; Fused expression:    "& *uip_flags 1 _Bool [sh&&->456] + uip_buf 14 + ax 33 == *ax 18 &&[456]  "
	mov	al, [_uip_flags]
	mov	ah, 0
	and	ax, 1
	test	ax, ax
	setne	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L456
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 33
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 18
	sete	al
	cbw
L456:
; JumpIfZero
	test	ax, ax
	je	L453
; {
; if
; loc             <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something459) tcpoffset -> *u 240 & 80 > "
; Expanded expression: "uip_buf 14 + 32 + *(1) 240 & 80 > "
; Fused expression:    "+ uip_buf 14 + ax 32 & *ax 240 > ax 80 IF! "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 32
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	and	ax, 240
	cmp	ax, 80
	jle	L457
; {
; for
; RPN'ized expression: "c 0 = "
; Expanded expression: "c 0 =(1) "
; Fused expression:    "=(154) *c 0 "
	mov	ax, 0
	mov	[_c], al
	mov	ah, 0
L460:
; loc                 <something> : * struct <something>
; RPN'ized expression: "c uip_buf 14 + *u &u (something464) tcpoffset -> *u 4 >> 5 - 2 << < "
; Expanded expression: "c *(1) uip_buf 14 + 32 + *(1) 4 >> 5 - 2 << < "
; Fused expression:    "+ uip_buf 14 + ax 32 >> *ax 4 - ax 5 << ax 2 < *c ax IF! "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 32
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	sar	ax, 4
	sub	ax, 5
	shl	ax, 2
	mov	cx, ax
	mov	al, [_c]
	mov	ah, 0
	cmp	ax, cx
	jge	L463
; {
; RPN'ized expression: "opt uip_buf 40 14 + c + + *u = "
; Expanded expression: "opt uip_buf 54 c *(1) + + *(1) =(1) "
; Fused expression:    "+ 54 *c + uip_buf ax =(153) *opt *ax "
	mov	ax, 54
	movzx	cx, byte [_c]
	add	ax, cx
	mov	cx, ax
	mov	ax, _uip_buf
	add	ax, cx
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	mov	[_opt], al
	mov	ah, 0
; if
; RPN'ized expression: "opt 0 == "
; Expanded expression: "opt *(1) 0 == "
; Fused expression:    "== *opt 0 IF! "
	mov	al, [_opt]
	mov	ah, 0
	cmp	ax, 0
	jne	L465
; {
; break
	jmp	L463
; }
	jmp	L466
L465:
; else
; if
; RPN'ized expression: "opt 1 == "
; Expanded expression: "opt *(1) 1 == "
; Fused expression:    "== *opt 1 IF! "
	mov	al, [_opt]
	mov	ah, 0
	cmp	ax, 1
	jne	L467
; {
; RPN'ized expression: "c ++ "
; Expanded expression: "c ++(1) "
; Fused expression:    "++(1) *c "
	inc	byte [_c]
	mov	al, [_c]
	mov	ah, 0
; }
	jmp	L468
L467:
; else
; if
; RPN'ized expression: "opt 2 == uip_buf 40 14 + 1 + c + + *u 4 == && "
; Expanded expression: "opt *(1) 2 == [sh&&->471] uip_buf 55 c *(1) + + *(1) 4 == &&[471] "
; Fused expression:    "== *opt 2 [sh&&->471] + 55 *c + uip_buf ax == *ax 4 &&[471]  "
	mov	al, [_opt]
	mov	ah, 0
	cmp	ax, 2
	sete	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L471
	mov	ax, 55
	movzx	cx, byte [_c]
	add	ax, cx
	mov	cx, ax
	mov	ax, _uip_buf
	add	ax, cx
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 4
	sete	al
	cbw
L471:
; JumpIfZero
	test	ax, ax
	je	L469
; {
; RPN'ized expression: "tmp16 uip_buf 40 14 + 2 + c + + *u 8 << uip_buf 40 14 + 3 + c + + *u | = "
; Expanded expression: "tmp16 uip_buf 56 c *(1) + + *(1) 8 << uip_buf 57 c *(1) + + *(1) | =(2) "
; Fused expression:    "+ 56 *c + uip_buf ax << *ax 8 push-ax + 57 *c + uip_buf ax | *sp *ax =(170) *tmp16 ax "
	mov	ax, 56
	movzx	cx, byte [_c]
	add	ax, cx
	mov	cx, ax
	mov	ax, _uip_buf
	add	ax, cx
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	shl	ax, 8
	push	ax
	mov	ax, 57
	movzx	cx, byte [_c]
	add	ax, cx
	mov	cx, ax
	mov	ax, _uip_buf
	add	ax, cx
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	ax
	or	ax, cx
	mov	[_tmp16], ax
; RPN'ized expression: "uip_connr initialmss -> *u uip_connr mss -> *u tmp16 1500 14 - 40 - > tmp16 1500 14 - 40 - ? = = "
; Expanded expression: "(@-2) *(2) 20 + (@-2) *(2) 18 + tmp16 *(2) 1446 >u [sh||->472] tmp16 *(2) goto &&[472] 1446 &&[473] =(2) =(2) "
; Fused expression:    "+ *(@-2) 20 push-ax + *(@-2) 18 push-ax >u *tmp16 1446 [sh||->472] *(2) tmp16 [goto->473] &&[472] 1446 &&[473] =(170) **sp ax =(170) **sp ax "
	mov	ax, [bp-2]
	add	ax, 20
	push	ax
	mov	ax, [bp-2]
	add	ax, 18
	push	ax
	mov	ax, [_tmp16]
	cmp	ax, 1446
	seta	al
	cbw
; JumpIfNotZero
	test	ax, ax
	jne	L472
	mov	ax, [_tmp16]
	jmp	L473
L472:
	mov	ax, 1446
L473:
	pop	bx
	mov	[bx], ax
	pop	bx
	mov	[bx], ax
; break
	jmp	L463
; }
	jmp	L470
L469:
; else
; {
; if
; RPN'ized expression: "uip_buf 40 14 + 1 + c + + *u 0 == "
; Expanded expression: "uip_buf 55 c *(1) + + *(1) 0 == "
; Fused expression:    "+ 55 *c + uip_buf ax == *ax 0 IF! "
	mov	ax, 55
	movzx	cx, byte [_c]
	add	ax, cx
	mov	cx, ax
	mov	ax, _uip_buf
	add	ax, cx
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 0
	jne	L474
; {
; break
	jmp	L463
; }
L474:
; RPN'ized expression: "c uip_buf 40 14 + 1 + c + + *u += "
; Expanded expression: "c uip_buf 55 c *(1) + + *(1) +=(1) "
; Fused expression:    "+ 55 *c + uip_buf ax +=(153) *c *ax "
	mov	ax, 55
	movzx	cx, byte [_c]
	add	ax, cx
	mov	cx, ax
	mov	ax, _uip_buf
	add	ax, cx
	mov	bx, ax
	movzx	cx, byte [bx]
	mov	al, [_c]
	mov	ah, 0
	add	ax, cx
	mov	[_c], al
	mov	ah, 0
; }
L470:
L468:
L466:
; }
	jmp	L460
L463:
; }
L457:
; RPN'ized expression: "uip_connr tcpstateflags -> *u 3 = "
; Expanded expression: "(@-2) *(2) 25 + 3 =(1) "
; Fused expression:    "+ *(@-2) 25 =(154) *ax 3 "
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	ax, 3
	mov	[bx], al
	mov	ah, 0
; loc             <something> : * struct <something>
; RPN'ized expression: "uip_connr rcv_nxt -> *u 0 + *u uip_buf 14 + *u &u (something476) seqno -> *u 0 + *u = "
; Expanded expression: "(@-2) *(2) 8 + uip_buf 14 + 24 + *(1) =(1) "
; Fused expression:    "+ *(@-2) 8 push-ax + uip_buf 14 + ax 24 =(153) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 8
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 24
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc             <something> : * struct <something>
; RPN'ized expression: "uip_connr rcv_nxt -> *u 1 + *u uip_buf 14 + *u &u (something477) seqno -> *u 1 + *u = "
; Expanded expression: "(@-2) *(2) 9 + uip_buf 14 + 25 + *(1) =(1) "
; Fused expression:    "+ *(@-2) 9 push-ax + uip_buf 14 + ax 25 =(153) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 9
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 25
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc             <something> : * struct <something>
; RPN'ized expression: "uip_connr rcv_nxt -> *u 2 + *u uip_buf 14 + *u &u (something478) seqno -> *u 2 + *u = "
; Expanded expression: "(@-2) *(2) 10 + uip_buf 14 + 26 + *(1) =(1) "
; Fused expression:    "+ *(@-2) 10 push-ax + uip_buf 14 + ax 26 =(153) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 10
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 26
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc             <something> : * struct <something>
; RPN'ized expression: "uip_connr rcv_nxt -> *u 3 + *u uip_buf 14 + *u &u (something479) seqno -> *u 3 + *u = "
; Expanded expression: "(@-2) *(2) 11 + uip_buf 14 + 27 + *(1) =(1) "
; Fused expression:    "+ *(@-2) 11 push-ax + uip_buf 14 + ax 27 =(153) **sp *ax "
	mov	ax, [bp-2]
	add	ax, 11
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 27
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "( 1 uip_add_rcv_nxt ) "
; Expanded expression: " 1  uip_add_rcv_nxt ()2 "
; Fused expression:    "( 1 , uip_add_rcv_nxt )2 "
	push	1
	call	_uip_add_rcv_nxt
	sub	sp, -2
; RPN'ized expression: "uip_flags 64 2 | = "
; Expanded expression: "uip_flags 66 =(1) "
; Fused expression:    "=(154) *uip_flags 66 "
	mov	ax, 66
	mov	[_uip_flags], al
	mov	ah, 0
; RPN'ized expression: "uip_connr len -> *u 0 = "
; Expanded expression: "(@-2) *(2) 16 + 0 =(2) "
; Fused expression:    "+ *(@-2) 16 =(170) *ax 0 "
	mov	ax, [bp-2]
	add	ax, 16
	mov	bx, ax
	mov	ax, 0
	mov	[bx], ax
; RPN'ized expression: "uip_len 0 = "
; Expanded expression: "uip_len 0 =(2) "
; Fused expression:    "=(170) *uip_len 0 "
	mov	ax, 0
	mov	[_uip_len], ax
; RPN'ized expression: "uip_slen 0 = "
; Expanded expression: "uip_slen 0 =(2) "
; Fused expression:    "=(170) *uip_slen 0 "
	mov	ax, 0
	mov	[_uip_slen], ax
; RPN'ized expression: "( tcp_appcall ) "
; Expanded expression: " tcp_appcall ()0 "
; Fused expression:    "( tcp_appcall )0 "
	call	_tcp_appcall
; goto appsend
	jmp	L193
; }
L453:
; goto reset
	jmp	L310
; case
; RPN'ized expression: "3 "
; Expanded expression: "3 "
; Expression value: 3
L480:
; if
; loc         <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something483) flags -> *u 1 & "
; Expanded expression: "uip_buf 14 + 33 + *(1) 1 & "
; Fused expression:    "+ uip_buf 14 + ax 33 & *ax 1  "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 33
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	and	ax, 1
; JumpIfZero
	test	ax, ax
	je	L481
; {
; if
; RPN'ized expression: "uip_connr len -> *u "
; Expanded expression: "(@-2) *(2) 16 + *(2) "
; Fused expression:    "+ *(@-2) 16 *(2) ax  "
	mov	ax, [bp-2]
	add	ax, 16
	mov	bx, ax
	mov	ax, [bx]
; JumpIfZero
	test	ax, ax
	je	L484
; {
; goto drop
	jmp	L194
; }
L484:
; RPN'ized expression: "( 1 uip_len + uip_add_rcv_nxt ) "
; Expanded expression: " 1 uip_len *(2) +  uip_add_rcv_nxt ()2 "
; Fused expression:    "( + 1 *uip_len , uip_add_rcv_nxt )2 "
	mov	ax, 1
	add	ax, [_uip_len]
	push	ax
	call	_uip_add_rcv_nxt
	sub	sp, -2
; RPN'ized expression: "uip_flags 16 = "
; Expanded expression: "uip_flags 16 =(1) "
; Fused expression:    "=(154) *uip_flags 16 "
	mov	ax, 16
	mov	[_uip_flags], al
	mov	ah, 0
; if
; RPN'ized expression: "uip_len 0 > "
; Expanded expression: "uip_len *(2) 0 >u "
; Fused expression:    ">u *uip_len 0 IF! "
	mov	ax, [_uip_len]
	cmp	ax, 0
	jbe	L486
; {
; RPN'ized expression: "uip_flags 2 |= "
; Expanded expression: "uip_flags 2 |=(1) "
; Fused expression:    "|=(154) *uip_flags 2 "
	mov	al, [_uip_flags]
	mov	ah, 0
	or	ax, 2
	mov	[_uip_flags], al
	mov	ah, 0
; }
L486:
; RPN'ized expression: "( tcp_appcall ) "
; Expanded expression: " tcp_appcall ()0 "
; Fused expression:    "( tcp_appcall )0 "
	call	_tcp_appcall
; RPN'ized expression: "uip_connr len -> *u 1 = "
; Expanded expression: "(@-2) *(2) 16 + 1 =(2) "
; Fused expression:    "+ *(@-2) 16 =(170) *ax 1 "
	mov	ax, [bp-2]
	add	ax, 16
	mov	bx, ax
	mov	ax, 1
	mov	[bx], ax
; RPN'ized expression: "uip_connr tcpstateflags -> *u 8 = "
; Expanded expression: "(@-2) *(2) 25 + 8 =(1) "
; Fused expression:    "+ *(@-2) 25 =(154) *ax 8 "
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	ax, 8
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_connr nrtx -> *u 0 = "
; Expanded expression: "(@-2) *(2) 27 + 0 =(1) "
; Fused expression:    "+ *(@-2) 27 =(154) *ax 0 "
	mov	ax, [bp-2]
	add	ax, 27
	mov	bx, ax
	mov	ax, 0
	mov	[bx], al
	mov	ah, 0
; tcp_send_finack:
L190:
; loc             <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something488) flags -> *u 1 16 | = "
; Expanded expression: "uip_buf 14 + 33 + 17 =(1) "
; Fused expression:    "+ uip_buf 14 + ax 33 =(154) *ax 17 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 33
	mov	bx, ax
	mov	ax, 17
	mov	[bx], al
	mov	ah, 0
; goto tcp_send_nodata
	jmp	L175
; }
L481:
; if
; loc         <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something491) flags -> *u 32 & "
; Expanded expression: "uip_buf 14 + 33 + *(1) 32 & "
; Fused expression:    "+ uip_buf 14 + ax 33 & *ax 32  "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 33
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	and	ax, 32
; JumpIfZero
	test	ax, ax
	je	L489
; {
; loc             <something> : * struct <something>
; loc             <something> : * struct <something>
; RPN'ized expression: "uip_urglen uip_buf 14 + *u &u (something492) urgp -> *u 0 + *u 8 << uip_buf 14 + *u &u (something493) urgp -> *u 1 + *u | = "
; Expanded expression: "uip_urglen uip_buf 14 + 38 + *(1) 8 << uip_buf 14 + 39 + *(1) | =(1) "
; Fused expression:    "+ uip_buf 14 + ax 38 << *ax 8 push-ax + uip_buf 14 + ax 39 | *sp *ax =(154) *uip_urglen ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 38
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	shl	ax, 8
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 39
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	ax
	or	ax, cx
	mov	[_uip_urglen], al
	mov	ah, 0
; if
; RPN'ized expression: "uip_urglen uip_len > "
; Expanded expression: "uip_urglen *(1) uip_len *(2) >u "
; Fused expression:    ">u *uip_urglen *uip_len IF! "
	mov	al, [_uip_urglen]
	mov	ah, 0
	cmp	ax, [_uip_len]
	jbe	L494
; {
; RPN'ized expression: "uip_urglen uip_len = "
; Expanded expression: "uip_urglen uip_len *(2) =(1) "
; Fused expression:    "=(154) *uip_urglen *uip_len "
	mov	ax, [_uip_len]
	mov	[_uip_urglen], al
	mov	ah, 0
; }
L494:
; RPN'ized expression: "( uip_urglen uip_add_rcv_nxt ) "
; Expanded expression: " uip_urglen *(1)  uip_add_rcv_nxt ()2 "
; Fused expression:    "( *(1) uip_urglen , uip_add_rcv_nxt )2 "
	mov	al, [_uip_urglen]
	mov	ah, 0
	push	ax
	call	_uip_add_rcv_nxt
	sub	sp, -2
; RPN'ized expression: "uip_len uip_urglen -= "
; Expanded expression: "uip_len uip_urglen *(1) -=(2) "
; Fused expression:    "-=(169) *uip_len *uip_urglen "
	mov	ax, [_uip_len]
	movzx	cx, byte [_uip_urglen]
	sub	ax, cx
	mov	[_uip_len], ax
; RPN'ized expression: "uip_urgdata uip_appdata = "
; Expanded expression: "uip_urgdata uip_appdata *(2) =(2) "
; Fused expression:    "=(170) *uip_urgdata *uip_appdata "
	mov	ax, [_uip_appdata]
	mov	[_uip_urgdata], ax
; RPN'ized expression: "uip_appdata uip_urglen += "
; Expanded expression: "uip_appdata uip_urglen *(1) +=(2) "
; Fused expression:    "+=(169) *uip_appdata *uip_urglen "
	mov	ax, [_uip_appdata]
	movzx	cx, byte [_uip_urglen]
	add	ax, cx
	mov	[_uip_appdata], ax
; }
	jmp	L490
L489:
; else
; {
; RPN'ized expression: "uip_urglen 0 = "
; Expanded expression: "uip_urglen 0 =(1) "
; Fused expression:    "=(154) *uip_urglen 0 "
	mov	ax, 0
	mov	[_uip_urglen], al
	mov	ah, 0
; loc             <something> : * struct <something>
; loc             <something> : * struct <something>
; RPN'ized expression: "uip_appdata uip_buf 14 + *u &u (something496) urgp -> *u 0 + *u 8 << uip_buf 14 + *u &u (something497) urgp -> *u 1 + *u | += "
; Expanded expression: "uip_appdata uip_buf 14 + 38 + *(1) 8 << uip_buf 14 + 39 + *(1) | +=(2) "
; Fused expression:    "+ uip_buf 14 + ax 38 << *ax 8 push-ax + uip_buf 14 + ax 39 | *sp *ax +=(170) *uip_appdata ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 38
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	shl	ax, 8
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 39
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	ax
	or	ax, cx
	mov	cx, ax
	mov	ax, [_uip_appdata]
	add	ax, cx
	mov	[_uip_appdata], ax
; loc             <something> : * struct <something>
; loc             <something> : * struct <something>
; RPN'ized expression: "uip_len uip_buf 14 + *u &u (something498) urgp -> *u 0 + *u 8 << uip_buf 14 + *u &u (something499) urgp -> *u 1 + *u | -= "
; Expanded expression: "uip_len uip_buf 14 + 38 + *(1) 8 << uip_buf 14 + 39 + *(1) | -=(2) "
; Fused expression:    "+ uip_buf 14 + ax 38 << *ax 8 push-ax + uip_buf 14 + ax 39 | *sp *ax -=(170) *uip_len ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 38
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	shl	ax, 8
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 39
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	ax
	or	ax, cx
	mov	cx, ax
	mov	ax, [_uip_len]
	sub	ax, cx
	mov	[_uip_len], ax
; }
L490:
; if
; RPN'ized expression: "uip_len 0 > uip_connr tcpstateflags -> *u 16 & 0 == && "
; Expanded expression: "uip_len *(2) 0 >u [sh&&->502] (@-2) *(2) 25 + *(1) 16 & 0 == &&[502] "
; Fused expression:    ">u *uip_len 0 [sh&&->502] + *(@-2) 25 & *ax 16 == ax 0 &&[502]  "
	mov	ax, [_uip_len]
	cmp	ax, 0
	seta	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L502
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	and	ax, 16
	cmp	ax, 0
	sete	al
	cbw
L502:
; JumpIfZero
	test	ax, ax
	je	L500
; {
; RPN'ized expression: "uip_flags 2 |= "
; Expanded expression: "uip_flags 2 |=(1) "
; Fused expression:    "|=(154) *uip_flags 2 "
	mov	al, [_uip_flags]
	mov	ah, 0
	or	ax, 2
	mov	[_uip_flags], al
	mov	ah, 0
; RPN'ized expression: "( uip_len uip_add_rcv_nxt ) "
; Expanded expression: " uip_len *(2)  uip_add_rcv_nxt ()2 "
; Fused expression:    "( *(2) uip_len , uip_add_rcv_nxt )2 "
	push	word [_uip_len]
	call	_uip_add_rcv_nxt
	sub	sp, -2
; }
L500:
; loc         <something> : unsigned
; loc         <something> : * struct <something>
; loc         <something> : unsigned
; loc         <something> : * struct <something>
; RPN'ized expression: "tmp16 uip_buf 14 + *u &u (something504) wnd -> *u 0 + *u (something503) 8 << uip_buf 14 + *u &u (something506) wnd -> *u 1 + *u (something505) + = "
; Expanded expression: "tmp16 uip_buf 14 + 34 + *(1) 8 << uip_buf 14 + 35 + *(1) + =(2) "
; Fused expression:    "+ uip_buf 14 + ax 34 << *ax 8 push-ax + uip_buf 14 + ax 35 + *sp *ax =(170) *tmp16 ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 34
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	shl	ax, 8
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 35
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	ax
	add	ax, cx
	mov	[_tmp16], ax
; if
; RPN'ized expression: "tmp16 uip_connr initialmss -> *u > tmp16 0 == || "
; Expanded expression: "tmp16 *(2) (@-2) *(2) 20 + *(2) >u [sh||->509] tmp16 *(2) 0 == ||[509] "
; Fused expression:    "+ *(@-2) 20 >u *tmp16 *ax [sh||->509] == *tmp16 0 ||[509]  "
	mov	ax, [bp-2]
	add	ax, 20
	mov	bx, ax
	mov	cx, [bx]
	mov	ax, [_tmp16]
	cmp	ax, cx
	seta	al
	cbw
; JumpIfNotZero
	test	ax, ax
	jne	L509
	mov	ax, [_tmp16]
	cmp	ax, 0
	sete	al
	cbw
L509:
; JumpIfZero
	test	ax, ax
	je	L507
; {
; RPN'ized expression: "tmp16 uip_connr initialmss -> *u = "
; Expanded expression: "tmp16 (@-2) *(2) 20 + *(2) =(2) "
; Fused expression:    "+ *(@-2) 20 =(170) *tmp16 *ax "
	mov	ax, [bp-2]
	add	ax, 20
	mov	bx, ax
	mov	ax, [bx]
	mov	[_tmp16], ax
; }
L507:
; RPN'ized expression: "uip_connr mss -> *u tmp16 = "
; Expanded expression: "(@-2) *(2) 18 + tmp16 *(2) =(2) "
; Fused expression:    "+ *(@-2) 18 =(170) *ax *tmp16 "
	mov	ax, [bp-2]
	add	ax, 18
	mov	bx, ax
	mov	ax, [_tmp16]
	mov	[bx], ax
; if
; RPN'ized expression: "uip_flags 2 1 | & "
; Expanded expression: "uip_flags *(1) 3 & "
; Fused expression:    "& *uip_flags 3  "
	mov	al, [_uip_flags]
	mov	ah, 0
	and	ax, 3
; JumpIfZero
	test	ax, ax
	je	L510
; {
; RPN'ized expression: "uip_slen 0 = "
; Expanded expression: "uip_slen 0 =(2) "
; Fused expression:    "=(170) *uip_slen 0 "
	mov	ax, 0
	mov	[_uip_slen], ax
; RPN'ized expression: "( tcp_appcall ) "
; Expanded expression: " tcp_appcall ()0 "
; Fused expression:    "( tcp_appcall )0 "
	call	_tcp_appcall
; appsend:
L193:
; if
; RPN'ized expression: "uip_flags 32 & "
; Expanded expression: "uip_flags *(1) 32 & "
; Fused expression:    "& *uip_flags 32  "
	mov	al, [_uip_flags]
	mov	ah, 0
	and	ax, 32
; JumpIfZero
	test	ax, ax
	je	L512
; {
; RPN'ized expression: "uip_slen 0 = "
; Expanded expression: "uip_slen 0 =(2) "
; Fused expression:    "=(170) *uip_slen 0 "
	mov	ax, 0
	mov	[_uip_slen], ax
; RPN'ized expression: "uip_connr tcpstateflags -> *u 0 = "
; Expanded expression: "(@-2) *(2) 25 + 0 =(1) "
; Fused expression:    "+ *(@-2) 25 =(154) *ax 0 "
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	ax, 0
	mov	[bx], al
	mov	ah, 0
; loc                 <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something514) flags -> *u 4 16 | = "
; Expanded expression: "uip_buf 14 + 33 + 20 =(1) "
; Fused expression:    "+ uip_buf 14 + ax 33 =(154) *ax 20 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 33
	mov	bx, ax
	mov	ax, 20
	mov	[bx], al
	mov	ah, 0
; goto tcp_send_nodata
	jmp	L175
; }
L512:
; if
; RPN'ized expression: "uip_flags 16 & "
; Expanded expression: "uip_flags *(1) 16 & "
; Fused expression:    "& *uip_flags 16  "
	mov	al, [_uip_flags]
	mov	ah, 0
	and	ax, 16
; JumpIfZero
	test	ax, ax
	je	L515
; {
; RPN'ized expression: "uip_slen 0 = "
; Expanded expression: "uip_slen 0 =(2) "
; Fused expression:    "=(170) *uip_slen 0 "
	mov	ax, 0
	mov	[_uip_slen], ax
; RPN'ized expression: "uip_connr len -> *u 1 = "
; Expanded expression: "(@-2) *(2) 16 + 1 =(2) "
; Fused expression:    "+ *(@-2) 16 =(170) *ax 1 "
	mov	ax, [bp-2]
	add	ax, 16
	mov	bx, ax
	mov	ax, 1
	mov	[bx], ax
; RPN'ized expression: "uip_connr tcpstateflags -> *u 4 = "
; Expanded expression: "(@-2) *(2) 25 + 4 =(1) "
; Fused expression:    "+ *(@-2) 25 =(154) *ax 4 "
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	ax, 4
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_connr nrtx -> *u 0 = "
; Expanded expression: "(@-2) *(2) 27 + 0 =(1) "
; Fused expression:    "+ *(@-2) 27 =(154) *ax 0 "
	mov	ax, [bp-2]
	add	ax, 27
	mov	bx, ax
	mov	ax, 0
	mov	[bx], al
	mov	ah, 0
; loc                 <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something517) flags -> *u 1 16 | = "
; Expanded expression: "uip_buf 14 + 33 + 17 =(1) "
; Fused expression:    "+ uip_buf 14 + ax 33 =(154) *ax 17 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 33
	mov	bx, ax
	mov	ax, 17
	mov	[bx], al
	mov	ah, 0
; goto tcp_send_nodata
	jmp	L175
; }
L515:
; if
; RPN'ized expression: "uip_slen 0 > "
; Expanded expression: "uip_slen *(2) 0 >u "
; Fused expression:    ">u *uip_slen 0 IF! "
	mov	ax, [_uip_slen]
	cmp	ax, 0
	jbe	L518
; {
; if
; RPN'ized expression: "uip_flags 1 & 0 != "
; Expanded expression: "uip_flags *(1) 1 & 0 != "
; Fused expression:    "& *uip_flags 1 != ax 0 IF! "
	mov	al, [_uip_flags]
	mov	ah, 0
	and	ax, 1
	cmp	ax, 0
	je	L520
; {
; RPN'ized expression: "uip_connr len -> *u 0 = "
; Expanded expression: "(@-2) *(2) 16 + 0 =(2) "
; Fused expression:    "+ *(@-2) 16 =(170) *ax 0 "
	mov	ax, [bp-2]
	add	ax, 16
	mov	bx, ax
	mov	ax, 0
	mov	[bx], ax
; }
L520:
; if
; RPN'ized expression: "uip_connr len -> *u 0 == "
; Expanded expression: "(@-2) *(2) 16 + *(2) 0 == "
; Fused expression:    "+ *(@-2) 16 == *ax 0 IF! "
	mov	ax, [bp-2]
	add	ax, 16
	mov	bx, ax
	mov	ax, [bx]
	cmp	ax, 0
	jne	L522
; {
; if
; RPN'ized expression: "uip_slen uip_connr mss -> *u > "
; Expanded expression: "uip_slen *(2) (@-2) *(2) 18 + *(2) >u "
; Fused expression:    "+ *(@-2) 18 >u *uip_slen *ax IF! "
	mov	ax, [bp-2]
	add	ax, 18
	mov	bx, ax
	mov	cx, [bx]
	mov	ax, [_uip_slen]
	cmp	ax, cx
	jbe	L524
; {
; RPN'ized expression: "uip_slen uip_connr mss -> *u = "
; Expanded expression: "uip_slen (@-2) *(2) 18 + *(2) =(2) "
; Fused expression:    "+ *(@-2) 18 =(170) *uip_slen *ax "
	mov	ax, [bp-2]
	add	ax, 18
	mov	bx, ax
	mov	ax, [bx]
	mov	[_uip_slen], ax
; }
L524:
; RPN'ized expression: "uip_connr len -> *u uip_slen = "
; Expanded expression: "(@-2) *(2) 16 + uip_slen *(2) =(2) "
; Fused expression:    "+ *(@-2) 16 =(170) *ax *uip_slen "
	mov	ax, [bp-2]
	add	ax, 16
	mov	bx, ax
	mov	ax, [_uip_slen]
	mov	[bx], ax
; }
	jmp	L523
L522:
; else
; {
; RPN'ized expression: "uip_slen uip_connr len -> *u = "
; Expanded expression: "uip_slen (@-2) *(2) 16 + *(2) =(2) "
; Fused expression:    "+ *(@-2) 16 =(170) *uip_slen *ax "
	mov	ax, [bp-2]
	add	ax, 16
	mov	bx, ax
	mov	ax, [bx]
	mov	[_uip_slen], ax
; }
L523:
; }
	jmp	L519
L518:
; else
; {
; RPN'ized expression: "uip_connr len -> *u 0 = "
; Expanded expression: "(@-2) *(2) 16 + 0 =(2) "
; Fused expression:    "+ *(@-2) 16 =(170) *ax 0 "
	mov	ax, [bp-2]
	add	ax, 16
	mov	bx, ax
	mov	ax, 0
	mov	[bx], ax
; }
L519:
; RPN'ized expression: "uip_connr nrtx -> *u 0 = "
; Expanded expression: "(@-2) *(2) 27 + 0 =(1) "
; Fused expression:    "+ *(@-2) 27 =(154) *ax 0 "
	mov	ax, [bp-2]
	add	ax, 27
	mov	bx, ax
	mov	ax, 0
	mov	[bx], al
	mov	ah, 0
; apprexmit:
L186:
; RPN'ized expression: "uip_appdata uip_sappdata = "
; Expanded expression: "uip_appdata uip_sappdata *(2) =(2) "
; Fused expression:    "=(170) *uip_appdata *uip_sappdata "
	mov	ax, [_uip_sappdata]
	mov	[_uip_appdata], ax
; if
; RPN'ized expression: "uip_slen 0 > uip_connr len -> *u 0 > && "
; Expanded expression: "uip_slen *(2) 0 >u [sh&&->528] (@-2) *(2) 16 + *(2) 0 >u &&[528] "
; Fused expression:    ">u *uip_slen 0 [sh&&->528] + *(@-2) 16 >u *ax 0 &&[528]  "
	mov	ax, [_uip_slen]
	cmp	ax, 0
	seta	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L528
	mov	ax, [bp-2]
	add	ax, 16
	mov	bx, ax
	mov	ax, [bx]
	cmp	ax, 0
	seta	al
	cbw
L528:
; JumpIfZero
	test	ax, ax
	je	L526
; {
; RPN'ized expression: "uip_len uip_connr len -> *u 40 + = "
; Expanded expression: "uip_len (@-2) *(2) 16 + *(2) 40 + =(2) "
; Fused expression:    "+ *(@-2) 16 + *ax 40 =(170) *uip_len ax "
	mov	ax, [bp-2]
	add	ax, 16
	mov	bx, ax
	mov	ax, [bx]
	add	ax, 40
	mov	[_uip_len], ax
; loc                 <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something529) flags -> *u 16 8 | = "
; Expanded expression: "uip_buf 14 + 33 + 24 =(1) "
; Fused expression:    "+ uip_buf 14 + ax 33 =(154) *ax 24 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 33
	mov	bx, ax
	mov	ax, 24
	mov	[bx], al
	mov	ah, 0
; goto tcp_send_noopts
	jmp	L530
; }
L526:
; if
; RPN'ized expression: "uip_flags 2 & "
; Expanded expression: "uip_flags *(1) 2 & "
; Fused expression:    "& *uip_flags 2  "
	mov	al, [_uip_flags]
	mov	ah, 0
	and	ax, 2
; JumpIfZero
	test	ax, ax
	je	L531
; {
; RPN'ized expression: "uip_len 40 = "
; Expanded expression: "uip_len 40 =(2) "
; Fused expression:    "=(170) *uip_len 40 "
	mov	ax, 40
	mov	[_uip_len], ax
; loc                 <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something533) flags -> *u 16 = "
; Expanded expression: "uip_buf 14 + 33 + 16 =(1) "
; Fused expression:    "+ uip_buf 14 + ax 33 =(154) *ax 16 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 33
	mov	bx, ax
	mov	ax, 16
	mov	[bx], al
	mov	ah, 0
; goto tcp_send_noopts
	jmp	L530
; }
L531:
; }
L510:
; goto drop
	jmp	L194
; case
; RPN'ized expression: "8 "
; Expanded expression: "8 "
; Expression value: 8
L534:
; if
; RPN'ized expression: "uip_flags 1 & "
; Expanded expression: "uip_flags *(1) 1 & "
; Fused expression:    "& *uip_flags 1  "
	mov	al, [_uip_flags]
	mov	ah, 0
	and	ax, 1
; JumpIfZero
	test	ax, ax
	je	L535
; {
; RPN'ized expression: "uip_connr tcpstateflags -> *u 0 = "
; Expanded expression: "(@-2) *(2) 25 + 0 =(1) "
; Fused expression:    "+ *(@-2) 25 =(154) *ax 0 "
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	ax, 0
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_flags 16 = "
; Expanded expression: "uip_flags 16 =(1) "
; Fused expression:    "=(154) *uip_flags 16 "
	mov	ax, 16
	mov	[_uip_flags], al
	mov	ah, 0
; RPN'ized expression: "( tcp_appcall ) "
; Expanded expression: " tcp_appcall ()0 "
; Fused expression:    "( tcp_appcall )0 "
	call	_tcp_appcall
; }
L535:
; break
	jmp	L445
; case
; RPN'ized expression: "4 "
; Expanded expression: "4 "
; Expression value: 4
L537:
; if
; RPN'ized expression: "uip_len 0 > "
; Expanded expression: "uip_len *(2) 0 >u "
; Fused expression:    ">u *uip_len 0 IF! "
	mov	ax, [_uip_len]
	cmp	ax, 0
	jbe	L538
; {
; RPN'ized expression: "( uip_len uip_add_rcv_nxt ) "
; Expanded expression: " uip_len *(2)  uip_add_rcv_nxt ()2 "
; Fused expression:    "( *(2) uip_len , uip_add_rcv_nxt )2 "
	push	word [_uip_len]
	call	_uip_add_rcv_nxt
	sub	sp, -2
; }
L538:
; if
; loc         <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something542) flags -> *u 1 & "
; Expanded expression: "uip_buf 14 + 33 + *(1) 1 & "
; Fused expression:    "+ uip_buf 14 + ax 33 & *ax 1  "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 33
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	and	ax, 1
; JumpIfZero
	test	ax, ax
	je	L540
; {
; if
; RPN'ized expression: "uip_flags 1 & "
; Expanded expression: "uip_flags *(1) 1 & "
; Fused expression:    "& *uip_flags 1  "
	mov	al, [_uip_flags]
	mov	ah, 0
	and	ax, 1
; JumpIfZero
	test	ax, ax
	je	L543
; {
; RPN'ized expression: "uip_connr tcpstateflags -> *u 7 = "
; Expanded expression: "(@-2) *(2) 25 + 7 =(1) "
; Fused expression:    "+ *(@-2) 25 =(154) *ax 7 "
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	ax, 7
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_connr timer -> *u 0 = "
; Expanded expression: "(@-2) *(2) 26 + 0 =(1) "
; Fused expression:    "+ *(@-2) 26 =(154) *ax 0 "
	mov	ax, [bp-2]
	add	ax, 26
	mov	bx, ax
	mov	ax, 0
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_connr len -> *u 0 = "
; Expanded expression: "(@-2) *(2) 16 + 0 =(2) "
; Fused expression:    "+ *(@-2) 16 =(170) *ax 0 "
	mov	ax, [bp-2]
	add	ax, 16
	mov	bx, ax
	mov	ax, 0
	mov	[bx], ax
; }
	jmp	L544
L543:
; else
; {
; RPN'ized expression: "uip_connr tcpstateflags -> *u 6 = "
; Expanded expression: "(@-2) *(2) 25 + 6 =(1) "
; Fused expression:    "+ *(@-2) 25 =(154) *ax 6 "
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	ax, 6
	mov	[bx], al
	mov	ah, 0
; }
L544:
; RPN'ized expression: "( 1 uip_add_rcv_nxt ) "
; Expanded expression: " 1  uip_add_rcv_nxt ()2 "
; Fused expression:    "( 1 , uip_add_rcv_nxt )2 "
	push	1
	call	_uip_add_rcv_nxt
	sub	sp, -2
; RPN'ized expression: "uip_flags 16 = "
; Expanded expression: "uip_flags 16 =(1) "
; Fused expression:    "=(154) *uip_flags 16 "
	mov	ax, 16
	mov	[_uip_flags], al
	mov	ah, 0
; RPN'ized expression: "( tcp_appcall ) "
; Expanded expression: " tcp_appcall ()0 "
; Fused expression:    "( tcp_appcall )0 "
	call	_tcp_appcall
; goto tcp_send_ack
	jmp	L427
; }
	jmp	L541
L540:
; else
; if
; RPN'ized expression: "uip_flags 1 & "
; Expanded expression: "uip_flags *(1) 1 & "
; Fused expression:    "& *uip_flags 1  "
	mov	al, [_uip_flags]
	mov	ah, 0
	and	ax, 1
; JumpIfZero
	test	ax, ax
	je	L545
; {
; RPN'ized expression: "uip_connr tcpstateflags -> *u 5 = "
; Expanded expression: "(@-2) *(2) 25 + 5 =(1) "
; Fused expression:    "+ *(@-2) 25 =(154) *ax 5 "
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	ax, 5
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_connr len -> *u 0 = "
; Expanded expression: "(@-2) *(2) 16 + 0 =(2) "
; Fused expression:    "+ *(@-2) 16 =(170) *ax 0 "
	mov	ax, [bp-2]
	add	ax, 16
	mov	bx, ax
	mov	ax, 0
	mov	[bx], ax
; goto drop
	jmp	L194
; }
L545:
L541:
; if
; RPN'ized expression: "uip_len 0 > "
; Expanded expression: "uip_len *(2) 0 >u "
; Fused expression:    ">u *uip_len 0 IF! "
	mov	ax, [_uip_len]
	cmp	ax, 0
	jbe	L547
; {
; goto tcp_send_ack
	jmp	L427
; }
L547:
; goto drop
	jmp	L194
; case
; RPN'ized expression: "5 "
; Expanded expression: "5 "
; Expression value: 5
L549:
; if
; RPN'ized expression: "uip_len 0 > "
; Expanded expression: "uip_len *(2) 0 >u "
; Fused expression:    ">u *uip_len 0 IF! "
	mov	ax, [_uip_len]
	cmp	ax, 0
	jbe	L550
; {
; RPN'ized expression: "( uip_len uip_add_rcv_nxt ) "
; Expanded expression: " uip_len *(2)  uip_add_rcv_nxt ()2 "
; Fused expression:    "( *(2) uip_len , uip_add_rcv_nxt )2 "
	push	word [_uip_len]
	call	_uip_add_rcv_nxt
	sub	sp, -2
; }
L550:
; if
; loc         <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something554) flags -> *u 1 & "
; Expanded expression: "uip_buf 14 + 33 + *(1) 1 & "
; Fused expression:    "+ uip_buf 14 + ax 33 & *ax 1  "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 33
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	and	ax, 1
; JumpIfZero
	test	ax, ax
	je	L552
; {
; RPN'ized expression: "uip_connr tcpstateflags -> *u 7 = "
; Expanded expression: "(@-2) *(2) 25 + 7 =(1) "
; Fused expression:    "+ *(@-2) 25 =(154) *ax 7 "
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	ax, 7
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_connr timer -> *u 0 = "
; Expanded expression: "(@-2) *(2) 26 + 0 =(1) "
; Fused expression:    "+ *(@-2) 26 =(154) *ax 0 "
	mov	ax, [bp-2]
	add	ax, 26
	mov	bx, ax
	mov	ax, 0
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "( 1 uip_add_rcv_nxt ) "
; Expanded expression: " 1  uip_add_rcv_nxt ()2 "
; Fused expression:    "( 1 , uip_add_rcv_nxt )2 "
	push	1
	call	_uip_add_rcv_nxt
	sub	sp, -2
; RPN'ized expression: "uip_flags 16 = "
; Expanded expression: "uip_flags 16 =(1) "
; Fused expression:    "=(154) *uip_flags 16 "
	mov	ax, 16
	mov	[_uip_flags], al
	mov	ah, 0
; RPN'ized expression: "( tcp_appcall ) "
; Expanded expression: " tcp_appcall ()0 "
; Fused expression:    "( tcp_appcall )0 "
	call	_tcp_appcall
; goto tcp_send_ack
	jmp	L427
; }
L552:
; if
; RPN'ized expression: "uip_len 0 > "
; Expanded expression: "uip_len *(2) 0 >u "
; Fused expression:    ">u *uip_len 0 IF! "
	mov	ax, [_uip_len]
	cmp	ax, 0
	jbe	L555
; {
; goto tcp_send_ack
	jmp	L427
; }
L555:
; goto drop
	jmp	L194
; case
; RPN'ized expression: "7 "
; Expanded expression: "7 "
; Expression value: 7
L557:
; goto tcp_send_ack
	jmp	L427
; case
; RPN'ized expression: "6 "
; Expanded expression: "6 "
; Expression value: 6
L558:
; if
; RPN'ized expression: "uip_flags 1 & "
; Expanded expression: "uip_flags *(1) 1 & "
; Fused expression:    "& *uip_flags 1  "
	mov	al, [_uip_flags]
	mov	ah, 0
	and	ax, 1
; JumpIfZero
	test	ax, ax
	je	L559
; {
; RPN'ized expression: "uip_connr tcpstateflags -> *u 7 = "
; Expanded expression: "(@-2) *(2) 25 + 7 =(1) "
; Fused expression:    "+ *(@-2) 25 =(154) *ax 7 "
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	ax, 7
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_connr timer -> *u 0 = "
; Expanded expression: "(@-2) *(2) 26 + 0 =(1) "
; Fused expression:    "+ *(@-2) 26 =(154) *ax 0 "
	mov	ax, [bp-2]
	add	ax, 26
	mov	bx, ax
	mov	ax, 0
	mov	[bx], al
	mov	ah, 0
; }
L559:
; }
	jmp	L445
L446:
	cmp	ax, 1
	je	L447
	cmp	ax, 2
	je	L452
	cmp	ax, 3
	je	L480
	cmp	ax, 8
	je	L534
	cmp	ax, 4
	je	L537
	cmp	ax, 5
	je	L549
	cmp	ax, 7
	je	L557
	cmp	ax, 6
	je	L558
L445:
; goto drop
	jmp	L194
; tcp_send_ack:
L427:
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something561) flags -> *u 16 = "
; Expanded expression: "uip_buf 14 + 33 + 16 =(1) "
; Fused expression:    "+ uip_buf 14 + ax 33 =(154) *ax 16 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 33
	mov	bx, ax
	mov	ax, 16
	mov	[bx], al
	mov	ah, 0
; tcp_send_nodata:
L175:
; RPN'ized expression: "uip_len 40 = "
; Expanded expression: "uip_len 40 =(2) "
; Fused expression:    "=(170) *uip_len 40 "
	mov	ax, 40
	mov	[_uip_len], ax
; tcp_send_noopts:
L530:
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something562) tcpoffset -> *u 5 4 << = "
; Expanded expression: "uip_buf 14 + 32 + 80 =(1) "
; Fused expression:    "+ uip_buf 14 + ax 32 =(154) *ax 80 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 32
	mov	bx, ax
	mov	ax, 80
	mov	[bx], al
	mov	ah, 0
; tcp_send:
L412:
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something563) ackno -> *u 0 + *u uip_connr rcv_nxt -> *u 0 + *u = "
; Expanded expression: "uip_buf 14 + 28 + (@-2) *(2) 8 + *(1) =(1) "
; Fused expression:    "+ uip_buf 14 + ax 28 push-ax + *(@-2) 8 =(153) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 28
	push	ax
	mov	ax, [bp-2]
	add	ax, 8
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something564) ackno -> *u 1 + *u uip_connr rcv_nxt -> *u 1 + *u = "
; Expanded expression: "uip_buf 14 + 29 + (@-2) *(2) 9 + *(1) =(1) "
; Fused expression:    "+ uip_buf 14 + ax 29 push-ax + *(@-2) 9 =(153) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 29
	push	ax
	mov	ax, [bp-2]
	add	ax, 9
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something565) ackno -> *u 2 + *u uip_connr rcv_nxt -> *u 2 + *u = "
; Expanded expression: "uip_buf 14 + 30 + (@-2) *(2) 10 + *(1) =(1) "
; Fused expression:    "+ uip_buf 14 + ax 30 push-ax + *(@-2) 10 =(153) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 30
	push	ax
	mov	ax, [bp-2]
	add	ax, 10
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something566) ackno -> *u 3 + *u uip_connr rcv_nxt -> *u 3 + *u = "
; Expanded expression: "uip_buf 14 + 31 + (@-2) *(2) 11 + *(1) =(1) "
; Fused expression:    "+ uip_buf 14 + ax 31 push-ax + *(@-2) 11 =(153) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 31
	push	ax
	mov	ax, [bp-2]
	add	ax, 11
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something567) seqno -> *u 0 + *u uip_connr snd_nxt -> *u 0 + *u = "
; Expanded expression: "uip_buf 14 + 24 + (@-2) *(2) 12 + *(1) =(1) "
; Fused expression:    "+ uip_buf 14 + ax 24 push-ax + *(@-2) 12 =(153) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 24
	push	ax
	mov	ax, [bp-2]
	add	ax, 12
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something568) seqno -> *u 1 + *u uip_connr snd_nxt -> *u 1 + *u = "
; Expanded expression: "uip_buf 14 + 25 + (@-2) *(2) 13 + *(1) =(1) "
; Fused expression:    "+ uip_buf 14 + ax 25 push-ax + *(@-2) 13 =(153) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 25
	push	ax
	mov	ax, [bp-2]
	add	ax, 13
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something569) seqno -> *u 2 + *u uip_connr snd_nxt -> *u 2 + *u = "
; Expanded expression: "uip_buf 14 + 26 + (@-2) *(2) 14 + *(1) =(1) "
; Fused expression:    "+ uip_buf 14 + ax 26 push-ax + *(@-2) 14 =(153) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 26
	push	ax
	mov	ax, [bp-2]
	add	ax, 14
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something570) seqno -> *u 3 + *u uip_connr snd_nxt -> *u 3 + *u = "
; Expanded expression: "uip_buf 14 + 27 + (@-2) *(2) 15 + *(1) =(1) "
; Fused expression:    "+ uip_buf 14 + ax 27 push-ax + *(@-2) 15 =(153) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 27
	push	ax
	mov	ax, [bp-2]
	add	ax, 15
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something571) proto -> *u 6 = "
; Expanded expression: "uip_buf 14 + 9 + 6 =(1) "
; Fused expression:    "+ uip_buf 14 + ax 9 =(154) *ax 6 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 9
	mov	bx, ax
	mov	ax, 6
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something572) srcport -> *u uip_connr lport -> *u = "
; Expanded expression: "uip_buf 14 + 20 + (@-2) *(2) 4 + *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 20 push-ax + *(@-2) 4 =(170) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 20
	push	ax
	mov	ax, [bp-2]
	add	ax, 4
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something573) destport -> *u uip_connr rport -> *u = "
; Expanded expression: "uip_buf 14 + 22 + (@-2) *(2) 6 + *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 22 push-ax + *(@-2) 6 =(170) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 22
	push	ax
	mov	ax, [bp-2]
	add	ax, 6
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something574) srcipaddr -> *u 0 + *u uip_hostaddr 0 + *u = "
; Expanded expression: "uip_buf 14 + 12 + uip_hostaddr 0 + *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 12 push-ax + uip_hostaddr 0 =(170) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 12
	push	ax
	mov	ax, _uip_hostaddr
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something575) srcipaddr -> *u 1 + *u uip_hostaddr 1 + *u = "
; Expanded expression: "uip_buf 14 + 14 + uip_hostaddr 2 + *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 14 push-ax + uip_hostaddr 2 =(170) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 14
	push	ax
	mov	ax, _uip_hostaddr
	add	ax, 2
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something576) destipaddr -> *u 0 + *u uip_connr ripaddr -> *u 0 + *u = "
; Expanded expression: "uip_buf 14 + 16 + (@-2) *(2) 0 + *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 16 push-ax + *(@-2) 0 =(170) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 16
	push	ax
	mov	ax, [bp-2]
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something577) destipaddr -> *u 1 + *u uip_connr ripaddr -> *u 1 + *u = "
; Expanded expression: "uip_buf 14 + 18 + (@-2) *(2) 2 + *(2) =(2) "
; Fused expression:    "+ uip_buf 14 + ax 18 push-ax + *(@-2) 2 =(170) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 18
	push	ax
	mov	ax, [bp-2]
	add	ax, 2
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; if
; RPN'ized expression: "uip_connr tcpstateflags -> *u 16 & "
; Expanded expression: "(@-2) *(2) 25 + *(1) 16 & "
; Fused expression:    "+ *(@-2) 25 & *ax 16  "
	mov	ax, [bp-2]
	add	ax, 25
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	and	ax, 16
; JumpIfZero
	test	ax, ax
	je	L578
; {
; loc         <something> : * struct <something>
; loc         <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something580) wnd -> *u 0 + *u uip_buf 14 + *u &u (something581) wnd -> *u 1 + *u 0 = = "
; Expanded expression: "uip_buf 14 + 34 + uip_buf 14 + 35 + 0 =(1) =(1) "
; Fused expression:    "+ uip_buf 14 + ax 34 push-ax + uip_buf 14 + ax 35 =(154) *ax 0 =(154) **sp ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 34
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 35
	mov	bx, ax
	mov	ax, 0
	mov	[bx], al
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; }
	jmp	L579
L578:
; else
; {
; loc         <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something582) wnd -> *u 0 + *u 32767 8 >> = "
; Expanded expression: "uip_buf 14 + 34 + 127 =(1) "
; Fused expression:    "+ uip_buf 14 + ax 34 =(154) *ax 127 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 34
	mov	bx, ax
	mov	ax, 127
	mov	[bx], al
	mov	ah, 0
; loc         <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something583) wnd -> *u 1 + *u 32767 255 & = "
; Expanded expression: "uip_buf 14 + 35 + 255 =(1) "
; Fused expression:    "+ uip_buf 14 + ax 35 =(154) *ax 255 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 35
	mov	bx, ax
	mov	ax, 255
	mov	[bx], al
	mov	ah, 0
; }
L579:
; tcp_send_noconn:
L362:
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something584) len -> *u 0 + *u uip_len 8 >> = "
; Expanded expression: "uip_buf 14 + 2 + uip_len *(2) 8 >>u =(1) "
; Fused expression:    "+ uip_buf 14 + ax 2 push-ax >>u *uip_len 8 =(154) **sp ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 2
	push	ax
	mov	ax, [_uip_len]
	shr	ax, 8
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something585) len -> *u 1 + *u uip_len 255 & = "
; Expanded expression: "uip_buf 14 + 3 + uip_len *(2) 255 & =(1) "
; Fused expression:    "+ uip_buf 14 + ax 3 push-ax & *uip_len 255 =(154) **sp ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 3
	push	ax
	mov	ax, [_uip_len]
	and	ax, 255
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something586) tcpchksum -> *u 0 = "
; Expanded expression: "uip_buf 14 + 36 + 0 =(2) "
; Fused expression:    "+ uip_buf 14 + ax 36 =(170) *ax 0 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 36
	mov	bx, ax
	mov	ax, 0
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something587) tcpchksum -> *u ( uip_tcpchksum ) ~ = "
; Expanded expression: "uip_buf 14 + 36 +  uip_tcpchksum ()0 ~ =(2) "
; Fused expression:    "+ uip_buf 14 + ax 36 push-ax ( uip_tcpchksum )0 ~ =(170) **sp ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 36
	push	ax
	call	_uip_tcpchksum
	not	ax
	pop	bx
	mov	[bx], ax
; ip_send_nolen:
L289:
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something588) vhl -> *u 69 = "
; Expanded expression: "uip_buf 14 + 0 + 69 =(1) "
; Fused expression:    "+ uip_buf 14 + ax 0 =(154) *ax 69 "
	mov	ax, _uip_buf
	add	ax, 14
	mov	bx, ax
	mov	ax, 69
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something589) tos -> *u 0 = "
; Expanded expression: "uip_buf 14 + 1 + 0 =(1) "
; Fused expression:    "+ uip_buf 14 + ax 1 =(154) *ax 0 "
	mov	ax, _uip_buf
	add	ax, 14
	inc	ax
	mov	bx, ax
	mov	ax, 0
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something590) ipoffset -> *u 0 + *u uip_buf 14 + *u &u (something591) ipoffset -> *u 1 + *u 0 = = "
; Expanded expression: "uip_buf 14 + 6 + uip_buf 14 + 7 + 0 =(1) =(1) "
; Fused expression:    "+ uip_buf 14 + ax 6 push-ax + uip_buf 14 + ax 7 =(154) *ax 0 =(154) **sp ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 6
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 7
	mov	bx, ax
	mov	ax, 0
	mov	[bx], al
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something592) ttl -> *u 255 = "
; Expanded expression: "uip_buf 14 + 8 + 255 =(1) "
; Fused expression:    "+ uip_buf 14 + ax 8 =(154) *ax 255 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 8
	mov	bx, ax
	mov	ax, 255
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "ipid ++ "
; Expanded expression: "ipid ++(2) "
; Fused expression:    "++(2) *ipid "
	inc	word [_ipid]
	mov	ax, [_ipid]
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something593) ipid -> *u 0 + *u ipid 8 >> = "
; Expanded expression: "uip_buf 14 + 4 + ipid *(2) 8 >>u =(1) "
; Fused expression:    "+ uip_buf 14 + ax 4 push-ax >>u *ipid 8 =(154) **sp ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 4
	push	ax
	mov	ax, [_ipid]
	shr	ax, 8
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something594) ipid -> *u 1 + *u ipid 255 & = "
; Expanded expression: "uip_buf 14 + 5 + ipid *(2) 255 & =(1) "
; Fused expression:    "+ uip_buf 14 + ax 5 push-ax & *ipid 255 =(154) **sp ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 5
	push	ax
	mov	ax, [_ipid]
	and	ax, 255
	pop	bx
	mov	[bx], al
	mov	ah, 0
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something595) ipchksum -> *u 0 = "
; Expanded expression: "uip_buf 14 + 10 + 0 =(2) "
; Fused expression:    "+ uip_buf 14 + ax 10 =(170) *ax 0 "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 10
	mov	bx, ax
	mov	ax, 0
	mov	[bx], ax
; loc     <something> : * struct <something>
; RPN'ized expression: "uip_buf 14 + *u &u (something596) ipchksum -> *u ( uip_ipchksum ) ~ = "
; Expanded expression: "uip_buf 14 + 10 +  uip_ipchksum ()0 ~ =(2) "
; Fused expression:    "+ uip_buf 14 + ax 10 push-ax ( uip_ipchksum )0 ~ =(170) **sp ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 10
	push	ax
	call	_uip_ipchksum
	not	ax
	pop	bx
	mov	[bx], ax
; RPN'ized expression: "uip_stat &u tcp -> *u &u sent -> *u ++ "
; Expanded expression: "uip_stat 26 + 4 + ++(2) "
; Fused expression:    "+ uip_stat 26 + ax 4 ++(2) *ax "
	mov	ax, _uip_stat
	add	ax, 26
	add	ax, 4
	mov	bx, ax
	inc	word [bx]
	mov	ax, [bx]
; send:
L258:
; RPN'ized expression: "uip_stat &u ip -> *u &u sent -> *u ++ "
; Expanded expression: "uip_stat 0 + 4 + ++(2) "
; Fused expression:    "+ uip_stat 0 + ax 4 ++(2) *ax "
	mov	ax, _uip_stat
	add	ax, 4
	mov	bx, ax
	inc	word [bx]
	mov	ax, [bx]
; return
	jmp	L146
; drop:
L194:
; RPN'ized expression: "uip_len 0 = "
; Expanded expression: "uip_len 0 =(2) "
; Fused expression:    "=(170) *uip_len 0 "
	mov	ax, 0
	mov	[_uip_len], ax
; return
L146:
	leave
	ret

; glb htons : (
; prm     val : unsigned
;     ) unsigned
section .text
	global	_htons
_htons:
	push	bp
	mov	bp, sp
	;sub	sp,          0
; loc     val : (@4): unsigned
; return
; loc     <something> : unsigned
; RPN'ized expression: "val 255 & (something599) 8 << val 65280u & 8 >> | "
; Expanded expression: "(@4) *(2) 255 & 8 << (@4) *(2) 65280u & 8 >>u | "
; Fused expression:    "& *(@4) 255 << ax 8 push-ax & *(@4) 65280u >>u ax 8 | *sp ax  "
	mov	ax, [bp+4]
	and	ax, 255
	shl	ax, 8
	push	ax
	mov	ax, [bp+4]
	and	ax, -256
	shr	ax, 8
	mov	cx, ax
	pop	ax
	or	ax, cx
L597:
	leave
	ret

; RPN'ized expression: "6 "
; Expanded expression: "6 "
; Expression value: 6
; glb uip_ethaddr : struct uip_eth_addr
; glb uip_arp_init : (void) void
; glb uip_arp_ipin : (void) void
; glb uip_arp_arpin : (void) void
; glb uip_arp_out : (void) void
; glb uip_arp_timer : (void) void
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; glb uip_arp_draddr : [2u] unsigned
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; glb uip_arp_netmask : [2u] unsigned
; glb memset : (
; prm     a : * char
; prm     c2u : int
; prm     l : int
;     ) void
section .text
	global	_memset
_memset:
	push	bp
	mov	bp, sp
	;sub	sp,          0
; loc     a : (@4): * char
; loc     c2u : (@6): int
; loc     l : (@8): int
; while
; RPN'ized expression: "l --p "
; Expanded expression: "(@8) --p(2) "
L602:
; Fused expression:    "--p(2) *(@8)  "
	mov	ax, [bp+8]
	dec	word [bp+8]
; JumpIfZero
	test	ax, ax
	je	L603
; RPN'ized expression: "a ++p *u c2u = "
; Expanded expression: "(@4) ++p(2) (@6) *(2) =(-1) "
; Fused expression:    "++p(2) *(@4) =(122) *ax *(@6) "
	mov	ax, [bp+4]
	inc	word [bp+4]
	mov	bx, ax
	mov	ax, [bp+6]
	mov	[bx], al
	cbw
	jmp	L602
L603:
L600:
	leave
	ret

; glb memcpy : (
; prm     a : * char
; prm     b : * char
; prm     l : int
;     ) * char
section .text
	global	_memcpy
_memcpy:
	push	bp
	mov	bp, sp
	 sub	sp,          2
; loc     a : (@4): * char
; loc     b : (@6): * char
; loc     l : (@8): int
; for
; loc     i : (@-2): int
; RPN'ized expression: "i 0 = "
; Expanded expression: "(@-2) 0 =(2) "
; Fused expression:    "=(170) *(@-2) 0 "
	mov	ax, 0
	mov	[bp-2], ax
L606:
; RPN'ized expression: "i l < "
; Expanded expression: "(@-2) *(2) (@8) *(2) < "
; Fused expression:    "< *(@-2) *(@8) IF! "
	mov	ax, [bp-2]
	cmp	ax, [bp+8]
	jge	L609
; RPN'ized expression: "i ++p "
; Expanded expression: "(@-2) ++p(2) "
; RPN'ized expression: "a i + *u b i + *u = "
; Expanded expression: "(@4) *(2) (@-2) *(2) + (@6) *(2) (@-2) *(2) + *(-1) =(-1) "
; Fused expression:    "+ *(@4) *(@-2) push-ax + *(@6) *(@-2) =(119) **sp *ax "
	mov	ax, [bp+4]
	add	ax, [bp-2]
	push	ax
	mov	ax, [bp+6]
	add	ax, [bp-2]
	mov	bx, ax
	mov	al, [bx]
	cbw
	pop	bx
	mov	[bx], al
	cbw
L607:
; Fused expression:    "++p(2) *(@-2) "
	mov	ax, [bp-2]
	inc	word [bp-2]
	jmp	L606
L609:
; return
; RPN'ized expression: "a "
; Expanded expression: "(@4) *(2) "
; Fused expression:    "*(2) (@4)  "
	mov	ax, [bp+4]
L604:
	leave
	ret

; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; glb uip_ethaddr : struct uip_eth_addr
section .data
	global	_uip_ethaddr
_uip_ethaddr:
; =
; RPN'ized expression: "0 "
; Expanded expression: "0 "
; Expression value: 0
	db	0
; RPN'ized expression: "189 "
; Expanded expression: "189 "
; Expression value: 189
	db	189
; RPN'ized expression: "59 "
; Expanded expression: "59 "
; Expression value: 59
	db	59
; RPN'ized expression: "51 "
; Expanded expression: "51 "
; Expression value: 51
	db	51
; RPN'ized expression: "5 "
; Expanded expression: "5 "
; Expression value: 5
	db	5
; RPN'ized expression: "113 "
; Expanded expression: "113 "
; Expression value: 113
	db	113

; RPN'ized expression: "8 "
; Expanded expression: "8 "
; Expression value: 8
; glb arp_table : [8u] struct arp_entry
section .bss
	alignb 2
_arp_table:
	resb	96

; RPN'ized expression: "2 "
; Expanded expression: "2 "
; Expression value: 2
; glb ipaddr : [2u] unsigned
section .bss
	alignb 2
_ipaddr:
	resb	4

; glb i : unsigned char
section .bss
_i:
	resb	1

; glb c2u : unsigned char
section .bss
_c2u:
	resb	1

; glb arptime : unsigned char
section .bss
_arptime:
	resb	1

; glb tmpage : unsigned char
section .bss
_tmpage:
	resb	1

; glb uip_arp_init : (void) void
section .text
	global	_uip_arp_init
_uip_arp_init:
	push	bp
	mov	bp, sp
	;sub	sp,          0
; for
; RPN'ized expression: "i 0 = "
; Expanded expression: "i 0 =(1) "
; Fused expression:    "=(154) *i 0 "
	mov	ax, 0
	mov	[_i], al
	mov	ah, 0
L612:
; RPN'ized expression: "i 8 < "
; Expanded expression: "i *(1) 8 < "
; Fused expression:    "< *i 8 IF! "
	mov	al, [_i]
	mov	ah, 0
	cmp	ax, 8
	jge	L615
; RPN'ized expression: "i ++ "
; Expanded expression: "i ++(1) "
; {
; RPN'ized expression: "( 4 , 0 , arp_table i + *u &u ipaddr -> *u memset ) "
; Expanded expression: " 4  0  arp_table i *(1) 12 * + 0 +  memset ()6 "
; Fused expression:    "( 4 , 0 , * *i 12 + arp_table ax + ax 0 , memset )6 "
	push	4
	push	0
	mov	al, [_i]
	mov	ah, 0
	imul	ax, ax, 12
	mov	cx, ax
	mov	ax, _arp_table
	add	ax, cx
	push	ax
	call	_memset
	sub	sp, -6
; }
L613:
; Fused expression:    "++(1) *i "
	inc	byte [_i]
	mov	al, [_i]
	mov	ah, 0
	jmp	L612
L615:
L610:
	leave
	ret

; glb uip_arp_timer : (void) void
section .text
	global	_uip_arp_timer
_uip_arp_timer:
	push	bp
	mov	bp, sp
	 sub	sp,          2
; loc     tabptr : (@-2): * struct arp_entry
; RPN'ized expression: "arptime ++ "
; Expanded expression: "arptime ++(1) "
; Fused expression:    "++(1) *arptime "
	inc	byte [_arptime]
	mov	al, [_arptime]
	mov	ah, 0
; for
; RPN'ized expression: "i 0 = "
; Expanded expression: "i 0 =(1) "
; Fused expression:    "=(154) *i 0 "
	mov	ax, 0
	mov	[_i], al
	mov	ah, 0
L618:
; RPN'ized expression: "i 8 < "
; Expanded expression: "i *(1) 8 < "
; Fused expression:    "< *i 8 IF! "
	mov	al, [_i]
	mov	ah, 0
	cmp	ax, 8
	jge	L621
; RPN'ized expression: "i ++ "
; Expanded expression: "i ++(1) "
; {
; RPN'ized expression: "tabptr arp_table i + *u &u = "
; Expanded expression: "(@-2) arp_table i *(1) 12 * + =(2) "
; Fused expression:    "* *i 12 + arp_table ax =(170) *(@-2) ax "
	mov	al, [_i]
	mov	ah, 0
	imul	ax, ax, 12
	mov	cx, ax
	mov	ax, _arp_table
	add	ax, cx
	mov	[bp-2], ax
; if
; RPN'ized expression: "tabptr ipaddr -> *u 0 + *u tabptr ipaddr -> *u 1 + *u | 0 != arptime tabptr time -> *u - 120 >= && "
; Expanded expression: "(@-2) *(2) 0 + *(2) (@-2) *(2) 2 + *(2) | 0 != [sh&&->624] arptime *(1) (@-2) *(2) 10 + *(1) - 120 >= &&[624] "
; Fused expression:    "+ *(@-2) 0 push-ax + *(@-2) 2 | **sp *ax != ax 0 [sh&&->624] + *(@-2) 10 - *arptime *ax >= ax 120 &&[624]  "
	mov	ax, [bp-2]
	push	ax
	mov	ax, [bp-2]
	add	ax, 2
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	or	ax, cx
	cmp	ax, 0
	setne	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L624
	mov	ax, [bp-2]
	add	ax, 10
	mov	bx, ax
	movzx	cx, byte [bx]
	mov	al, [_arptime]
	mov	ah, 0
	sub	ax, cx
	cmp	ax, 120
	setge	al
	cbw
L624:
; JumpIfZero
	test	ax, ax
	je	L622
; {
; RPN'ized expression: "( 4 , 0 , tabptr ipaddr -> *u memset ) "
; Expanded expression: " 4  0  (@-2) *(2) 0 +  memset ()6 "
; Fused expression:    "( 4 , 0 , + *(@-2) 0 , memset )6 "
	push	4
	push	0
	mov	ax, [bp-2]
	push	ax
	call	_memset
	sub	sp, -6
; }
L622:
; }
L619:
; Fused expression:    "++(1) *i "
	inc	byte [_i]
	mov	al, [_i]
	mov	ah, 0
	jmp	L618
L621:
L616:
	leave
	ret

; glb uip_arp_update : (
; prm     ipaddr : * unsigned
; prm     ethaddr : * struct uip_eth_addr
;     ) void
section .text
_uip_arp_update:
	push	bp
	mov	bp, sp
	 sub	sp,          2
; loc     ipaddr : (@4): * unsigned
; loc     ethaddr : (@6): * struct uip_eth_addr
; loc     tabptr : (@-2): * struct arp_entry
; for
; RPN'ized expression: "i 0 = "
; Expanded expression: "i 0 =(1) "
; Fused expression:    "=(154) *i 0 "
	mov	ax, 0
	mov	[_i], al
	mov	ah, 0
L627:
; RPN'ized expression: "i 8 < "
; Expanded expression: "i *(1) 8 < "
; Fused expression:    "< *i 8 IF! "
	mov	al, [_i]
	mov	ah, 0
	cmp	ax, 8
	jge	L630
; RPN'ized expression: "i ++ "
; Expanded expression: "i ++(1) "
; {
; RPN'ized expression: "tabptr arp_table i + *u &u = "
; Expanded expression: "(@-2) arp_table i *(1) 12 * + =(2) "
; Fused expression:    "* *i 12 + arp_table ax =(170) *(@-2) ax "
	mov	al, [_i]
	mov	ah, 0
	imul	ax, ax, 12
	mov	cx, ax
	mov	ax, _arp_table
	add	ax, cx
	mov	[bp-2], ax
; if
; RPN'ized expression: "tabptr ipaddr -> *u 0 + *u 0 != tabptr ipaddr -> *u 1 + *u 0 != && "
; Expanded expression: "(@-2) *(2) 0 + *(2) 0 != [sh&&->633] (@-2) *(2) 2 + *(2) 0 != &&[633] "
; Fused expression:    "+ *(@-2) 0 != *ax 0 [sh&&->633] + *(@-2) 2 != *ax 0 &&[633]  "
	mov	ax, [bp-2]
	mov	bx, ax
	mov	ax, [bx]
	cmp	ax, 0
	setne	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L633
	mov	ax, [bp-2]
	add	ax, 2
	mov	bx, ax
	mov	ax, [bx]
	cmp	ax, 0
	setne	al
	cbw
L633:
; JumpIfZero
	test	ax, ax
	je	L631
; {
; if
; RPN'ized expression: "ipaddr 0 + *u tabptr ipaddr -> *u 0 + *u == ipaddr 1 + *u tabptr ipaddr -> *u 1 + *u == && "
; Expanded expression: "(@4) *(2) 0 + *(2) (@-2) *(2) 0 + *(2) == [sh&&->636] (@4) *(2) 2 + *(2) (@-2) *(2) 2 + *(2) == &&[636] "
; Fused expression:    "+ *(@4) 0 push-ax + *(@-2) 0 == **sp *ax [sh&&->636] + *(@4) 2 push-ax + *(@-2) 2 == **sp *ax &&[636]  "
	mov	ax, [bp+4]
	push	ax
	mov	ax, [bp-2]
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	cmp	ax, cx
	sete	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L636
	mov	ax, [bp+4]
	add	ax, 2
	push	ax
	mov	ax, [bp-2]
	add	ax, 2
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	cmp	ax, cx
	sete	al
	cbw
L636:
; JumpIfZero
	test	ax, ax
	je	L634
; {
; RPN'ized expression: "( 6 , ethaddr addr -> *u , tabptr ethaddr -> *u &u addr -> *u memcpy ) "
; Expanded expression: " 6  (@6) *(2) 0 +  (@-2) *(2) 4 + 0 +  memcpy ()6 "
; Fused expression:    "( 6 , + *(@6) 0 , + *(@-2) 4 + ax 0 , memcpy )6 "
	push	6
	mov	ax, [bp+6]
	push	ax
	mov	ax, [bp-2]
	add	ax, 4
	push	ax
	call	_memcpy
	sub	sp, -6
; RPN'ized expression: "tabptr time -> *u arptime = "
; Expanded expression: "(@-2) *(2) 10 + arptime *(1) =(1) "
; Fused expression:    "+ *(@-2) 10 =(153) *ax *arptime "
	mov	ax, [bp-2]
	add	ax, 10
	mov	bx, ax
	mov	al, [_arptime]
	mov	ah, 0
	mov	[bx], al
	mov	ah, 0
; return
	jmp	L625
; }
L634:
; }
L631:
; }
L628:
; Fused expression:    "++(1) *i "
	inc	byte [_i]
	mov	al, [_i]
	mov	ah, 0
	jmp	L627
L630:
; for
; RPN'ized expression: "i 0 = "
; Expanded expression: "i 0 =(1) "
; Fused expression:    "=(154) *i 0 "
	mov	ax, 0
	mov	[_i], al
	mov	ah, 0
L637:
; RPN'ized expression: "i 8 < "
; Expanded expression: "i *(1) 8 < "
; Fused expression:    "< *i 8 IF! "
	mov	al, [_i]
	mov	ah, 0
	cmp	ax, 8
	jge	L640
; RPN'ized expression: "i ++ "
; Expanded expression: "i ++(1) "
; {
; RPN'ized expression: "tabptr arp_table i + *u &u = "
; Expanded expression: "(@-2) arp_table i *(1) 12 * + =(2) "
; Fused expression:    "* *i 12 + arp_table ax =(170) *(@-2) ax "
	mov	al, [_i]
	mov	ah, 0
	imul	ax, ax, 12
	mov	cx, ax
	mov	ax, _arp_table
	add	ax, cx
	mov	[bp-2], ax
; if
; RPN'ized expression: "tabptr ipaddr -> *u 0 + *u 0 == tabptr ipaddr -> *u 1 + *u 0 == && "
; Expanded expression: "(@-2) *(2) 0 + *(2) 0 == [sh&&->643] (@-2) *(2) 2 + *(2) 0 == &&[643] "
; Fused expression:    "+ *(@-2) 0 == *ax 0 [sh&&->643] + *(@-2) 2 == *ax 0 &&[643]  "
	mov	ax, [bp-2]
	mov	bx, ax
	mov	ax, [bx]
	cmp	ax, 0
	sete	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L643
	mov	ax, [bp-2]
	add	ax, 2
	mov	bx, ax
	mov	ax, [bx]
	cmp	ax, 0
	sete	al
	cbw
L643:
; JumpIfZero
	test	ax, ax
	je	L641
; {
; break
	jmp	L640
; }
L641:
; }
L638:
; Fused expression:    "++(1) *i "
	inc	byte [_i]
	mov	al, [_i]
	mov	ah, 0
	jmp	L637
L640:
; if
; RPN'ized expression: "i 8 == "
; Expanded expression: "i *(1) 8 == "
; Fused expression:    "== *i 8 IF! "
	mov	al, [_i]
	mov	ah, 0
	cmp	ax, 8
	jne	L644
; {
; RPN'ized expression: "tmpage 0 = "
; Expanded expression: "tmpage 0 =(1) "
; Fused expression:    "=(154) *tmpage 0 "
	mov	ax, 0
	mov	[_tmpage], al
	mov	ah, 0
; RPN'ized expression: "c2u 0 = "
; Expanded expression: "c2u 0 =(1) "
; Fused expression:    "=(154) *c2u 0 "
	mov	ax, 0
	mov	[_c2u], al
	mov	ah, 0
; for
; RPN'ized expression: "i 0 = "
; Expanded expression: "i 0 =(1) "
; Fused expression:    "=(154) *i 0 "
	mov	ax, 0
	mov	[_i], al
	mov	ah, 0
L646:
; RPN'ized expression: "i 8 < "
; Expanded expression: "i *(1) 8 < "
; Fused expression:    "< *i 8 IF! "
	mov	al, [_i]
	mov	ah, 0
	cmp	ax, 8
	jge	L649
; RPN'ized expression: "i ++ "
; Expanded expression: "i ++(1) "
; {
; RPN'ized expression: "tabptr arp_table i + *u &u = "
; Expanded expression: "(@-2) arp_table i *(1) 12 * + =(2) "
; Fused expression:    "* *i 12 + arp_table ax =(170) *(@-2) ax "
	mov	al, [_i]
	mov	ah, 0
	imul	ax, ax, 12
	mov	cx, ax
	mov	ax, _arp_table
	add	ax, cx
	mov	[bp-2], ax
; if
; RPN'ized expression: "arptime tabptr time -> *u - tmpage > "
; Expanded expression: "arptime *(1) (@-2) *(2) 10 + *(1) - tmpage *(1) > "
; Fused expression:    "+ *(@-2) 10 - *arptime *ax > ax *tmpage IF! "
	mov	ax, [bp-2]
	add	ax, 10
	mov	bx, ax
	movzx	cx, byte [bx]
	mov	al, [_arptime]
	mov	ah, 0
	sub	ax, cx
	movzx	cx, byte [_tmpage]
	cmp	ax, cx
	jle	L650
; {
; RPN'ized expression: "tmpage arptime tabptr time -> *u - = "
; Expanded expression: "tmpage arptime *(1) (@-2) *(2) 10 + *(1) - =(1) "
; Fused expression:    "+ *(@-2) 10 - *arptime *ax =(154) *tmpage ax "
	mov	ax, [bp-2]
	add	ax, 10
	mov	bx, ax
	movzx	cx, byte [bx]
	mov	al, [_arptime]
	mov	ah, 0
	sub	ax, cx
	mov	[_tmpage], al
	mov	ah, 0
; RPN'ized expression: "c2u i = "
; Expanded expression: "c2u i *(1) =(1) "
; Fused expression:    "=(153) *c2u *i "
	mov	al, [_i]
	mov	ah, 0
	mov	[_c2u], al
	mov	ah, 0
; }
L650:
; }
L647:
; Fused expression:    "++(1) *i "
	inc	byte [_i]
	mov	al, [_i]
	mov	ah, 0
	jmp	L646
L649:
; RPN'ized expression: "i c2u = "
; Expanded expression: "i c2u *(1) =(1) "
; Fused expression:    "=(153) *i *c2u "
	mov	al, [_c2u]
	mov	ah, 0
	mov	[_i], al
	mov	ah, 0
; }
L644:
; RPN'ized expression: "( 4 , ipaddr , tabptr ipaddr -> *u memcpy ) "
; Expanded expression: " 4  (@4) *(2)  (@-2) *(2) 0 +  memcpy ()6 "
; Fused expression:    "( 4 , *(2) (@4) , + *(@-2) 0 , memcpy )6 "
	push	4
	push	word [bp+4]
	mov	ax, [bp-2]
	push	ax
	call	_memcpy
	sub	sp, -6
; RPN'ized expression: "( 6 , ethaddr addr -> *u , tabptr ethaddr -> *u &u addr -> *u memcpy ) "
; Expanded expression: " 6  (@6) *(2) 0 +  (@-2) *(2) 4 + 0 +  memcpy ()6 "
; Fused expression:    "( 6 , + *(@6) 0 , + *(@-2) 4 + ax 0 , memcpy )6 "
	push	6
	mov	ax, [bp+6]
	push	ax
	mov	ax, [bp-2]
	add	ax, 4
	push	ax
	call	_memcpy
	sub	sp, -6
; RPN'ized expression: "tabptr time -> *u arptime = "
; Expanded expression: "(@-2) *(2) 10 + arptime *(1) =(1) "
; Fused expression:    "+ *(@-2) 10 =(153) *ax *arptime "
	mov	ax, [bp-2]
	add	ax, 10
	mov	bx, ax
	mov	al, [_arptime]
	mov	ah, 0
	mov	[bx], al
	mov	ah, 0
L625:
	leave
	ret

; glb uip_arp_ipin : (void) void
section .text
	global	_uip_arp_ipin
_uip_arp_ipin:
	push	bp
	mov	bp, sp
	;sub	sp,          0
; loc     <something> : struct uip_eth_hdr
; RPN'ized expression: "uip_len <something654> sizeof -= "
; Expanded expression: "uip_len 14u -=(2) "
; Fused expression:    "-=(170) *uip_len 14u "
	mov	ax, [_uip_len]
	sub	ax, 14
	mov	[_uip_len], ax
; if
; loc     <something> : * struct ethip_hdr
; RPN'ized expression: "uip_buf 0 + *u &u (something657) srcipaddr -> *u 0 + *u uip_arp_netmask 0 + *u & uip_hostaddr 0 + *u uip_arp_netmask 0 + *u & != "
; Expanded expression: "uip_buf 0 + 26 + *(2) uip_arp_netmask 0 + *(2) & uip_hostaddr 0 + *(2) uip_arp_netmask 0 + *(2) & != "
; Fused expression:    "+ uip_buf 0 + ax 26 push-ax + uip_arp_netmask 0 & **sp *ax push-ax + uip_hostaddr 0 push-ax + uip_arp_netmask 0 & **sp *ax != *sp ax IF! "
	mov	ax, _uip_buf
	add	ax, 26
	push	ax
	mov	ax, _uip_arp_netmask
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	and	ax, cx
	push	ax
	mov	ax, _uip_hostaddr
	push	ax
	mov	ax, _uip_arp_netmask
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	and	ax, cx
	mov	cx, ax
	pop	ax
	cmp	ax, cx
	je	L655
; {
; return
	jmp	L652
; }
L655:
; if
; loc     <something> : * struct ethip_hdr
; RPN'ized expression: "uip_buf 0 + *u &u (something660) srcipaddr -> *u 1 + *u uip_arp_netmask 1 + *u & uip_hostaddr 1 + *u uip_arp_netmask 1 + *u & != "
; Expanded expression: "uip_buf 0 + 28 + *(2) uip_arp_netmask 2 + *(2) & uip_hostaddr 2 + *(2) uip_arp_netmask 2 + *(2) & != "
; Fused expression:    "+ uip_buf 0 + ax 28 push-ax + uip_arp_netmask 2 & **sp *ax push-ax + uip_hostaddr 2 push-ax + uip_arp_netmask 2 & **sp *ax != *sp ax IF! "
	mov	ax, _uip_buf
	add	ax, 28
	push	ax
	mov	ax, _uip_arp_netmask
	add	ax, 2
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	and	ax, cx
	push	ax
	mov	ax, _uip_hostaddr
	add	ax, 2
	push	ax
	mov	ax, _uip_arp_netmask
	add	ax, 2
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	and	ax, cx
	mov	cx, ax
	pop	ax
	cmp	ax, cx
	je	L658
; {
; return
	jmp	L652
; }
L658:
; loc     <something> : * struct ethip_hdr
; loc     <something> : * struct ethip_hdr
; RPN'ized expression: "( uip_buf 0 + *u &u (something662) ethhdr -> *u &u src -> *u &u , uip_buf 0 + *u &u (something661) srcipaddr -> *u uip_arp_update ) "
; Expanded expression: " uip_buf 0 + 0 + 6 +  uip_buf 0 + 26 +  uip_arp_update ()4 "
; Fused expression:    "( + uip_buf 0 + ax 0 + ax 6 , + uip_buf 0 + ax 26 , uip_arp_update )4 "
	mov	ax, _uip_buf
	add	ax, 6
	push	ax
	mov	ax, _uip_buf
	add	ax, 26
	push	ax
	call	_uip_arp_update
	sub	sp, -4
; return
L652:
	leave
	ret

; glb uip_arp_arpin : (void) void
section .text
	global	_uip_arp_arpin
_uip_arp_arpin:
	push	bp
	mov	bp, sp
	;sub	sp,          0
; if
; loc     <something> : struct arp_hdr
; RPN'ized expression: "uip_len <something667> sizeof < "
; Expanded expression: "uip_len *(2) 42u <u "
; Fused expression:    "<u *uip_len 42u IF! "
	mov	ax, [_uip_len]
	cmp	ax, 42
	jae	L665
; {
; RPN'ized expression: "uip_len 0 = "
; Expanded expression: "uip_len 0 =(2) "
; Fused expression:    "=(170) *uip_len 0 "
	mov	ax, 0
	mov	[_uip_len], ax
; return
	jmp	L663
; }
L665:
; RPN'ized expression: "uip_len 0 = "
; Expanded expression: "uip_len 0 =(2) "
; Fused expression:    "=(170) *uip_len 0 "
	mov	ax, 0
	mov	[_uip_len], ax
; switch
; loc     <something> : * struct arp_hdr
; RPN'ized expression: "uip_buf 0 + *u &u (something670) opcode -> *u "
; Expanded expression: "uip_buf 0 + 20 + *(2) "
; Fused expression:    "+ uip_buf 0 + ax 20 *(2) ax  "
	mov	ax, _uip_buf
	add	ax, 20
	mov	bx, ax
	mov	ax, [bx]
	jmp	L669
; {
; case
; loc         <something> : unsigned
; RPN'ized expression: "1 255 & (something671) 8 << 1 65280u & 8 >> | "
; Expanded expression: "256u "
; Expression value: 256u
L672:
; if
; loc         <something> : * struct arp_hdr
; loc         <something> : * struct arp_hdr
; RPN'ized expression: "uip_buf 0 + *u &u (something675) dipaddr -> *u 0 + *u uip_hostaddr 0 + *u == uip_buf 0 + *u &u (something676) dipaddr -> *u 1 + *u uip_hostaddr 1 + *u == && "
; Expanded expression: "uip_buf 0 + 38 + *(2) uip_hostaddr 0 + *(2) == [sh&&->677] uip_buf 0 + 40 + *(2) uip_hostaddr 2 + *(2) == &&[677] "
; Fused expression:    "+ uip_buf 0 + ax 38 push-ax + uip_hostaddr 0 == **sp *ax [sh&&->677] + uip_buf 0 + ax 40 push-ax + uip_hostaddr 2 == **sp *ax &&[677]  "
	mov	ax, _uip_buf
	add	ax, 38
	push	ax
	mov	ax, _uip_hostaddr
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	cmp	ax, cx
	sete	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L677
	mov	ax, _uip_buf
	add	ax, 40
	push	ax
	mov	ax, _uip_hostaddr
	add	ax, 2
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	cmp	ax, cx
	sete	al
	cbw
L677:
; JumpIfZero
	test	ax, ax
	je	L673
; {
; loc             <something> : * struct arp_hdr
; loc             <something> : unsigned
; RPN'ized expression: "uip_buf 0 + *u &u (something678) opcode -> *u 2 255 & (something679) 8 << 2 65280u & 8 >> | = "
; Expanded expression: "uip_buf 0 + 20 + 512u =(2) "
; Fused expression:    "+ uip_buf 0 + ax 20 =(170) *ax 512u "
	mov	ax, _uip_buf
	add	ax, 20
	mov	bx, ax
	mov	ax, 512
	mov	[bx], ax
; loc             <something> : * struct arp_hdr
; loc             <something> : * struct arp_hdr
; RPN'ized expression: "( 6 , uip_buf 0 + *u &u (something681) shwaddr -> *u &u addr -> *u , uip_buf 0 + *u &u (something680) dhwaddr -> *u &u addr -> *u memcpy ) "
; Expanded expression: " 6  uip_buf 0 + 22 + 0 +  uip_buf 0 + 32 + 0 +  memcpy ()6 "
; Fused expression:    "( 6 , + uip_buf 0 + ax 22 + ax 0 , + uip_buf 0 + ax 32 + ax 0 , memcpy )6 "
	push	6
	mov	ax, _uip_buf
	add	ax, 22
	push	ax
	mov	ax, _uip_buf
	add	ax, 32
	push	ax
	call	_memcpy
	sub	sp, -6
; loc             <something> : * struct arp_hdr
; RPN'ized expression: "( 6 , uip_ethaddr &u addr -> *u , uip_buf 0 + *u &u (something682) shwaddr -> *u &u addr -> *u memcpy ) "
; Expanded expression: " 6  uip_ethaddr 0 +  uip_buf 0 + 22 + 0 +  memcpy ()6 "
; Fused expression:    "( 6 , + uip_ethaddr 0 , + uip_buf 0 + ax 22 + ax 0 , memcpy )6 "
	push	6
	mov	ax, _uip_ethaddr
	push	ax
	mov	ax, _uip_buf
	add	ax, 22
	push	ax
	call	_memcpy
	sub	sp, -6
; loc             <something> : * struct arp_hdr
; RPN'ized expression: "( 6 , uip_ethaddr &u addr -> *u , uip_buf 0 + *u &u (something683) ethhdr -> *u &u src -> *u &u addr -> *u memcpy ) "
; Expanded expression: " 6  uip_ethaddr 0 +  uip_buf 0 + 0 + 6 + 0 +  memcpy ()6 "
; Fused expression:    "( 6 , + uip_ethaddr 0 , + uip_buf 0 + ax 0 + ax 6 + ax 0 , memcpy )6 "
	push	6
	mov	ax, _uip_ethaddr
	push	ax
	mov	ax, _uip_buf
	add	ax, 6
	push	ax
	call	_memcpy
	sub	sp, -6
; loc             <something> : * struct arp_hdr
; loc             <something> : * struct arp_hdr
; RPN'ized expression: "( 6 , uip_buf 0 + *u &u (something685) dhwaddr -> *u &u addr -> *u , uip_buf 0 + *u &u (something684) ethhdr -> *u &u dest -> *u &u addr -> *u memcpy ) "
; Expanded expression: " 6  uip_buf 0 + 32 + 0 +  uip_buf 0 + 0 + 0 + 0 +  memcpy ()6 "
; Fused expression:    "( 6 , + uip_buf 0 + ax 32 + ax 0 , + uip_buf 0 + ax 0 + ax 0 + ax 0 , memcpy )6 "
	push	6
	mov	ax, _uip_buf
	add	ax, 32
	push	ax
	mov	ax, _uip_buf
	push	ax
	call	_memcpy
	sub	sp, -6
; loc             <something> : * struct arp_hdr
; loc             <something> : * struct arp_hdr
; RPN'ized expression: "uip_buf 0 + *u &u (something686) dipaddr -> *u 0 + *u uip_buf 0 + *u &u (something687) sipaddr -> *u 0 + *u = "
; Expanded expression: "uip_buf 0 + 38 + uip_buf 0 + 28 + *(2) =(2) "
; Fused expression:    "+ uip_buf 0 + ax 38 push-ax + uip_buf 0 + ax 28 =(170) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 38
	push	ax
	mov	ax, _uip_buf
	add	ax, 28
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc             <something> : * struct arp_hdr
; loc             <something> : * struct arp_hdr
; RPN'ized expression: "uip_buf 0 + *u &u (something688) dipaddr -> *u 1 + *u uip_buf 0 + *u &u (something689) sipaddr -> *u 1 + *u = "
; Expanded expression: "uip_buf 0 + 40 + uip_buf 0 + 30 + *(2) =(2) "
; Fused expression:    "+ uip_buf 0 + ax 40 push-ax + uip_buf 0 + ax 30 =(170) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 40
	push	ax
	mov	ax, _uip_buf
	add	ax, 30
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc             <something> : * struct arp_hdr
; RPN'ized expression: "uip_buf 0 + *u &u (something690) sipaddr -> *u 0 + *u uip_hostaddr 0 + *u = "
; Expanded expression: "uip_buf 0 + 28 + uip_hostaddr 0 + *(2) =(2) "
; Fused expression:    "+ uip_buf 0 + ax 28 push-ax + uip_hostaddr 0 =(170) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 28
	push	ax
	mov	ax, _uip_hostaddr
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc             <something> : * struct arp_hdr
; RPN'ized expression: "uip_buf 0 + *u &u (something691) sipaddr -> *u 1 + *u uip_hostaddr 1 + *u = "
; Expanded expression: "uip_buf 0 + 30 + uip_hostaddr 2 + *(2) =(2) "
; Fused expression:    "+ uip_buf 0 + ax 30 push-ax + uip_hostaddr 2 =(170) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 30
	push	ax
	mov	ax, _uip_hostaddr
	add	ax, 2
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc             <something> : * struct arp_hdr
; loc             <something> : unsigned
; RPN'ized expression: "uip_buf 0 + *u &u (something692) ethhdr -> *u &u type -> *u 2054 255 & (something693) 8 << 2054 65280u & 8 >> | = "
; Expanded expression: "uip_buf 0 + 0 + 12 + 1544u =(2) "
; Fused expression:    "+ uip_buf 0 + ax 0 + ax 12 =(170) *ax 1544u "
	mov	ax, _uip_buf
	add	ax, 12
	mov	bx, ax
	mov	ax, 1544
	mov	[bx], ax
; loc             <something> : struct arp_hdr
; RPN'ized expression: "uip_len <something694> sizeof = "
; Expanded expression: "uip_len 42u =(2) "
; Fused expression:    "=(170) *uip_len 42u "
	mov	ax, 42
	mov	[_uip_len], ax
; }
L673:
; break
	jmp	L668
; case
; loc         <something> : unsigned
; RPN'ized expression: "2 255 & (something695) 8 << 2 65280u & 8 >> | "
; Expanded expression: "512u "
; Expression value: 512u
L696:
; if
; loc         <something> : * struct arp_hdr
; loc         <something> : * struct arp_hdr
; RPN'ized expression: "uip_buf 0 + *u &u (something699) dipaddr -> *u 0 + *u uip_hostaddr 0 + *u == uip_buf 0 + *u &u (something700) dipaddr -> *u 1 + *u uip_hostaddr 1 + *u == && "
; Expanded expression: "uip_buf 0 + 38 + *(2) uip_hostaddr 0 + *(2) == [sh&&->701] uip_buf 0 + 40 + *(2) uip_hostaddr 2 + *(2) == &&[701] "
; Fused expression:    "+ uip_buf 0 + ax 38 push-ax + uip_hostaddr 0 == **sp *ax [sh&&->701] + uip_buf 0 + ax 40 push-ax + uip_hostaddr 2 == **sp *ax &&[701]  "
	mov	ax, _uip_buf
	add	ax, 38
	push	ax
	mov	ax, _uip_hostaddr
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	cmp	ax, cx
	sete	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L701
	mov	ax, _uip_buf
	add	ax, 40
	push	ax
	mov	ax, _uip_hostaddr
	add	ax, 2
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	cmp	ax, cx
	sete	al
	cbw
L701:
; JumpIfZero
	test	ax, ax
	je	L697
; {
; loc             <something> : * struct arp_hdr
; loc             <something> : * struct arp_hdr
; RPN'ized expression: "( uip_buf 0 + *u &u (something703) shwaddr -> *u &u , uip_buf 0 + *u &u (something702) sipaddr -> *u uip_arp_update ) "
; Expanded expression: " uip_buf 0 + 22 +  uip_buf 0 + 28 +  uip_arp_update ()4 "
; Fused expression:    "( + uip_buf 0 + ax 22 , + uip_buf 0 + ax 28 , uip_arp_update )4 "
	mov	ax, _uip_buf
	add	ax, 22
	push	ax
	mov	ax, _uip_buf
	add	ax, 28
	push	ax
	call	_uip_arp_update
	sub	sp, -4
; }
L697:
; break
	jmp	L668
; }
	jmp	L668
L669:
	cmp	ax, 256
	je	L672
	cmp	ax, 512
	je	L696
L668:
; return
L663:
	leave
	ret

; glb uip_arp_out : (void) void
section .text
	global	_uip_arp_out
_uip_arp_out:
	push	bp
	mov	bp, sp
	 sub	sp,          2
; loc     tabptr : (@-2): * struct arp_entry
; if
; loc     <something> : * struct ethip_hdr
; loc     <something> : * struct ethip_hdr
; RPN'ized expression: "uip_buf 0 + *u &u (something708) destipaddr -> *u 0 + *u uip_arp_netmask 0 + *u & uip_hostaddr 0 + *u uip_arp_netmask 0 + *u & != uip_buf 0 + *u &u (something709) destipaddr -> *u 1 + *u uip_arp_netmask 1 + *u & uip_hostaddr 1 + *u uip_arp_netmask 1 + *u & != || "
; Expanded expression: "uip_buf 0 + 30 + *(2) uip_arp_netmask 0 + *(2) & uip_hostaddr 0 + *(2) uip_arp_netmask 0 + *(2) & != [sh||->710] uip_buf 0 + 32 + *(2) uip_arp_netmask 2 + *(2) & uip_hostaddr 2 + *(2) uip_arp_netmask 2 + *(2) & != ||[710] "
; Fused expression:    "+ uip_buf 0 + ax 30 push-ax + uip_arp_netmask 0 & **sp *ax push-ax + uip_hostaddr 0 push-ax + uip_arp_netmask 0 & **sp *ax != *sp ax [sh||->710] + uip_buf 0 + ax 32 push-ax + uip_arp_netmask 2 & **sp *ax push-ax + uip_hostaddr 2 push-ax + uip_arp_netmask 2 & **sp *ax != *sp ax ||[710]  "
	mov	ax, _uip_buf
	add	ax, 30
	push	ax
	mov	ax, _uip_arp_netmask
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	and	ax, cx
	push	ax
	mov	ax, _uip_hostaddr
	push	ax
	mov	ax, _uip_arp_netmask
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	and	ax, cx
	mov	cx, ax
	pop	ax
	cmp	ax, cx
	setne	al
	cbw
; JumpIfNotZero
	test	ax, ax
	jne	L710
	mov	ax, _uip_buf
	add	ax, 32
	push	ax
	mov	ax, _uip_arp_netmask
	add	ax, 2
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	and	ax, cx
	push	ax
	mov	ax, _uip_hostaddr
	add	ax, 2
	push	ax
	mov	ax, _uip_arp_netmask
	add	ax, 2
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	and	ax, cx
	mov	cx, ax
	pop	ax
	cmp	ax, cx
	setne	al
	cbw
L710:
; JumpIfZero
	test	ax, ax
	je	L706
; {
; RPN'ized expression: "ipaddr 0 + *u uip_arp_draddr 0 + *u = "
; Expanded expression: "ipaddr 0 + uip_arp_draddr 0 + *(2) =(2) "
; Fused expression:    "+ ipaddr 0 push-ax + uip_arp_draddr 0 =(170) **sp *ax "
	mov	ax, _ipaddr
	push	ax
	mov	ax, _uip_arp_draddr
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; RPN'ized expression: "ipaddr 1 + *u uip_arp_draddr 1 + *u = "
; Expanded expression: "ipaddr 2 + uip_arp_draddr 2 + *(2) =(2) "
; Fused expression:    "+ ipaddr 2 push-ax + uip_arp_draddr 2 =(170) **sp *ax "
	mov	ax, _ipaddr
	add	ax, 2
	push	ax
	mov	ax, _uip_arp_draddr
	add	ax, 2
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; }
	jmp	L707
L706:
; else
; {
; loc         <something> : * struct ethip_hdr
; RPN'ized expression: "ipaddr 0 + *u uip_buf 0 + *u &u (something711) destipaddr -> *u 0 + *u = "
; Expanded expression: "ipaddr 0 + uip_buf 0 + 30 + *(2) =(2) "
; Fused expression:    "+ ipaddr 0 push-ax + uip_buf 0 + ax 30 =(170) **sp *ax "
	mov	ax, _ipaddr
	push	ax
	mov	ax, _uip_buf
	add	ax, 30
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc         <something> : * struct ethip_hdr
; RPN'ized expression: "ipaddr 1 + *u uip_buf 0 + *u &u (something712) destipaddr -> *u 1 + *u = "
; Expanded expression: "ipaddr 2 + uip_buf 0 + 32 + *(2) =(2) "
; Fused expression:    "+ ipaddr 2 push-ax + uip_buf 0 + ax 32 =(170) **sp *ax "
	mov	ax, _ipaddr
	add	ax, 2
	push	ax
	mov	ax, _uip_buf
	add	ax, 32
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; }
L707:
; for
; RPN'ized expression: "i 0 = "
; Expanded expression: "i 0 =(1) "
; Fused expression:    "=(154) *i 0 "
	mov	ax, 0
	mov	[_i], al
	mov	ah, 0
L713:
; RPN'ized expression: "i 8 < "
; Expanded expression: "i *(1) 8 < "
; Fused expression:    "< *i 8 IF! "
	mov	al, [_i]
	mov	ah, 0
	cmp	ax, 8
	jge	L716
; RPN'ized expression: "i ++ "
; Expanded expression: "i ++(1) "
; {
; RPN'ized expression: "tabptr arp_table i + *u &u = "
; Expanded expression: "(@-2) arp_table i *(1) 12 * + =(2) "
; Fused expression:    "* *i 12 + arp_table ax =(170) *(@-2) ax "
	mov	al, [_i]
	mov	ah, 0
	imul	ax, ax, 12
	mov	cx, ax
	mov	ax, _arp_table
	add	ax, cx
	mov	[bp-2], ax
; if
; RPN'ized expression: "ipaddr 0 + *u tabptr ipaddr -> *u 0 + *u == ipaddr 1 + *u tabptr ipaddr -> *u 1 + *u == && "
; Expanded expression: "ipaddr 0 + *(2) (@-2) *(2) 0 + *(2) == [sh&&->719] ipaddr 2 + *(2) (@-2) *(2) 2 + *(2) == &&[719] "
; Fused expression:    "+ ipaddr 0 push-ax + *(@-2) 0 == **sp *ax [sh&&->719] + ipaddr 2 push-ax + *(@-2) 2 == **sp *ax &&[719]  "
	mov	ax, _ipaddr
	push	ax
	mov	ax, [bp-2]
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	cmp	ax, cx
	sete	al
	cbw
; JumpIfZero
	test	ax, ax
	je	L719
	mov	ax, _ipaddr
	add	ax, 2
	push	ax
	mov	ax, [bp-2]
	add	ax, 2
	mov	bx, ax
	mov	cx, [bx]
	pop	bx
	mov	ax, [bx]
	cmp	ax, cx
	sete	al
	cbw
L719:
; JumpIfZero
	test	ax, ax
	je	L717
; break
	jmp	L716
L717:
; }
L714:
; Fused expression:    "++(1) *i "
	inc	byte [_i]
	mov	al, [_i]
	mov	ah, 0
	jmp	L713
L716:
; if
; RPN'ized expression: "i 8 == "
; Expanded expression: "i *(1) 8 == "
; Fused expression:    "== *i 8 IF! "
	mov	al, [_i]
	mov	ah, 0
	cmp	ax, 8
	jne	L720
; {
; loc         <something> : * struct arp_hdr
; RPN'ized expression: "( 6 , 255 , uip_buf 0 + *u &u (something722) ethhdr -> *u &u dest -> *u &u addr -> *u memset ) "
; Expanded expression: " 6  255  uip_buf 0 + 0 + 0 + 0 +  memset ()6 "
; Fused expression:    "( 6 , 255 , + uip_buf 0 + ax 0 + ax 0 + ax 0 , memset )6 "
	push	6
	push	255
	mov	ax, _uip_buf
	push	ax
	call	_memset
	sub	sp, -6
; loc         <something> : * struct arp_hdr
; RPN'ized expression: "( 6 , 0 , uip_buf 0 + *u &u (something723) dhwaddr -> *u &u addr -> *u memset ) "
; Expanded expression: " 6  0  uip_buf 0 + 32 + 0 +  memset ()6 "
; Fused expression:    "( 6 , 0 , + uip_buf 0 + ax 32 + ax 0 , memset )6 "
	push	6
	push	0
	mov	ax, _uip_buf
	add	ax, 32
	push	ax
	call	_memset
	sub	sp, -6
; loc         <something> : * struct arp_hdr
; RPN'ized expression: "( 6 , uip_ethaddr &u addr -> *u , uip_buf 0 + *u &u (something724) ethhdr -> *u &u src -> *u &u addr -> *u memcpy ) "
; Expanded expression: " 6  uip_ethaddr 0 +  uip_buf 0 + 0 + 6 + 0 +  memcpy ()6 "
; Fused expression:    "( 6 , + uip_ethaddr 0 , + uip_buf 0 + ax 0 + ax 6 + ax 0 , memcpy )6 "
	push	6
	mov	ax, _uip_ethaddr
	push	ax
	mov	ax, _uip_buf
	add	ax, 6
	push	ax
	call	_memcpy
	sub	sp, -6
; loc         <something> : * struct arp_hdr
; RPN'ized expression: "( 6 , uip_ethaddr &u addr -> *u , uip_buf 0 + *u &u (something725) shwaddr -> *u &u addr -> *u memcpy ) "
; Expanded expression: " 6  uip_ethaddr 0 +  uip_buf 0 + 22 + 0 +  memcpy ()6 "
; Fused expression:    "( 6 , + uip_ethaddr 0 , + uip_buf 0 + ax 22 + ax 0 , memcpy )6 "
	push	6
	mov	ax, _uip_ethaddr
	push	ax
	mov	ax, _uip_buf
	add	ax, 22
	push	ax
	call	_memcpy
	sub	sp, -6
; loc         <something> : * struct arp_hdr
; RPN'ized expression: "uip_buf 0 + *u &u (something726) dipaddr -> *u 0 + *u ipaddr 0 + *u = "
; Expanded expression: "uip_buf 0 + 38 + ipaddr 0 + *(2) =(2) "
; Fused expression:    "+ uip_buf 0 + ax 38 push-ax + ipaddr 0 =(170) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 38
	push	ax
	mov	ax, _ipaddr
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc         <something> : * struct arp_hdr
; RPN'ized expression: "uip_buf 0 + *u &u (something727) dipaddr -> *u 1 + *u ipaddr 1 + *u = "
; Expanded expression: "uip_buf 0 + 40 + ipaddr 2 + *(2) =(2) "
; Fused expression:    "+ uip_buf 0 + ax 40 push-ax + ipaddr 2 =(170) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 40
	push	ax
	mov	ax, _ipaddr
	add	ax, 2
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc         <something> : * struct arp_hdr
; RPN'ized expression: "uip_buf 0 + *u &u (something728) sipaddr -> *u 0 + *u uip_hostaddr 0 + *u = "
; Expanded expression: "uip_buf 0 + 28 + uip_hostaddr 0 + *(2) =(2) "
; Fused expression:    "+ uip_buf 0 + ax 28 push-ax + uip_hostaddr 0 =(170) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 28
	push	ax
	mov	ax, _uip_hostaddr
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc         <something> : * struct arp_hdr
; RPN'ized expression: "uip_buf 0 + *u &u (something729) sipaddr -> *u 1 + *u uip_hostaddr 1 + *u = "
; Expanded expression: "uip_buf 0 + 30 + uip_hostaddr 2 + *(2) =(2) "
; Fused expression:    "+ uip_buf 0 + ax 30 push-ax + uip_hostaddr 2 =(170) **sp *ax "
	mov	ax, _uip_buf
	add	ax, 30
	push	ax
	mov	ax, _uip_hostaddr
	add	ax, 2
	mov	bx, ax
	mov	ax, [bx]
	pop	bx
	mov	[bx], ax
; loc         <something> : * struct arp_hdr
; loc         <something> : unsigned
; RPN'ized expression: "uip_buf 0 + *u &u (something730) opcode -> *u 1 255 & (something731) 8 << 1 65280u & 8 >> | = "
; Expanded expression: "uip_buf 0 + 20 + 256u =(2) "
; Fused expression:    "+ uip_buf 0 + ax 20 =(170) *ax 256u "
	mov	ax, _uip_buf
	add	ax, 20
	mov	bx, ax
	mov	ax, 256
	mov	[bx], ax
; loc         <something> : * struct arp_hdr
; loc         <something> : unsigned
; RPN'ized expression: "uip_buf 0 + *u &u (something732) hwtype -> *u 1 255 & (something733) 8 << 1 65280u & 8 >> | = "
; Expanded expression: "uip_buf 0 + 14 + 256u =(2) "
; Fused expression:    "+ uip_buf 0 + ax 14 =(170) *ax 256u "
	mov	ax, _uip_buf
	add	ax, 14
	mov	bx, ax
	mov	ax, 256
	mov	[bx], ax
; loc         <something> : * struct arp_hdr
; loc         <something> : unsigned
; RPN'ized expression: "uip_buf 0 + *u &u (something734) protocol -> *u 2048 255 & (something735) 8 << 2048 65280u & 8 >> | = "
; Expanded expression: "uip_buf 0 + 16 + 8u =(2) "
; Fused expression:    "+ uip_buf 0 + ax 16 =(170) *ax 8u "
	mov	ax, _uip_buf
	add	ax, 16
	mov	bx, ax
	mov	ax, 8
	mov	[bx], ax
; loc         <something> : * struct arp_hdr
; RPN'ized expression: "uip_buf 0 + *u &u (something736) hwlen -> *u 6 = "
; Expanded expression: "uip_buf 0 + 18 + 6 =(1) "
; Fused expression:    "+ uip_buf 0 + ax 18 =(154) *ax 6 "
	mov	ax, _uip_buf
	add	ax, 18
	mov	bx, ax
	mov	ax, 6
	mov	[bx], al
	mov	ah, 0
; loc         <something> : * struct arp_hdr
; RPN'ized expression: "uip_buf 0 + *u &u (something737) protolen -> *u 4 = "
; Expanded expression: "uip_buf 0 + 19 + 4 =(1) "
; Fused expression:    "+ uip_buf 0 + ax 19 =(154) *ax 4 "
	mov	ax, _uip_buf
	add	ax, 19
	mov	bx, ax
	mov	ax, 4
	mov	[bx], al
	mov	ah, 0
; loc         <something> : * struct arp_hdr
; loc         <something> : unsigned
; RPN'ized expression: "uip_buf 0 + *u &u (something738) ethhdr -> *u &u type -> *u 2054 255 & (something739) 8 << 2054 65280u & 8 >> | = "
; Expanded expression: "uip_buf 0 + 0 + 12 + 1544u =(2) "
; Fused expression:    "+ uip_buf 0 + ax 0 + ax 12 =(170) *ax 1544u "
	mov	ax, _uip_buf
	add	ax, 12
	mov	bx, ax
	mov	ax, 1544
	mov	[bx], ax
; RPN'ized expression: "uip_appdata uip_buf 40 14 + + *u &u = "
; Expanded expression: "uip_appdata uip_buf 54 + =(2) "
; Fused expression:    "+ uip_buf 54 =(170) *uip_appdata ax "
	mov	ax, _uip_buf
	add	ax, 54
	mov	[_uip_appdata], ax
; loc         <something> : struct arp_hdr
; RPN'ized expression: "uip_len <something740> sizeof = "
; Expanded expression: "uip_len 42u =(2) "
; Fused expression:    "=(170) *uip_len 42u "
	mov	ax, 42
	mov	[_uip_len], ax
; return
	jmp	L704
; }
L720:
; loc     <something> : * struct ethip_hdr
; RPN'ized expression: "( 6 , tabptr ethaddr -> *u &u addr -> *u , uip_buf 0 + *u &u (something741) ethhdr -> *u &u dest -> *u &u addr -> *u memcpy ) "
; Expanded expression: " 6  (@-2) *(2) 4 + 0 +  uip_buf 0 + 0 + 0 + 0 +  memcpy ()6 "
; Fused expression:    "( 6 , + *(@-2) 4 + ax 0 , + uip_buf 0 + ax 0 + ax 0 + ax 0 , memcpy )6 "
	push	6
	mov	ax, [bp-2]
	add	ax, 4
	push	ax
	mov	ax, _uip_buf
	push	ax
	call	_memcpy
	sub	sp, -6
; loc     <something> : * struct ethip_hdr
; RPN'ized expression: "( 6 , uip_ethaddr &u addr -> *u , uip_buf 0 + *u &u (something742) ethhdr -> *u &u src -> *u &u addr -> *u memcpy ) "
; Expanded expression: " 6  uip_ethaddr 0 +  uip_buf 0 + 0 + 6 + 0 +  memcpy ()6 "
; Fused expression:    "( 6 , + uip_ethaddr 0 , + uip_buf 0 + ax 0 + ax 6 + ax 0 , memcpy )6 "
	push	6
	mov	ax, _uip_ethaddr
	push	ax
	mov	ax, _uip_buf
	add	ax, 6
	push	ax
	call	_memcpy
	sub	sp, -6
; loc     <something> : * struct ethip_hdr
; loc     <something> : unsigned
; RPN'ized expression: "uip_buf 0 + *u &u (something743) ethhdr -> *u &u type -> *u 2048 255 & (something744) 8 << 2048 65280u & 8 >> | = "
; Expanded expression: "uip_buf 0 + 0 + 12 + 8u =(2) "
; Fused expression:    "+ uip_buf 0 + ax 0 + ax 12 =(170) *ax 8u "
	mov	ax, _uip_buf
	add	ax, 12
	mov	bx, ax
	mov	ax, 8
	mov	[bx], ax
; loc     <something> : struct uip_eth_hdr
; RPN'ized expression: "uip_len <something745> sizeof += "
; Expanded expression: "uip_len 14u +=(2) "
; Fused expression:    "+=(170) *uip_len 14u "
	mov	ax, [_uip_len]
	add	ax, 14
	mov	[_uip_len], ax
L704:
	leave
	ret

; glb uip_add32 : (
; prm     op32 : * unsigned char
; prm     op16 : unsigned
;     ) void
section .text
	global	_uip_add32
_uip_add32:
	push	bp
	mov	bp, sp
	;sub	sp,          0
; loc     op32 : (@4): * unsigned char
; loc     op16 : (@6): unsigned
; RPN'ized expression: "uip_acc32 3 + *u op32 3 + *u op16 255 & + = "
; Expanded expression: "uip_acc32 3 + (@4) *(2) 3 + *(1) (@6) *(2) 255 & + =(1) "
; Fused expression:    "+ uip_acc32 3 push-ax + *(@4) 3 push-ax & *(@6) 255 + **sp ax =(154) **sp ax "
	mov	ax, _uip_acc32
	add	ax, 3
	push	ax
	mov	ax, [bp+4]
	add	ax, 3
	push	ax
	mov	ax, [bp+6]
	and	ax, 255
	mov	cx, ax
	pop	bx
	mov	al, [bx]
	mov	ah, 0
	add	ax, cx
	pop	bx
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_acc32 2 + *u op32 2 + *u op16 8 >> + = "
; Expanded expression: "uip_acc32 2 + (@4) *(2) 2 + *(1) (@6) *(2) 8 >>u + =(1) "
; Fused expression:    "+ uip_acc32 2 push-ax + *(@4) 2 push-ax >>u *(@6) 8 + **sp ax =(154) **sp ax "
	mov	ax, _uip_acc32
	add	ax, 2
	push	ax
	mov	ax, [bp+4]
	add	ax, 2
	push	ax
	mov	ax, [bp+6]
	shr	ax, 8
	mov	cx, ax
	pop	bx
	mov	al, [bx]
	mov	ah, 0
	add	ax, cx
	pop	bx
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_acc32 1 + *u op32 1 + *u = "
; Expanded expression: "uip_acc32 1 + (@4) *(2) 1 + *(1) =(1) "
; Fused expression:    "+ uip_acc32 1 push-ax + *(@4) 1 =(153) **sp *ax "
	mov	ax, _uip_acc32
	inc	ax
	push	ax
	mov	ax, [bp+4]
	inc	ax
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; RPN'ized expression: "uip_acc32 0 + *u op32 0 + *u = "
; Expanded expression: "uip_acc32 0 + (@4) *(2) 0 + *(1) =(1) "
; Fused expression:    "+ uip_acc32 0 push-ax + *(@4) 0 =(153) **sp *ax "
	mov	ax, _uip_acc32
	push	ax
	mov	ax, [bp+4]
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	pop	bx
	mov	[bx], al
	mov	ah, 0
; if
; RPN'ized expression: "uip_acc32 2 + *u op16 8 >> < "
; Expanded expression: "uip_acc32 2 + *(1) (@6) *(2) 8 >>u <u "
; Fused expression:    "+ uip_acc32 2 push-ax >>u *(@6) 8 <u **sp ax IF! "
	mov	ax, _uip_acc32
	add	ax, 2
	push	ax
	mov	ax, [bp+6]
	shr	ax, 8
	mov	cx, ax
	pop	bx
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, cx
	jae	L748
; {
; RPN'ized expression: "uip_acc32 1 + *u ++ "
; Expanded expression: "uip_acc32 1 + ++(1) "
; Fused expression:    "+ uip_acc32 1 ++(1) *ax "
	mov	ax, _uip_acc32
	inc	ax
	mov	bx, ax
	inc	byte [bx]
	mov	al, [bx]
	mov	ah, 0
; if
; RPN'ized expression: "uip_acc32 1 + *u 0 == "
; Expanded expression: "uip_acc32 1 + *(1) 0 == "
; Fused expression:    "+ uip_acc32 1 == *ax 0 IF! "
	mov	ax, _uip_acc32
	inc	ax
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 0
	jne	L750
; {
; RPN'ized expression: "uip_acc32 0 + *u ++ "
; Expanded expression: "uip_acc32 0 + ++(1) "
; Fused expression:    "+ uip_acc32 0 ++(1) *ax "
	mov	ax, _uip_acc32
	mov	bx, ax
	inc	byte [bx]
	mov	al, [bx]
	mov	ah, 0
; }
L750:
; }
L748:
; if
; RPN'ized expression: "uip_acc32 3 + *u op16 255 & < "
; Expanded expression: "uip_acc32 3 + *(1) (@6) *(2) 255 & <u "
; Fused expression:    "+ uip_acc32 3 push-ax & *(@6) 255 <u **sp ax IF! "
	mov	ax, _uip_acc32
	add	ax, 3
	push	ax
	mov	ax, [bp+6]
	and	ax, 255
	mov	cx, ax
	pop	bx
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, cx
	jae	L752
; {
; RPN'ized expression: "uip_acc32 2 + *u ++ "
; Expanded expression: "uip_acc32 2 + ++(1) "
; Fused expression:    "+ uip_acc32 2 ++(1) *ax "
	mov	ax, _uip_acc32
	add	ax, 2
	mov	bx, ax
	inc	byte [bx]
	mov	al, [bx]
	mov	ah, 0
; if
; RPN'ized expression: "uip_acc32 2 + *u 0 == "
; Expanded expression: "uip_acc32 2 + *(1) 0 == "
; Fused expression:    "+ uip_acc32 2 == *ax 0 IF! "
	mov	ax, _uip_acc32
	add	ax, 2
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 0
	jne	L754
; {
; RPN'ized expression: "uip_acc32 1 + *u ++ "
; Expanded expression: "uip_acc32 1 + ++(1) "
; Fused expression:    "+ uip_acc32 1 ++(1) *ax "
	mov	ax, _uip_acc32
	inc	ax
	mov	bx, ax
	inc	byte [bx]
	mov	al, [bx]
	mov	ah, 0
; if
; RPN'ized expression: "uip_acc32 1 + *u 0 == "
; Expanded expression: "uip_acc32 1 + *(1) 0 == "
; Fused expression:    "+ uip_acc32 1 == *ax 0 IF! "
	mov	ax, _uip_acc32
	inc	ax
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	cmp	ax, 0
	jne	L756
; {
; RPN'ized expression: "uip_acc32 0 + *u ++ "
; Expanded expression: "uip_acc32 0 + ++(1) "
; Fused expression:    "+ uip_acc32 0 ++(1) *ax "
	mov	ax, _uip_acc32
	mov	bx, ax
	inc	byte [bx]
	mov	al, [bx]
	mov	ah, 0
; }
L756:
; }
L754:
; }
L752:
L746:
	leave
	ret

; glb uip_chksum : (
; prm     sdata : * unsigned
; prm     len : unsigned
;     ) unsigned
section .text
	global	_uip_chksum
_uip_chksum:
	push	bp
	mov	bp, sp
	 sub	sp,          2
; loc     sdata : (@4): * unsigned
; loc     len : (@6): unsigned
; loc     acc : (@-2): unsigned
; for
; RPN'ized expression: "acc 0 = "
; Expanded expression: "(@-2) 0 =(2) "
; Fused expression:    "=(170) *(@-2) 0 "
	mov	ax, 0
	mov	[bp-2], ax
L760:
; RPN'ized expression: "len 1 > "
; Expanded expression: "(@6) *(2) 1 >u "
; Fused expression:    ">u *(@6) 1 IF! "
	mov	ax, [bp+6]
	cmp	ax, 1
	jbe	L763
; RPN'ized expression: "len 2 -= "
; Expanded expression: "(@6) 2 -=(2) "
; {
; RPN'ized expression: "acc sdata *u += "
; Expanded expression: "(@-2) (@4) *(2) *(2) +=(2) "
; Fused expression:    "*(2) (@4) +=(170) *(@-2) *ax "
	mov	ax, [bp+4]
	mov	bx, ax
	mov	cx, [bx]
	mov	ax, [bp-2]
	add	ax, cx
	mov	[bp-2], ax
; if
; RPN'ized expression: "acc sdata *u < "
; Expanded expression: "(@-2) *(2) (@4) *(2) *(2) <u "
; Fused expression:    "*(2) (@4) <u *(@-2) *ax IF! "
	mov	ax, [bp+4]
	mov	bx, ax
	mov	cx, [bx]
	mov	ax, [bp-2]
	cmp	ax, cx
	jae	L764
; {
; RPN'ized expression: "acc ++ "
; Expanded expression: "(@-2) ++(2) "
; Fused expression:    "++(2) *(@-2) "
	inc	word [bp-2]
	mov	ax, [bp-2]
; }
L764:
; RPN'ized expression: "sdata ++ "
; Expanded expression: "(@4) 2 +=(2) "
; Fused expression:    "+=(170) *(@4) 2 "
	mov	ax, [bp+4]
	add	ax, 2
	mov	[bp+4], ax
; }
L761:
; Fused expression:    "-=(170) *(@6) 2 "
	mov	ax, [bp+6]
	sub	ax, 2
	mov	[bp+6], ax
	jmp	L760
L763:
; if
; RPN'ized expression: "len 1 == "
; Expanded expression: "(@6) *(2) 1 == "
; Fused expression:    "== *(@6) 1 IF! "
	mov	ax, [bp+6]
	cmp	ax, 1
	jne	L766
; {
; loc         <something> : unsigned
; loc         <something> : * unsigned char
; RPN'ized expression: "acc ( sdata (something769) *u (something768) 8 << htons ) += "
; Expanded expression: "(@-2)  (@4) *(2) *(1) 8 <<  htons ()2 +=(2) "
; Fused expression:    "( *(2) (@4) << *ax 8 , htons )2 +=(170) *(@-2) ax "
	mov	ax, [bp+4]
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	shl	ax, 8
	push	ax
	call	_htons
	sub	sp, -2
	mov	cx, ax
	mov	ax, [bp-2]
	add	ax, cx
	mov	[bp-2], ax
; if
; loc         <something> : unsigned
; loc         <something> : * unsigned char
; RPN'ized expression: "acc ( sdata (something773) *u (something772) 8 << htons ) < "
; Expanded expression: "(@-2) *(2)  (@4) *(2) *(1) 8 <<  htons ()2 <u "
; Fused expression:    "( *(2) (@4) << *ax 8 , htons )2 <u *(@-2) ax IF! "
	mov	ax, [bp+4]
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	shl	ax, 8
	push	ax
	call	_htons
	sub	sp, -2
	mov	cx, ax
	mov	ax, [bp-2]
	cmp	ax, cx
	jae	L770
; {
; RPN'ized expression: "acc ++ "
; Expanded expression: "(@-2) ++(2) "
; Fused expression:    "++(2) *(@-2) "
	inc	word [bp-2]
	mov	ax, [bp-2]
; }
L770:
; }
L766:
; return
; RPN'ized expression: "acc "
; Expanded expression: "(@-2) *(2) "
; Fused expression:    "*(2) (@-2)  "
	mov	ax, [bp-2]
L758:
	leave
	ret

; glb uip_ipchksum : (void) unsigned
section .text
	global	_uip_ipchksum
_uip_ipchksum:
	push	bp
	mov	bp, sp
	;sub	sp,          0
; return
; loc     <something> : * unsigned
; RPN'ized expression: "( 20 , uip_buf 14 + *u &u (something776) uip_chksum ) "
; Expanded expression: " 20  uip_buf 14 +  uip_chksum ()4 "
; Fused expression:    "( 20 , + uip_buf 14 , uip_chksum )4  "
	push	20
	mov	ax, _uip_buf
	add	ax, 14
	push	ax
	call	_uip_chksum
	sub	sp, -4
L774:
	leave
	ret

; glb uip_tcpchksum : (void) unsigned
section .text
	global	_uip_tcpchksum
_uip_tcpchksum:
	push	bp
	mov	bp, sp
	 sub	sp,          4
; loc     hsum : (@-2): unsigned
; loc     sum : (@-4): unsigned
; loc     <something> : * unsigned
; RPN'ized expression: "hsum ( 20 , uip_buf 20 14 + + *u &u (something779) uip_chksum ) = "
; Expanded expression: "(@-2)  20  uip_buf 34 +  uip_chksum ()4 =(2) "
; Fused expression:    "( 20 , + uip_buf 34 , uip_chksum )4 =(170) *(@-2) ax "
	push	20
	mov	ax, _uip_buf
	add	ax, 34
	push	ax
	call	_uip_chksum
	sub	sp, -4
	mov	[bp-2], ax
; loc     <something> : * unsigned
; loc     <something> : unsigned
; loc     <something> : unsigned
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; RPN'ized expression: "sum ( uip_buf 14 + *u &u (something783) len -> *u 0 + *u (something782) 8 << uip_buf 14 + *u &u (something784) len -> *u 1 + *u + 40 - (something781) , uip_appdata (something780) uip_chksum ) = "
; Expanded expression: "(@-4)  uip_buf 14 + 2 + *(1) 8 << uip_buf 14 + 3 + *(1) + 40 -  uip_appdata *(2)  uip_chksum ()4 =(2) "
; Fused expression:    "( + uip_buf 14 + ax 2 << *ax 8 push-ax + uip_buf 14 + ax 3 + *sp *ax - ax 40 , *(2) uip_appdata , uip_chksum )4 =(170) *(@-4) ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 2
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	shl	ax, 8
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 3
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	ax
	add	ax, cx
	sub	ax, 40
	push	ax
	push	word [_uip_appdata]
	call	_uip_chksum
	sub	sp, -4
	mov	[bp-4], ax
; if
; RPN'ized expression: "sum hsum += hsum < "
; Expanded expression: "(@-4) (@-2) *(2) +=(2) (@-2) *(2) <u "
; Fused expression:    "+=(170) *(@-4) *(@-2) <u ax *(@-2) IF! "
	mov	ax, [bp-4]
	add	ax, [bp-2]
	mov	[bp-4], ax
	cmp	ax, [bp-2]
	jae	L785
; {
; RPN'ized expression: "sum ++ "
; Expanded expression: "(@-4) ++(2) "
; Fused expression:    "++(2) *(@-4) "
	inc	word [bp-4]
	mov	ax, [bp-4]
; }
L785:
; if
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; RPN'ized expression: "sum uip_buf 14 + *u &u (something789) srcipaddr -> *u 0 + *u += uip_buf 14 + *u &u (something790) srcipaddr -> *u 0 + *u < "
; Expanded expression: "(@-4) uip_buf 14 + 12 + *(2) +=(2) uip_buf 14 + 12 + *(2) <u "
; Fused expression:    "+ uip_buf 14 + ax 12 +=(170) *(@-4) *ax push-ax + uip_buf 14 + ax 12 <u *sp *ax IF! "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 12
	mov	bx, ax
	mov	cx, [bx]
	mov	ax, [bp-4]
	add	ax, cx
	mov	[bp-4], ax
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 12
	mov	bx, ax
	mov	cx, [bx]
	pop	ax
	cmp	ax, cx
	jae	L787
; {
; RPN'ized expression: "sum ++ "
; Expanded expression: "(@-4) ++(2) "
; Fused expression:    "++(2) *(@-4) "
	inc	word [bp-4]
	mov	ax, [bp-4]
; }
L787:
; if
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; RPN'ized expression: "sum uip_buf 14 + *u &u (something793) srcipaddr -> *u 1 + *u += uip_buf 14 + *u &u (something794) srcipaddr -> *u 1 + *u < "
; Expanded expression: "(@-4) uip_buf 14 + 14 + *(2) +=(2) uip_buf 14 + 14 + *(2) <u "
; Fused expression:    "+ uip_buf 14 + ax 14 +=(170) *(@-4) *ax push-ax + uip_buf 14 + ax 14 <u *sp *ax IF! "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 14
	mov	bx, ax
	mov	cx, [bx]
	mov	ax, [bp-4]
	add	ax, cx
	mov	[bp-4], ax
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 14
	mov	bx, ax
	mov	cx, [bx]
	pop	ax
	cmp	ax, cx
	jae	L791
; {
; RPN'ized expression: "sum ++ "
; Expanded expression: "(@-4) ++(2) "
; Fused expression:    "++(2) *(@-4) "
	inc	word [bp-4]
	mov	ax, [bp-4]
; }
L791:
; if
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; RPN'ized expression: "sum uip_buf 14 + *u &u (something797) destipaddr -> *u 0 + *u += uip_buf 14 + *u &u (something798) destipaddr -> *u 0 + *u < "
; Expanded expression: "(@-4) uip_buf 14 + 16 + *(2) +=(2) uip_buf 14 + 16 + *(2) <u "
; Fused expression:    "+ uip_buf 14 + ax 16 +=(170) *(@-4) *ax push-ax + uip_buf 14 + ax 16 <u *sp *ax IF! "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 16
	mov	bx, ax
	mov	cx, [bx]
	mov	ax, [bp-4]
	add	ax, cx
	mov	[bp-4], ax
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 16
	mov	bx, ax
	mov	cx, [bx]
	pop	ax
	cmp	ax, cx
	jae	L795
; {
; RPN'ized expression: "sum ++ "
; Expanded expression: "(@-4) ++(2) "
; Fused expression:    "++(2) *(@-4) "
	inc	word [bp-4]
	mov	ax, [bp-4]
; }
L795:
; if
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; RPN'ized expression: "sum uip_buf 14 + *u &u (something801) destipaddr -> *u 1 + *u += uip_buf 14 + *u &u (something802) destipaddr -> *u 1 + *u < "
; Expanded expression: "(@-4) uip_buf 14 + 18 + *(2) +=(2) uip_buf 14 + 18 + *(2) <u "
; Fused expression:    "+ uip_buf 14 + ax 18 +=(170) *(@-4) *ax push-ax + uip_buf 14 + ax 18 <u *sp *ax IF! "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 18
	mov	bx, ax
	mov	cx, [bx]
	mov	ax, [bp-4]
	add	ax, cx
	mov	[bp-4], ax
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 18
	mov	bx, ax
	mov	cx, [bx]
	pop	ax
	cmp	ax, cx
	jae	L799
; {
; RPN'ized expression: "sum ++ "
; Expanded expression: "(@-4) ++(2) "
; Fused expression:    "++(2) *(@-4) "
	inc	word [bp-4]
	mov	ax, [bp-4]
; }
L799:
; if
; loc     <something> : unsigned
; loc     <something> : unsigned
; loc     <something> : unsigned
; loc     <something> : unsigned
; RPN'ized expression: "sum ( 6 (something806) htons ) (something805) += ( 6 (something808) htons ) (something807) < "
; Expanded expression: "(@-4)  6u  htons ()2 +=(2)  6u  htons ()2 <u "
; Fused expression:    "( 6u , htons )2 +=(170) *(@-4) ax push-ax ( 6u , htons )2 <u *sp ax IF! "
	push	6
	call	_htons
	sub	sp, -2
	mov	cx, ax
	mov	ax, [bp-4]
	add	ax, cx
	mov	[bp-4], ax
	push	ax
	push	6
	call	_htons
	sub	sp, -2
	mov	cx, ax
	pop	ax
	cmp	ax, cx
	jae	L803
; {
; RPN'ized expression: "sum ++ "
; Expanded expression: "(@-4) ++(2) "
; Fused expression:    "++(2) *(@-4) "
	inc	word [bp-4]
	mov	ax, [bp-4]
; }
L803:
; loc     <something> : unsigned
; loc     <something> : unsigned
; loc     <something> : * struct <something>
; loc     <something> : * struct <something>
; RPN'ized expression: "hsum ( uip_buf 14 + *u &u (something811) len -> *u 0 + *u (something810) 8 << uip_buf 14 + *u &u (something812) len -> *u 1 + *u + 20 - htons ) (something809) = "
; Expanded expression: "(@-2)  uip_buf 14 + 2 + *(1) 8 << uip_buf 14 + 3 + *(1) + 20 -  htons ()2 =(2) "
; Fused expression:    "( + uip_buf 14 + ax 2 << *ax 8 push-ax + uip_buf 14 + ax 3 + *sp *ax - ax 20 , htons )2 =(170) *(@-2) ax "
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 2
	mov	bx, ax
	mov	al, [bx]
	mov	ah, 0
	shl	ax, 8
	push	ax
	mov	ax, _uip_buf
	add	ax, 14
	add	ax, 3
	mov	bx, ax
	movzx	cx, byte [bx]
	pop	ax
	add	ax, cx
	sub	ax, 20
	push	ax
	call	_htons
	sub	sp, -2
	mov	[bp-2], ax
; if
; RPN'ized expression: "sum hsum += hsum < "
; Expanded expression: "(@-4) (@-2) *(2) +=(2) (@-2) *(2) <u "
; Fused expression:    "+=(170) *(@-4) *(@-2) <u ax *(@-2) IF! "
	mov	ax, [bp-4]
	add	ax, [bp-2]
	mov	[bp-4], ax
	cmp	ax, [bp-2]
	jae	L813
; {
; RPN'ized expression: "sum ++ "
; Expanded expression: "(@-4) ++(2) "
; Fused expression:    "++(2) *(@-4) "
	inc	word [bp-4]
	mov	ax, [bp-4]
; }
L813:
; return
; RPN'ized expression: "sum "
; Expanded expression: "(@-4) *(2) "
; Fused expression:    "*(2) (@-4)  "
	mov	ax, [bp-4]
L777:
	leave
	ret

; glb tcp_appcall : () void
section .text
	global	_tcp_appcall
_tcp_appcall:
	push	bp
	mov	bp, sp
	;sub	sp,          0
L815:
	leave
	ret

; glb udp_appcall : () void
section .text
	global	_udp_appcall
_udp_appcall:
	push	bp
	mov	bp, sp
	;sub	sp,          0
L817:
	leave
	ret

; glb modinit : () int
section .text
	global	_modinit
_modinit:
	push	bp
	mov	bp, sp
	;sub	sp,          0
; return
; RPN'ized expression: "0 "
; Expanded expression: "0 "
; Expression value: 0
; Fused expression:    "0  "
	mov	ax, 0
L819:
	leave
	ret



; Syntax/declaration table/stack:
; Bytes used: 5420/168960


; Macro table:
; Macro __SMALLER_C__ = `0x0100`
; Macro __SMALLER_C_16__ = ``
; Macro __SMALLER_C_SCHAR__ = ``
; Bytes used: 63/5120


; Identifier table:
; Ident __floatsisf
; Ident __floatunsisf
; Ident __fixsfsi
; Ident __fixunssfsi
; Ident __addsf3
; Ident __subsf3
; Ident __negsf2
; Ident __mulsf3
; Ident __divsf3
; Ident __lesf2
; Ident __gesf2
; Ident cmd
; Ident module_msg
; Ident datalen
; Ident data
; Ident <something>
; Ident get_buffer
; Ident u8_t
; Ident u16_t
; Ident uip_stats_t
; Ident uip_log
; Ident msg
; Ident uip_init
; Ident uip_buf
; Ident uip_listen
; Ident port
; Ident uip_unlisten
; Ident uip_conn
; Ident uip_connect
; Ident ripaddr
; Ident uip_udp_conn
; Ident uip_udp_new
; Ident rport
; Ident htons
; Ident val
; Ident uip_appdata
; Ident uip_sappdata
; Ident uip_urgdata
; Ident uip_len
; Ident uip_slen
; Ident uip_urglen
; Ident uip_surglen
; Ident lport
; Ident rcv_nxt
; Ident snd_nxt
; Ident len
; Ident mss
; Ident initialmss
; Ident sa
; Ident sv
; Ident rto
; Ident tcpstateflags
; Ident timer
; Ident nrtx
; Ident appstate
; Ident uip_conns
; Ident uip_acc32
; Ident uip_udp_conns
; Ident uip_stats
; Ident drop
; Ident recv
; Ident sent
; Ident vhlerr
; Ident hblenerr
; Ident lblenerr
; Ident fragerr
; Ident chkerr
; Ident protoerr
; Ident ip
; Ident typeerr
; Ident icmp
; Ident ackerr
; Ident rst
; Ident rexmit
; Ident syndrop
; Ident synrst
; Ident tcp
; Ident uip_stat
; Ident uip_flags
; Ident uip_process
; Ident flag
; Ident vhl
; Ident tos
; Ident ipid
; Ident ipoffset
; Ident ttl
; Ident proto
; Ident ipchksum
; Ident srcipaddr
; Ident destipaddr
; Ident srcport
; Ident destport
; Ident seqno
; Ident ackno
; Ident tcpoffset
; Ident flags
; Ident wnd
; Ident tcpchksum
; Ident urgp
; Ident optdata
; Ident uip_tcpip_hdr
; Ident type
; Ident icode
; Ident icmpchksum
; Ident id
; Ident uip_icmpip_hdr
; Ident udplen
; Ident udpchksum
; Ident uip_udpip_hdr
; Ident uip_hostaddr
; Ident uip_add32
; Ident op32
; Ident op16
; Ident uip_chksum
; Ident buf
; Ident uip_ipchksum
; Ident uip_tcpchksum
; Ident uip_arp_draddr
; Ident uip_arp_netmask
; Ident uip_listenports
; Ident iss
; Ident lastport
; Ident c
; Ident opt
; Ident tmp16
; Ident uip_reassbuf
; Ident uip_reassbitmap
; Ident bitmap_bits
; Ident uip_reasslen
; Ident uip_reassflags
; Ident uip_reasstmr
; Ident uip_reass
; Ident uip_add_rcv_nxt
; Ident n
; Ident uip_eth_addr
; Ident addr
; Ident uip_ethaddr
; Ident uip_eth_hdr
; Ident dest
; Ident src
; Ident uip_arp_init
; Ident uip_arp_ipin
; Ident uip_arp_arpin
; Ident uip_arp_out
; Ident uip_arp_timer
; Ident memset
; Ident a
; Ident c2u
; Ident l
; Ident memcpy
; Ident b
; Ident arp_hdr
; Ident ethhdr
; Ident hwtype
; Ident protocol
; Ident hwlen
; Ident protolen
; Ident opcode
; Ident shwaddr
; Ident sipaddr
; Ident dhwaddr
; Ident dipaddr
; Ident ethip_hdr
; Ident arp_entry
; Ident ipaddr
; Ident ethaddr
; Ident time
; Ident arp_table
; Ident i
; Ident arptime
; Ident tmpage
; Ident uip_arp_update
; Ident sdata
; Ident tcp_appcall
; Ident udp_appcall
; Ident modinit
; Bytes used: 1669/129536

; Next label number: 821
; Compilation succeeded.
