#include <module.h>

void bios_putc(char c) {
	asm("mov al, [bp+4]\nmov ah, 0x0e\nint 0x10");
}

int modinit() {
	bios_putc('H');
	bios_putc('i');
	return cmd;
}
