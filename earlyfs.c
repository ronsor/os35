#ifdef EARLYFS
#include "fatfs/pff.h"
#include <stddef.h>
#include "conf.h"
FATFS early_fs;
size_t earlyfs_fsize() {
	return early_fs.fsize;
}
void earlyfs_init() {
	if(pf_mount(&early_fs)) printf("Failed to load filesystem.");
}
FRESULT earlyfs_listfiles (char* path, char **buf) {
    FRESULT res;
    FILINFO fno;
    DIR dir;
    int i;
    size_t bufpos = 0;
    res = pf_opendir(&dir, path);
    if (res == FR_OK) {
        i = strlen(path);
        for (;;) {
            res = pf_readdir(&dir, &fno);
            if (res != FR_OK || fno.fname[0] == 0) break;
            if (fno.fattrib & AM_DIR) {
                sprintf(&path[i], "/%s", fno.fname);
                res = earlyfs_listfiles(path, buf + bufpos);
                if (res != FR_OK) break;
                path[i] = 0;
            } else {
                printf("%s/%s\n", path, fno.fname);
            }
        }
    }
    return res;
}
#endif
