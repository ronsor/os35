#define ONE_MB (1048576)
#include <stdio.h>
int A20Enabled(void)
{
  volatile unsigned char* p1 = (volatile unsigned char*)1;
  volatile unsigned char* p1M1 = (volatile unsigned char*)(ONE_MB + 1);
  unsigned char x1, x1M1;
  int enabled = 0;

  asm("cli");

  x1 = *p1;
  x1M1 = *p1M1;
  *p1M1 = ~x1;

  if (*p1 == x1)
  {
    *p1M1 = x1M1;
    enabled = 1;
  }
  else
  {
    *p1 = x1;
  }

  asm("sti");

  return enabled;
}

void TryEnableA20_Bios(void)
{
  asm("push ebp\n"
      "push dword 0\n"
      "mov  ax, 0x2401\n"
      "int  0x15\n"
      "pop  es\n"
      "pop  ds\n"
      "pop  ebp");
}

enum
{
  PORT_KBD_A = 0x60,
  PORT_KBD_STATUS = 0x64
};

unsigned char InPortByte(unsigned short port)
{
  asm("mov   dx, [bp+8]\n"
      "in    al, dx\n"
      "movzx eax, al");
}
void OutPortByte(unsigned short port, unsigned char value)
{
  asm("mov dx, [bp+8]\n"
      "mov al, [bp+12]\n"
      "out dx, al");
}

void KbdWaitDone2(void)
{
  while ((InPortByte(PORT_KBD_STATUS) & 2) == 2);
}
void KbdWaitDone1(void)
{
  while (!(InPortByte(PORT_KBD_STATUS) & 1));
}
void KbdSendCmd(unsigned char cmd)
{
  OutPortByte(PORT_KBD_STATUS, cmd);
}

void TryEnableA20_AtKbd(void)
{
  unsigned char x;
  asm("cli");

  KbdWaitDone2();
  KbdSendCmd(0xAD);
  KbdWaitDone2();

  KbdSendCmd(0xD0);
  KbdWaitDone1();
  x = InPortByte(PORT_KBD_A);
  KbdWaitDone2();

  KbdSendCmd(0xD1);
  KbdWaitDone2();

  OutPortByte(PORT_KBD_A, x | 2 | 1); // set bit 0 to prevent reset
  KbdWaitDone2();

  KbdSendCmd(0xAE);
  KbdWaitDone2();

  asm("sti");
}

void TryEnableA20_Ps2Fast(void)
{
  unsigned char x;
  asm("cli");
  x = InPortByte(0x92) & 0xFE; // reset bit 0 to prevent reset
  if ((x & 2) == 0)
    OutPortByte(0x92, x | 2);
  asm("sti");
}

// This A20 code is likely imperfect (no delays, no retries),
// but it appears to be working in DOSBox and VirtualBox.
// See:
// http://wiki.osdev.org/A20
// http://www.win.tue.nl/~aeb/linux/kbd/A20.html
// http://lxr.free-electrons.com/source/arch/x86/boot/a20.c
int EnableA20(void)
{
  if (A20Enabled())
  {
    puts("A20 already enabled.");
    return 1;
  }
  // Try himem.sys first? OTOH, if it's loaded, it's probably enabled A20 already.
  puts("Trying to enable A20 via BIOS...");
  TryEnableA20_Bios();
  if (A20Enabled())
  {
    puts("A20 enabled via BIOS.");
    return 1;
  }
  puts("Trying to enable A20 via keyboard controller...");
  TryEnableA20_AtKbd();
  if (A20Enabled())
  {
    puts("A20 enabled via keyboard controller.");
    return 1;
  }
  puts("Trying to enable A20 via fast method...");
  TryEnableA20_Ps2Fast();
  if (A20Enabled())
  {
    puts("A20 enabled via fast method.");
    return 1;
  }
  puts("Failed to enable A20!");
  return 0;
}
